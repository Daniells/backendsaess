﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class PersonasPorRolData
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public PersonasPorRolData(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<Persona> ObtenerPersonasPorRol(int rol)
        {
            var persona = (from p in _context.Persona
                               join pr in _context.PersonaRol on p.PersId equals pr.PersId
                               join r in _context.Rol on pr.RolId equals r.RolId

                               where r.RolId == rol
                               && p.PersActivo == true
                               && pr.PeroEstado == true
                               && r.RolEstado == true

                               select p).ToList();
             
            return persona;
        }
    }
}
