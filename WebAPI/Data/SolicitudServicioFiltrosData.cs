﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class SolicitudServicioFiltrosData
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public SolicitudServicioFiltrosData(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<SolicitudServicioFiltrosDto> ObtenerSolicitudServiciosPorNombrePersona(string personaNombre)
        {
            List<SolicitudServicioFiltrosDto> solicitud = new List<SolicitudServicioFiltrosDto>();

            var solicitudes = (from ss in _context.SolicitudServicio
                                     join c in _context.Contrato on
                                     ss.ContId equals c.ContId
                                     join per in _context.Persona on
                                     c.PersId equals per.PersId
                                     where ss.SoseActivo == true
                                        && c.ContActivo == true
                                        && per.PersNombre == personaNombre

                                     select ss);

            foreach (var items in solicitudes)
            {
                solicitud.Add(new SolicitudServicioFiltrosDto
                {
                    SoseId = items.SoseId,
                    Esso = (from est in _context.EstadoSolicitud where est.EssoId == items.EssoId && est.EssoActivo == true select est).FirstOrDefault(),
                    Tiso = (from t in _context.TipoSolicitud where t.TisoId == items.TisoId && t.TisoActivo == true select t).FirstOrDefault(),
                    SoseNumero = items.SoseNumero,
                    SoseDescripcion = items.SoseDescripcion,
                    SosePrecio = items.SosePrecio,
                    SoseFechaEjecucion = items.SoseFechaEjecucion,
                    SoseFechaCreacion = items.SoseFechaCreacion,
                    persId = (from c in _context.Contrato join per in _context.Persona on c.PersId equals per.PersId
                              where c.ContId == items.ContId && c.ContActivo == true select c.PersId).FirstOrDefault(),
                    Contrato = (from c in _context.Contrato where c.ContId == items.ContId where c.ContActivo == true select c).FirstOrDefault(),
                    UltimoNumeroSolicitudBD = (from s in _context.SolicitudServicio select s.SoseNumero).LastOrDefault()
                });
            }

            return solicitud;
        }

        public IEnumerable<SolicitudServicioFiltrosDto> ObtenerSolicitudServiciosPorPersonaCedula(string personaCedula)
        {
            List<SolicitudServicioFiltrosDto> solicitud = new List<SolicitudServicioFiltrosDto>();

            var solicitudes = (from ss in _context.SolicitudServicio
                               join c in _context.Contrato on
                               ss.ContId equals c.ContId
                               join per in _context.Persona on
                               c.PersId equals per.PersId
                               where ss.SoseActivo == true
                                  && c.ContActivo == true
                                  && per.PersNumDocumento == personaCedula

                               select ss).ToList();

            foreach (var items in solicitudes)
            {
                solicitud.Add(new SolicitudServicioFiltrosDto
                {
                    SoseId = items.SoseId,
                    Esso = (from est in _context.EstadoSolicitud where est.EssoId == items.EssoId && est.EssoActivo == true select est).FirstOrDefault(),
                    Tiso = (from t in _context.TipoSolicitud where t.TisoId == items.TisoId && t.TisoActivo == true select t).FirstOrDefault(),
                    SoseNumero = items.SoseNumero,
                    SoseDescripcion = items.SoseDescripcion,
                    SosePrecio = items.SosePrecio,
                    SoseFechaEjecucion = items.SoseFechaEjecucion,
                    SoseFechaCreacion = items.SoseFechaCreacion,
                    persId = (from c in _context.Contrato
                              join per in _context.Persona on c.PersId equals per.PersId
                              where c.ContId == items.ContId && c.ContActivo == true
                              select c.PersId).FirstOrDefault(),
                    Contrato = (from c in _context.Contrato where c.ContId == items.ContId where c.ContActivo == true select c).FirstOrDefault(),
                    UltimoNumeroSolicitudBD = (from s in _context.SolicitudServicio select s.SoseNumero).LastOrDefault()
                });
            }

            return solicitud;
        }

        public IEnumerable<SolicitudServicioFiltrosDto> ObtenerSolicitudServiciosPorNumeroContrato(string numContrato)
        {
            List<SolicitudServicioFiltrosDto> solicitud = new List<SolicitudServicioFiltrosDto>();

            var solicitudes = (from ss in _context.SolicitudServicio
                               join c in _context.Contrato on
                               ss.ContId equals c.ContId

                               where ss.SoseActivo == true
                                  && c.ContActivo == true
                                  && c.ContNumero == numContrato

                               select ss).ToList();

            foreach (var items in solicitudes)
            {
                solicitud.Add(new SolicitudServicioFiltrosDto
                {
                    SoseId = items.SoseId,
                    Esso = (from est in _context.EstadoSolicitud where est.EssoId == items.EssoId && est.EssoActivo == true select est).FirstOrDefault(),
                    Tiso = (from t in _context.TipoSolicitud where t.TisoId == items.TisoId && t.TisoActivo == true select t).FirstOrDefault(),
                    SoseNumero = items.SoseNumero,
                    SoseDescripcion = items.SoseDescripcion,
                    SosePrecio = items.SosePrecio,
                    SoseFechaEjecucion = items.SoseFechaEjecucion,
                    SoseFechaCreacion = items.SoseFechaCreacion,
                    persId = (from c in _context.Contrato
                              join per in _context.Persona on c.PersId equals per.PersId
                              where c.ContId == items.ContId && c.ContActivo == true
                              select c.PersId).FirstOrDefault(),
                    Contrato = (from c in _context.Contrato where c.ContId == items.ContId where c.ContActivo == true select c).FirstOrDefault(),
                    UltimoNumeroSolicitudBD = (from s in _context.SolicitudServicio select s.SoseNumero).LastOrDefault()
                });
            }

            return solicitud;
        }
    }
}
