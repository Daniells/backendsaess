﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class PersonaPorIDData
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public PersonaPorIDData(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<PersonaPorIdDto> ObtenerPersonaPorID(string Id)
        {
            List<PersonaPorIdDto> persona = new List<PersonaPorIdDto>();

            var informacion = (from p in _context.Persona
                               where p.PersId.Contains(Id)
                               select p).FirstOrDefault();

            if (informacion != null)
            {
                persona.Add(new PersonaPorIdDto
                {
                    PersId = Id,
                    PersNombre = informacion.PersNombre,
                    PersApellido = informacion.PersApellido,
                    PersFechanacimiento = informacion.PersFechanacimiento,
                    PersDireccion = informacion.PersDireccion,
                    PersTelefono = informacion.PersTelefono,
                    PersCelular = informacion.PersCelular,
                    PersEmail = informacion.PersEmail,
                    PersNumDocumento = informacion.PersNumDocumento,
                    Contribuyente = informacion.PersIscontribuyente == true ? "SI" : "NO",
                    Esci = (from e in _context.EstadoCivil where e.EsciId == informacion.EsciId && e.EsciActivo == true select e).FirstOrDefault(),
                    Sex = (from s in _context.Sexo where s.SexId == informacion.SexId && s.SexActivo == true select s).FirstOrDefault(),
                    Tido = (from t in _context.Tipodocumento where t.TidoId == informacion.TidoId && t.TidoActivo == true select t).FirstOrDefault(),
                    Clasificacion = (from cc in _context.ClasificacionCliente where cc.ClclId == informacion.ClclId && cc.ClclActivo == true select cc).FirstOrDefault(),
                    Tipo = (from tp in _context.Tipopersona where tp.TipeId == informacion.TipeId && tp.TipeActivo == true select tp).FirstOrDefault(),
                    zona = (from pz in _context.Personaconzona
                            join z in _context.Zona on pz.ZonaId equals z.ZonaId
                            where pz.PersId.Contains(informacion.PersId) && z.ZonaActivo == true && pz.PezoActivo == true
                            select z).ToList(),
                    sector = (from pz in _context.Personaconzona
                              join z in _context.Zona on pz.ZonaId equals z.ZonaId
                              join s in _context.Sector on z.ZonaId equals s.ZonaId
                              where pz.PersId == informacion.PersId && z.ZonaActivo == true && pz.PezoActivo == true && s.SectActivo == true
                              select s).ToList()
                });
            }
             
            return persona;
        }
    }
}
