﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class RepositorioSolicitudServicios  
    {
        private readonly SAESSContext _context;       
        private readonly IMapper _mapper;
               
        public RepositorioSolicitudServicios(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
              
        public IEnumerable<SolicitudServicio> ObtenerSolicitudServicios()
        {
            return (from ss in _context.SolicitudServicio where ss.SoseActivo == true select ss).ToList().OrderByDescending(x => x.SoseNumero);                      
        }
    }
}
