﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class FacturaData
    {

        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public FacturaData(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<FacturasPorFiltrosDto> obtenerFacturasPorNombrePersona(string nombre)
        {

            List<FacturasPorFiltrosDto> factura = new List<FacturasPorFiltrosDto>();
             
            var _facturas = (from f in _context.Factura   
                             join p in _context.Persona on
                             f.PersId equals p.PersId

                             where p.PersNombre.Contains(nombre)
                             && f.EsfaId != 3 && p.PersActivo == true
                             select f).ToList();

            foreach (var item in _facturas)
            {
                factura.Add(new FacturasPorFiltrosDto
                {
                    FactId = item.FactId,
                    FactNcontrol = item.FactNcontrol,
                    FactNfactura = item.FactNfactura,
                    FactIva = item.FactIva,
                    FactBase = item.FactBase,
                    FactMontoIva = item.FactMontoIva,
                    FactTotal = item.FactTotal,
                    FactFechaCreacion = item.FactFechaCreacion,
                    TipoDeMovimiento = item.TipoDeMovimiento,
                    FactDescuento = item.FactDescuento,
                    FactCodigoPago = item.FactCodigoPago,
                    FactConcepto = item.FactConcepto,
                    PersId = item.PersId,  
                    ContId = item.ContId,
                    estado = (from ef in _context.Estadofactura where ef.EsfaId == item.EsfaId select ef).FirstOrDefault(),
                    formaPago = (from fp in _context.FormaPago where fp.FopaId == item.FopaId && fp.FopaActivo == true select fp).FirstOrDefault(),
                    mes = (from m in _context.Mes where m.MesId == item.MesId && m.MesActivo == true select m).FirstOrDefault(),
                    serie = (from s in _context.Serie where s.SeriId == item.SeriId && s.SeriActivo == true select s).FirstOrDefault(),
                    contrato = (from c in _context.Contrato where c.PersId.Contains(item.PersId) && c.ContActivo == true select c).FirstOrDefault(),
                    servicio = (from s in _context.Servicio
                                where item.SerId == s.SerId
                                && s.SerActivo == true
                                select s).ToList()
                });
            }

            return factura;
        }
        
        public IEnumerable<FacturasPorFiltrosDto> obtenerFacturasPorNumeroContrato(string numero)
        {

            List<FacturasPorFiltrosDto> factura = new List<FacturasPorFiltrosDto>();

            var _facturas = (from f in _context.Factura
                             join c in _context.Contrato on
                             f.PersId equals c.PersId

                             where c.ContNumero.Contains(numero)
                             && f.EsfaId != 3 && c.ContActivo == true
                             select f).ToList();

            foreach (var item in _facturas)
            {
                factura.Add(new FacturasPorFiltrosDto
                {
                    FactId = item.FactId,
                    FactNcontrol = item.FactNcontrol,
                    FactNfactura = item.FactNfactura,
                    FactIva = item.FactIva,
                    FactBase = item.FactBase,
                    FactMontoIva = item.FactMontoIva,
                    FactTotal = item.FactTotal,
                    FactFechaCreacion = item.FactFechaCreacion,
                    TipoDeMovimiento = item.TipoDeMovimiento,
                    FactDescuento = item.FactDescuento,
                    FactCodigoPago = item.FactCodigoPago,
                    FactConcepto = item.FactConcepto,
                    PersId = item.PersId,
                    ContId = item.ContId,
                    estado = (from ef in _context.Estadofactura where ef.EsfaId == item.EsfaId select ef).FirstOrDefault(),
                    formaPago = (from fp in _context.FormaPago where fp.FopaId == item.FopaId && fp.FopaActivo == true select fp).FirstOrDefault(),
                    mes = (from m in _context.Mes where m.MesId == item.MesId && m.MesActivo == true select m).FirstOrDefault(),
                    serie = (from s in _context.Serie where s.SeriId == item.SeriId && s.SeriActivo == true select s).FirstOrDefault(),
                    contrato = (from c in _context.Contrato where c.PersId.Contains(item.PersId) && c.ContActivo == true select c).FirstOrDefault(),
                    servicio = (from s in _context.Servicio
                                where  item.SerId == s.SerId
                                &&  s.SerActivo == true
                                select s).ToList()
                });
            }

            return factura;
        }

        public IEnumerable<FacturasPorFiltrosDto> obtenerFacturasPorNumeroDocumento(string numero)
        {

            List<FacturasPorFiltrosDto> factura = new List<FacturasPorFiltrosDto>();

            var _facturas = (from f in _context.Factura
                             join p in _context.Persona on
                             f.PersId equals p.PersId

                             where p.PersNumDocumento.Contains(numero)
                             && f.EsfaId != 3 && p.PersActivo == true
                             select f).ToList();

            foreach (var item in _facturas)
            {
                factura.Add(new FacturasPorFiltrosDto
                {
                    FactId = item.FactId,
                    FactNcontrol = item.FactNcontrol,
                    FactNfactura = item.FactNfactura,
                    FactIva = item.FactIva,
                    FactBase = item.FactBase,
                    FactMontoIva = item.FactMontoIva,
                    FactTotal = item.FactTotal,
                    FactFechaCreacion = item.FactFechaCreacion,
                    TipoDeMovimiento = item.TipoDeMovimiento,
                    FactDescuento = item.FactDescuento,
                    FactCodigoPago = item.FactCodigoPago,
                    FactConcepto = item.FactConcepto,
                    PersId = item.PersId,
                    ContId = item.ContId,
                    estado = (from ef in _context.Estadofactura where ef.EsfaId == item.EsfaId select ef).FirstOrDefault(),
                    formaPago = (from fp in _context.FormaPago where fp.FopaId == item.FopaId && fp.FopaActivo == true select fp).FirstOrDefault(),
                    mes = (from m in _context.Mes where m.MesId == item.MesId && m.MesActivo == true select m).FirstOrDefault(),
                    serie = (from s in _context.Serie where s.SeriId == item.SeriId && s.SeriActivo == true select s).FirstOrDefault(),
                    contrato = (from c in _context.Contrato where c.PersId.Contains(item.PersId) && c.ContActivo == true select c).FirstOrDefault(),
                    servicio = (from s in _context.Servicio
                                where item.SerId == s.SerId
                                && s.SerActivo == true
                                select s).ToList()
                });
            }

            return factura;
        }
                
    }
}
