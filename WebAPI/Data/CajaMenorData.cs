﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class CajaMenorData
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public CajaMenorData(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IEnumerable<CajaMenorDto> obtenerRegistrosCajaPorFecha(string fechaInicio, string fechaFin)
        {

            List<CajaMenorDto> caja = new List<CajaMenorDto>();

            var registros = (from cm in _context.CajaMenor

                             where ((string.IsNullOrEmpty(fechaInicio) || (Convert.ToInt32(cm.CameFechaRegistro.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture).Replace("/", "").Replace("-", "")) >= Convert.ToInt32(fechaInicio.Replace("/", "").Replace("-", ""))))
                                && (string.IsNullOrEmpty(fechaFin) || (Convert.ToInt32(cm.CameFechaRegistro.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture).Replace("/", "").Replace("-", "")) <= Convert.ToInt32(fechaFin.Replace("/", "").Replace("-", "")))))
                                && cm.CameActivo == true
                                   select cm).ToList();

            foreach (var item in registros)
            {
                caja.Add(new CajaMenorDto
                {
                    CameId = item.CameId,
                    CameDescripcion = item.CameDescripcion,
                    CameIngreso = item.CameIngreso,
                    CameEgreso = item.CameEgreso,
                    CameFechaRegistro = item.CameFechaRegistro,
                    persona = (from p in _context.Persona where p.PersId.Contains(item.PersId) select p).FirstOrDefault()                   
                });
            }

            return caja;
        }
    }
}
