﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class TipoSolicitudData
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public TipoSolicitudData(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<TipoSolicitud> ObtenerTipoSolicitudes()
        {
            return _context.TipoSolicitud;
        }
    }
}
