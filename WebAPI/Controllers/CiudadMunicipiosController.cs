﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CiudadMunicipiosController : ControllerBase
    {
        private readonly SAESSContext _context;

        public CiudadMunicipiosController(SAESSContext context)
        {
            _context = context;
        }

         
        [HttpGet]
        public IEnumerable<CiudadMunicipiosDto> GetCiudadMunicipio()
        {
            var modelo = from cm in _context.CiudadMunicipio

                         select cm;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<CiudadMunicipio, CiudadMunicipiosDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<CiudadMunicipiosDto>>(modelo);
        }

      
        [HttpGet("{id}")]
        public IEnumerable<CiudadMunicipiosDto> GetCiudadMunicipio([FromRoute] long id)
        {
            var modelo = from cm in _context.CiudadMunicipio

                         where cm.CimuId == id
                         select cm;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<CiudadMunicipio, CiudadMunicipiosDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<CiudadMunicipiosDto>>(modelo);
        }


        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCiudadMunicipio([FromRoute] long id, [FromBody] CiudadMunicipio ciudadMunicipio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ciudadMunicipio.CimuId)
            {
                return BadRequest();
            }

            _context.Entry(ciudadMunicipio).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CiudadMunicipioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostCiudadMunicipio([FromBody] CiudadMunicipio ciudadMunicipio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.CiudadMunicipio.Add(ciudadMunicipio);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CiudadMunicipioExists(ciudadMunicipio.CimuId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCiudadMunicipio", new { id = ciudadMunicipio.CimuId }, ciudadMunicipio);
        }

        
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteCiudadMunicipio([FromRoute] long id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var ciudadMunicipio = await _context.CiudadMunicipio.FindAsync(id);
        //    if (ciudadMunicipio == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.CiudadMunicipio.Remove(ciudadMunicipio);
        //    await _context.SaveChangesAsync();

        //    return Ok(ciudadMunicipio);
        //}

        private bool CiudadMunicipioExists(long id)
        {
            return _context.CiudadMunicipio.Any(e => e.CimuId == id);
        }
    }
}