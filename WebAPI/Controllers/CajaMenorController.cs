﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CajaMenorController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public CajaMenorController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        [HttpGet("{fechaInicio}/{fechaFin}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]      
        public IEnumerable<CajaMenorResponseDto> GetCajaMenor(string fechaInicio, string fechaFin)
        {
            List<CajaMenorResponseDto> _null = new List<CajaMenorResponseDto>();
            CajaMenorData cajaMenor = new CajaMenorData(_context, _mapper);

            var response = cajaMenor.obtenerRegistrosCajaPorFecha(fechaInicio.Trim(), fechaFin.Trim());
            if(response.Count() > 0)
               return _mapper.Map<IEnumerable<CajaMenorResponseDto>>(response);
            
            return _null;
        }

      
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutCajaMenor([FromRoute] int id, [FromBody] CajaMenor cajaMenor)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);            

            if (id != cajaMenor.CameId)            
                return BadRequest();

            cajaMenor.CameFechaRegistro = DateTime.UtcNow.Date;
            cajaMenor.CameActivo = true;
            _context.Entry(cajaMenor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CajaMenorExists(id))                
                    return NotFound();                
                else                
                    throw;                
            }
             
            var response = await _context.CajaMenor.FindAsync(cajaMenor.CameId);
            return Ok(response);
        }

       
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostCajaMenor([FromBody] CajaMenor cajaMenor)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            

            cajaMenor.CameFechaRegistro = DateTime.UtcNow.Date; 
            cajaMenor.CameActivo = true;
            _context.CajaMenor.Add(cajaMenor);
            await _context.SaveChangesAsync();

            var response = await _context.CajaMenor.FindAsync(cajaMenor.CameId);

            if (cajaMenor == null)
                return BadRequest();

            return Ok(response);
        }

        
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteCajaMenor([FromRoute] int id)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            
            var cajaMenor = await _context.CajaMenor.FindAsync(id);

            if (cajaMenor == null)            
                return NotFound();

            cajaMenor.CameActivo = false;
            _context.Entry(cajaMenor).State = EntityState.Modified;            
            await _context.SaveChangesAsync();

            return Ok(cajaMenor);
        }


        private bool CajaMenorExists(int id)
        {
            return _context.CajaMenor.Any(e => e.CameId == id);
        }
    }
}