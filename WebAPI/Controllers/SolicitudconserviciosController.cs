﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SolicitudconserviciosController : ControllerBase
    {
        private readonly SAESSContext _context;

        public SolicitudconserviciosController(SAESSContext context)
        {
            _context = context;
        }

       
        [HttpGet]
        public IEnumerable<Solicitudconservicio> GetSolicitudconservicio()
        {
            return _context.Solicitudconservicio;
        }

        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSolicitudconservicio([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var solicitudconservicio = await _context.Solicitudconservicio.FindAsync(id);

            if (solicitudconservicio == null)
            {
                return NotFound();
            }

            return Ok(solicitudconservicio);
        }


       
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSolicitudconservicio([FromRoute] int id, [FromBody] Solicitudconservicio solicitudconservicio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != solicitudconservicio.SocseId)
            {
                return BadRequest();
            }

            _context.Entry(solicitudconservicio).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SolicitudconservicioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        

        [HttpPost]
        public async Task<IActionResult> PostSolicitudconservicio([FromBody] Solicitudconservicio solicitudconservicio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Solicitudconservicio.Add(solicitudconservicio);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSolicitudconservicio", new { id = solicitudconservicio.SocseId }, solicitudconservicio);
        }

      

        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteSolicitudconservicio([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var solicitudconservicio = await _context.Solicitudconservicio.FindAsync(id);
        //    if (solicitudconservicio == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Solicitudconservicio.Remove(solicitudconservicio);
        //    await _context.SaveChangesAsync();

        //    return Ok(solicitudconservicio);
        //}


        private bool SolicitudconservicioExists(int id)
        {
            return _context.Solicitudconservicio.Any(e => e.SocseId == id);
        }
    }
}