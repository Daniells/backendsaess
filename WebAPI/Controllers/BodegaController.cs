﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BodegaController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public BodegaController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<BodegaDto> GetBodega()
        {
            var modelo = _context.Bodega.Where(x => x.BodeActivo == true).ToList();
            return _mapper.Map<IEnumerable<BodegaDto>>(modelo);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<BodegaDto> GetBodega([FromRoute] int id)
        {
            var modelo = _context.Bodega.Where(x => x.BodeActivo == true
                                               && x.EmprId == id).ToList();
            return _mapper.Map<IEnumerable<BodegaDto>>(modelo);
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBodega([FromRoute] int id, [FromBody] BodegaDto bodegaDto)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            
            if (id != bodegaDto.BodeId)         
                return BadRequest();
         

            var modelo = _context.Bodega.Where(x => x.BodeActivo == true
                                              && x.BodeId == id).FirstOrDefault();
            if(modelo is null)
                return BadRequest();

            modelo.BodeNombre = bodegaDto.BodeNombre;
            modelo.BodeResponsable = bodegaDto.BodeResponsable;
            modelo.BodeDireccion = bodegaDto.BodeDireccion;
            modelo.BodeTelefono = bodegaDto.BodeTelefono;
            modelo.BodeFechaRegistro = DateTime.Now;
            _context.Entry(modelo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (BodegaExists(bodegaDto.BodeId))
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                else
                    throw;
            }

            return CreatedAtAction("GetBodega", new { id = bodegaDto.BodeId }, bodegaDto);
        }

        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostBodega([FromBody] Bodega bodega)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            bodega.BodeActivo = true;
            bodega.BodeFechaRegistro = DateTime.Now;

            _context.Bodega.Add(bodega);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBodega", new { id = bodega.BodeId }, bodega);
        }

        private bool BodegaExists(int id)
        {
            return _context.Bodega.Any(e => e.BodeId == id);
        }

    }
}