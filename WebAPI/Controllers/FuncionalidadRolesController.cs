﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FuncionalidadRolesController : ControllerBase
    {
        private readonly SAESSContext _context;

        public FuncionalidadRolesController(SAESSContext context)
        {
            _context = context;
        }

       
        [HttpGet]
        public IEnumerable<FuncionalidadRol> GetFuncionalidadRol()
        { // GET: api/FuncionalidadRoles

            return _context.FuncionalidadRol.OrderBy(x => x.FuncId);
        }


       
        [HttpGet("{id}")]
        public async Task<IEnumerable<FuncionalidadRolDto>> GetFuncionalidadRol([FromRoute] int id)
        {
            return await (from me in _context.Menu
                          join mo in _context.Modulo on
                          me.Idmenu equals mo.Idmenu
                          join f in _context.Funcionalidad on
                          mo.ModuId equals f.ModuId
                          join fr in _context.FuncionalidadRol on
                          f.FuncId equals fr.FuncId
                          join r in _context.Rol on
                          fr.RolId equals r.RolId
                          where me.Activo == true
                          && mo.ModuEstado == true
                          && f.FuncEstado == true
                          && fr.FuroEstado == true
                          && r.RolEstado == true
                          && r.RolId == id 

                          select new FuncionalidadRolDto
                          {
                              Idmenu = me.Idmenu,
                              Menu = me.Nombre,
                              Idmodulo = mo.ModuId,
                              Modulo = mo.ModuNombre ,
                              IdFuncionalidad = f.FuncId,
                              Funcionalidad = f.FuncNombre,
                              IdRol = r.RolId,
                              Rol = r.RolNombre
                          }).ToListAsync();
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFuncionalidadRol([FromRoute] int id, [FromBody] FuncionalidadRol funcionalidadRol)
        { // PUT: api/FuncionalidadRoles/5
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != funcionalidadRol.FuroId)
            {
                return BadRequest();
            }

            _context.Entry(funcionalidadRol).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FuncionalidadRolExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostFuncionalidadRol([FromBody] FuncionalidadRol funcionalidadRol)
        {   // POST: api/FuncionalidadRoles
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FuncionalidadRol.Add(funcionalidadRol);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FuncionalidadRolExists(funcionalidadRol.FuroId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFuncionalidadRol", new { id = funcionalidadRol.FuroId }, funcionalidadRol);
        }


     
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteFuncionalidadRol([FromRoute] int id)
        //{   
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var funcionalidadRol = await _context.FuncionalidadRol.FindAsync(id);
        //    if (funcionalidadRol == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.FuncionalidadRol.Remove(funcionalidadRol);
        //    await _context.SaveChangesAsync();

        //    return Ok(funcionalidadRol);
        //}



        private bool FuncionalidadRolExists(int id)
        {
            return _context.FuncionalidadRol.Any(e => e.FuroId == id);
        }
    }
}