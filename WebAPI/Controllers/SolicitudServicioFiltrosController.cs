﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Data;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SolicitudServicioFiltrosController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public SolicitudServicioFiltrosController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet("{personaNombre}/{personaCedula}/{numContrato}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<SolicitudServicioFiltrosResponseDto> GetSolicitudServicioFiltros(string personaNombre,string personaCedula, string numContrato)
        {
            SolicitudServicioFiltrosData solicitudFiltro = new SolicitudServicioFiltrosData(_context, _mapper);
            List<SolicitudServicioFiltrosResponseDto> _null = new List<SolicitudServicioFiltrosResponseDto>();

            if (personaNombre != "null") {

                var response = solicitudFiltro.ObtenerSolicitudServiciosPorNombrePersona(personaNombre);
                 
                return _mapper.Map<IEnumerable<SolicitudServicioFiltrosResponseDto>>(response);

            }
            else if (personaCedula != "null")
            {
                var response = solicitudFiltro.ObtenerSolicitudServiciosPorPersonaCedula(personaCedula);

                return _mapper.Map<IEnumerable<SolicitudServicioFiltrosResponseDto>>(response);                
            }
            else if (numContrato != "null")
            {
                var response = solicitudFiltro.ObtenerSolicitudServiciosPorNumeroContrato(numContrato);

                return _mapper.Map<IEnumerable<SolicitudServicioFiltrosResponseDto>>(response);                
            }

            return _null;
        }
    }
}