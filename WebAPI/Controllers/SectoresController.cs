﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{   
    [Route("api/[controller]")]
    [ApiController]
    public class SectoresController : ControllerBase
    {
        private readonly SAESSContext _context;

        public SectoresController(SAESSContext context)
        {
            _context = context;
        }

      
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<SectorDto> GetSector()
        {
            var modelo = from s in _context.Sector

                         where s.SectActivo == true
                         select s;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Sector, SectorDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<SectorDto>>(modelo);
        }

       

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<SectorDto> GetSector([FromRoute] int id)
        {
            var modelo = from s in _context.Sector

                         where s.SectActivo == true
                         && s.SectId == id
                         select s;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Sector, SectorDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<SectorDto>>(modelo);
        }


        [Authorize]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutSector([FromRoute] int id, ObjectSector sector)
        { 
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            

            if (id != sector.SectId)            
                return BadRequest();

            Sector objSector = objectoSector(sector);
            objSector.SectId = sector.SectId;
            _context.Entry(objSector).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                Sector _sector = await _context.Sector.FindAsync(id);
                if (_sector == null)
                    return NotFound();

                return Ok(_sector);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SectorExists(id))                
                    return NotFound();                
                else                
                    throw;                
            }            
        }


        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostSector(ObjectSector sector)
        { 
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);

            Sector objSector = objectoSector(sector);
            _context.Sector.Add(objSector);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SectorExists(sector.SectId))                
                    return new StatusCodeResult(StatusCodes.Status409Conflict);                
                else                
                    throw;                
            }
            
            return CreatedAtAction("GetSector", new { id = objSector.SectId }, objSector);
        }


        [Authorize]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteSector([FromRoute] int id)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var sector = await _context.Sector.FindAsync(id);

            if (sector == null || sector.SectActivo == false)         
                return NotFound();


            sector.SectActivo = false;
            _context.Entry(sector).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(sector);
        }

        
        private bool SectorExists(int id)
        {
            return _context.Sector.Any(e => e.SectId == id);
        }

        private Sector objectoSector(ObjectSector sector)
        {
            Sector _sector = new Sector();
            _sector.SectNombre = sector.SectNombre;
            _sector.SectDescripcion = sector.SectDescripcion;
            _sector.SectAbreviatura = sector.SectAbreviatura;
            _sector.SectRecargo = sector.SectRecargo;
            _sector.SectRegistradopor = sector.SectRegistradopor;
            _sector.ZonaId = sector.ZonaId;
            _sector.SectActivo = true;
            _sector.SectFechaCreacion = DateTime.UtcNow.Date;

            return _sector;
        }

    }
}