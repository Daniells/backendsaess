﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoCivilController : ControllerBase
    {
        private readonly SAESSContext _context;

        public EstadoCivilController(SAESSContext context)
        {
            _context = context;
        }

 
        [HttpGet]
        public IEnumerable<EstadoCivilDto> GetEstadoCivil()
        {
            var modelo = from ec in _context.EstadoCivil

                         select ec;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<EstadoCivil, EstadoCivilDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<EstadoCivilDto>>(modelo);
        }


        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetEstadoCivil([FromRoute] short id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var estadoCivil = await _context.EstadoCivil.FindAsync(id);

        //    if (estadoCivil == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(estadoCivil);
        //}

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEstadoCivil([FromRoute] short id, [FromBody] EstadoCivil estadoCivil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != estadoCivil.EsciId)
            {
                return BadRequest();
            }

            _context.Entry(estadoCivil).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstadoCivilExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostEstadoCivil([FromBody] EstadoCivil estadoCivil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.EstadoCivil.Add(estadoCivil);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEstadoCivil", new { id = estadoCivil.EsciId }, estadoCivil);
        }

    
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteEstadoCivil([FromRoute] short id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var estadoCivil = await _context.EstadoCivil.FindAsync(id);
        //    if (estadoCivil == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.EstadoCivil.Remove(estadoCivil);
        //    await _context.SaveChangesAsync();

        //    return Ok(estadoCivil);
        //}
         
        private bool EstadoCivilExists(short id)
        {
            return _context.EstadoCivil.Any(e => e.EsciId == id);
        }
    }
}