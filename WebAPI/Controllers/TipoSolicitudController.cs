﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TipoSolicitudController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public TipoSolicitudController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
         }

        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<TipoSolicitudDto> GetTipoSolicitud()
        {
            TipoSolicitudData tiposolicitud = new TipoSolicitudData(_context, _mapper);
            var response = tiposolicitud.ObtenerTipoSolicitudes();
            return _mapper.Map<IEnumerable<TipoSolicitudDto>>(response);
        }
         
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoSolicitud([FromRoute] int id, [FromBody] TipoSolicitud tipoSolicitud)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoSolicitud.TisoId)
            {
                return BadRequest();
            }

            _context.Entry(tipoSolicitud).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoSolicitudExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        
        [HttpPost]
        public async Task<IActionResult> PostTipoSolicitud([FromBody] TipoSolicitud tipoSolicitud)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.TipoSolicitud.Add(tipoSolicitud);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TipoSolicitudExists(tipoSolicitud.TisoId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTipoSolicitud", new { id = tipoSolicitud.TisoId }, tipoSolicitud);
        }
         
        private bool TipoSolicitudExists(int id)
        {
            return _context.TipoSolicitud.Any(e => e.TisoId == id);
        }
    }
}