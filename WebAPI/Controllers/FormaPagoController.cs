﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FormaPagoController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public FormaPagoController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
               

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<FormaPagoDto> GetFormaPago()
        {
            var response = (from fp in _context.FormaPago
                           where fp.FopaActivo == true select fp).ToList();
            return _mapper.Map<IEnumerable<FormaPagoDto>>(response);
        }         
    }
}