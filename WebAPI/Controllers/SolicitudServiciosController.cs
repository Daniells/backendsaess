﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SolicitudServiciosController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public SolicitudServiciosController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            this._mapper = mapper;
        }
        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<SolicitudServicioDto> GetSolicitudServicio()
        { 
            List<SolicitudServicioDto> _null = new List<SolicitudServicioDto>();  
            return _null;
        }      

        [HttpGet("{id}")]
        public IEnumerable<SolicitudServicioDto> GetSolicitudServicio(long id)
        {
            var modelo = from ss in _context.SolicitudServicio
                         join ps in _context.Productosolicitud on
                         ss.SoseId equals ps.SoseId
                         join p in _context.Producto on
                         ps.ProdId equals p.ProdId
                         join es in _context.EstadoSolicitud on
                         ss.EssoId equals es.EssoId
                         join scs in _context.Solicitudconservicio on
                         ss.SoseId equals scs.SoseId
                         join s in _context.Servicio on
                         scs.SerId equals s.SerId
                         join c in _context.Contrato on
                         ss.ContId equals c.ContId

                         where ss.SoseId == id
                         && ss.SoseActivo == true
                         && ps.SoprActivo == true
                         && p.ProdActivo == true
                         && es.EssoActivo == true
                         && scs.SocseActivo == true
                         && s.SerActivo == true
                         && c.ContActivo == true

                         select new SolicitudServicioDto
                         {
                             SoseId = ss.SoseId,                             
                             ProdId = p.ProdId,                           
                             FactId = ss.FactId,
                             SoseEmpleado = ss.SoseEmpleado,
                             SoseNumero = ss.SoseNumero,                             
                             SoseDescripcion = ss.SoseDescripcion,
                             SoseFechaEjecucion = ss.SoseFechaEjecucion,
                             SosePrecio = ss.SosePrecio,
                             SerId = s.SerId,                            
                             ProdCodigo = p.ProdCodigo,
                             ProdNombre = p.ProdNombre,
                             ProdPrecioVenta = p.ProdPrecioVenta,
                             SerNombre = s.SerNombre,
                             SerDescripcion = s.SerDescripcion,
                             SerCodigo = s.SerCodigo,
                             PersonaID = c.PersId
                         };

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SolicitudServicio, SolicitudServicioDto>()
               .ForMember(x => x.Contrato, o => o.Ignore())
               .ReverseMap();
            });

            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<SolicitudServicioDto>>(modelo);
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutSolicitudServicio([FromRoute] long id, [FromBody] SolicitudServicio solicitud)
        {   
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
           

            if (id != solicitud.SoseId)          
                return BadRequest();

            var _solicitud = _context.SolicitudServicio.Where(x => x.SoseId == id).FirstOrDefault();

            if (_solicitud is null)
                return NoContent();

            _solicitud.EssoId = solicitud.EssoId;
            _solicitud.SoseDescripcion = solicitud.SoseDescripcion;
            _solicitud.SoseFechaEjecucion = solicitud.SoseFechaEjecucion;
            _solicitud.SoseRegistradopor = solicitud.SoseRegistradopor;
            _solicitud.SosePrecio = solicitud.SosePrecio;
            _solicitud.SoseResponsable = solicitud.SoseResponsable;
            _solicitud.TisoId = solicitud.TisoId;
            _context.Entry(_solicitud).State = EntityState.Modified;
                
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SolicitudServicioExists(id))                 
                    return NotFound();                 
                else              
                    throw;              
            }

            return CreatedAtAction("GetSolicitudServicio", new { id = _solicitud.SoseId }, _solicitud);          
        }
              

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostSolicitudServicio(SolicitudDto solicitudDto)
        {
            long idContrato = 0;
            SolicitudServicio AuxSolicitud = new SolicitudServicio();

            if (!ModelState.IsValid)            
                return BadRequest(ModelState);

            var AuxFactura = setFactura(solicitudDto);
            var objectFactura = _context.Factura.Add(AuxFactura);
            await _context.SaveChangesAsync();


            AuxSolicitud = setSolicitudServicio(solicitudDto);
            AuxSolicitud.FactId = objectFactura.Entity.FactId;

            if (!string.IsNullOrEmpty(solicitudDto.ContNumero))
            {
                idContrato = _context.Contrato
                                        .Where(x => x.ContNumero.Trim() == solicitudDto.ContNumero)
                                        .FirstOrDefault().ContId;
            }

            AuxSolicitud.ContId = idContrato;
            _context.SolicitudServicio.Add(AuxSolicitud);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SolicitudServicioExists(solicitudDto.SoseId))                
                    return new StatusCodeResult(StatusCodes.Status409Conflict);              
                else               
                    throw;            
            }

            return CreatedAtAction("GetSolicitudServicio", new { id = AuxSolicitud.SoseId }, AuxSolicitud);
        }
                 
        private bool SolicitudServicioExists(long id)
        {
            return _context.SolicitudServicio.Any(e => e.SoseId == id);
        }

        private SolicitudServicio setSolicitudServicio(SolicitudDto solicitudDto)
        {
            SolicitudServicio response = new SolicitudServicio();
            response.EssoId = solicitudDto.EssoId; 
            response.SoseEmpleado = 1;
            response.SoseNumero = letraAleatoria() + numeroUnico(5).ToString();
            response.SoseDescripcion = solicitudDto.SoseDescripcion;            
            response.SoseRegistradopor = solicitudDto.SoseRegistradopor;
            response.SoseFechaCreacion = DateTime.UtcNow.Date;
            response.SoseActivo = true;
            response.SosePrecio = solicitudDto.SosePrecio;
            response.SoseResponsable = solicitudDto.SoseResponsable;
            response.TisoId = solicitudDto.TisoId;
            return response;
        }

        private Factura setFactura(SolicitudDto solicitudDto)
        {
            Factura response = new Factura();
            response.SeriId = 1;
            response.FactNcontrol = "1";
            response.FactNfactura = letraAleatoria() + numeroUnico(6).ToString();
            response.FopaId = solicitudDto.Factura.FopaId;
            response.FactIva = solicitudDto.Factura.FactIva;
            response.FactBase = solicitudDto.Factura.FactBase;
            response.FactMontoIva = response.FactBase * (((decimal)response.FactIva) / 100);
            response.FactTotal = response.FactBase + response.FactMontoIva;
            response.FactRegistradopor = solicitudDto.SoseRegistradopor;
            response.FactFechaCreacion = DateTime.UtcNow.Date;
            response.SerId = solicitudDto.Factura.SerId;
            response.TipoDeMovimiento = "1";
            response.MesId = solicitudDto.Factura.MesId;
            response.PersId = solicitudDto.Factura.PersId;
            response.EsfaId = 2;
            response.FactDescuento = 0;
            response.FactCodigoPago = solicitudDto.Factura.FactCodigoPago;
            response.FactConcepto = solicitudDto.Factura.FactConcepto;

            return response;
        }

        private long numeroUnico(int numero)
        {
            DateTime today = DateTime.UtcNow.Date;
            long number = today.Year + today.Month + today.Day + today.Hour + today.Minute + today.Second + today.Millisecond + numero;
            return number;
        }

        private string letraAleatoria()
        {
            Random rnd = new Random();
            return ((char)rnd.Next('A', 'Z')) + ((char)rnd.Next('A', 'Z')).ToString();
        }

    }
}