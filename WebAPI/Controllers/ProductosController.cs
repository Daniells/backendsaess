﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public ProductosController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<ProductoDto> GetProducto()
        { 
            var producto = from p in _context.Producto.OrderBy(x => x.ProdNombre)                          
                         where p.ProdActivo == true                        

                         select new Producto {
                           ProdId = p.ProdId,                          
                           ProdCodigo = p.ProdCodigo,
                           ProdNombre = p.ProdNombre,                                       
                           ProdPrecioVenta = p.ProdPrecioVenta,                           
                           ProdCminimo = p.ProdCminimo,
                           Capr = _context.CategoriaProducto
                                               .Where(x=>x.CaprId == p.CaprId).FirstOrDefault(),
                           Unme = _context.UnidadMedida
                                               .Where(x => x.UnmeId == p.UnmeId).FirstOrDefault(),
                           Iva = _context.Iva.Where(x => x.IvaId == p.IvaId).FirstOrDefault(),
                           IdmarcaNavigation = _context.Marca
                                                    .Where(x => x.Idmarca == p.Idmarca).FirstOrDefault(),
                           IdgrupoNavigation = _context.Grupoproducto
                                                    .Where(x => x.Idgrupo == p.Idgrupo).FirstOrDefault()
                         };

            return _mapper.Map<IEnumerable<ProductoDto>>(producto);
        }
                
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<ProductoDto> GetProducto(int id)
        {
            var producto = from p in _context.Producto.OrderBy(x => x.ProdNombre)
                           where p.ProdActivo == true
                           && p.ProdId == id

                           select new Producto
                           {
                               ProdId = p.ProdId,
                               ProdCodigo = p.ProdCodigo,
                               ProdNombre = p.ProdNombre,
                               ProdPrecioVenta = p.ProdPrecioVenta,
                               ProdCminimo = p.ProdCminimo,
                               Capr = _context.CategoriaProducto
                                                 .Where(x => x.CaprId == p.CaprId).FirstOrDefault(),
                               Unme = _context.UnidadMedida
                                                 .Where(x => x.UnmeId == p.UnmeId).FirstOrDefault(),
                               Iva = _context.Iva.Where(x => x.IvaId == p.IvaId).FirstOrDefault(),
                               IdmarcaNavigation = _context.Marca
                                                      .Where(x => x.Idmarca == p.Idmarca).FirstOrDefault(),
                               IdgrupoNavigation = _context.Grupoproducto
                                                      .Where(x => x.Idgrupo == p.Idgrupo).FirstOrDefault()
                           };

            return _mapper.Map<IEnumerable<ProductoDto>>(producto);
        }

        [Authorize]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutProducto([FromRoute] int id, [FromBody] Producto producto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != producto.ProdId)
                return BadRequest();

            Producto auxproducto = _context.Producto
                                  .Where(e => e.ProdId == id && e.ProdActivo == true)
                                  .FirstOrDefault();

            if (auxproducto is null)            
                return BadRequest();            
            else {
                auxproducto.CaprId = producto.CaprId;
                auxproducto.UnmeId = producto.UnmeId;
                auxproducto.IvaId = producto.IvaId;
                auxproducto.ProdNombre = producto.ProdNombre;
                auxproducto.ProdPrecioVenta = producto.ProdPrecioVenta;
                auxproducto.ProdRegistradopor = producto.ProdRegistradopor;
                auxproducto.ProdCminimo = producto.ProdCminimo;
                auxproducto.Idgrupo = producto.Idgrupo;
                auxproducto.Idmarca = producto.Idmarca;
                auxproducto.ProdFechaCreacion = DateTime.Now.Date;
            }


            _context.Entry(auxproducto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductoExists(id))                
                    return NotFound();                
                else                
                    throw;                
            }

            return CreatedAtAction("GetProducto", new { id = auxproducto.ProdId }, auxproducto);
        }


        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostProducto(Producto producto)
        {  
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);

            producto.ProdCodigo = numeroUnico().ToString();
            producto.ProdFechaCreacion = DateTime.Now.Date;
            producto.ProdActivo = true;
            _context.Producto.Add(producto);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProductoExists(producto.ProdId))                
                    return new StatusCodeResult(StatusCodes.Status409Conflict);                
                else                
                    throw;                
            }

            return CreatedAtAction("GetProducto", new { id = producto.ProdId }, producto);
        }

        [Authorize]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteProducto([FromRoute] int id)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);            

            var producto = await _context.Producto.FindAsync(id);
            if (producto == null)            
                return NotFound();

            producto.ProdActivo = false;
            _context.Entry(producto).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(producto);
        }


        private bool ProductoExists(int id)
        {
            return _context.Producto.Any(e => e.ProdId == id && e.ProdActivo == true);
        }

        private string numeroUnico()
        {
            DateTime today = DateTime.Now;
            long number = today.Year + today.Month + today.Day + today.Hour;
            string response = number.ToString() + today.Minute.ToString() + today.Second.ToString() + today.Millisecond.ToString();            
            return response;
        }
    }
}