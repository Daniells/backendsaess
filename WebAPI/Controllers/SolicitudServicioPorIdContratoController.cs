﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SolicitudServicioPorIdContratoController : ControllerBase
    {
        private readonly SAESSContext _context;

        public SolicitudServicioPorIdContratoController(SAESSContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IEnumerable<SolicitudServicioDto> GetSolicitudServicioPorIdContrato(long id)
        {
            var modelo = from c in _context.Contrato 
                         join ss in _context.SolicitudServicio on
                         c.ContId equals ss.ContId
                         join ps in _context.Productosolicitud on
                         ss.SoseId equals ps.SoseId
                         join p in _context.Producto on
                         ps.ProdId equals p.ProdId
                         join es in _context.EstadoSolicitud on
                         ss.EssoId equals es.EssoId
                         join scs in _context.Solicitudconservicio on
                         ss.SoseId equals scs.SoseId
                         join s in _context.Servicio on
                         scs.SerId equals s.SerId

                         where c.ContId == id
                         && ss.SoseActivo == true
                         && ps.SoprActivo == true
                         && p.ProdActivo == true
                         && es.EssoActivo == true
                         && scs.SocseActivo == true
                         && s.SerActivo == true

                         select new SolicitudServicioDto
                         {
                             SoseId = ss.SoseId,
                             ContId = c.ContId,
                             ProdId = p.ProdId,                             
                             FactId = ss.FactId,
                             SoseEmpleado = ss.SoseEmpleado,
                             SoseNumero = ss.SoseNumero,
                             SoseDescripcion = ss.SoseDescripcion,
                             SoseFechaEjecucion = ss.SoseFechaEjecucion,
                             SosePrecio = ss.SosePrecio,
                             SerId = s.SerId,                            
                             ProdCodigo = p.ProdCodigo,
                             ProdNombre = p.ProdNombre,
                             ProdPrecioVenta = p.ProdPrecioVenta,
                             SerNombre = s.SerNombre,
                             SerDescripcion = s.SerDescripcion,
                             SerCodigo = s.SerCodigo
                         };

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SolicitudServicio, SolicitudServicioDto>()
               .ForMember(x => x.Contrato, o => o.Ignore())
               .ReverseMap();
            });

            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<SolicitudServicioDto>>(modelo);
        }
    }
}
