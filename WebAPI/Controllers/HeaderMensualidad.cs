﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class HeaderMensualidad : ControllerBase
    {
        private readonly SAESSContext _context;

        public HeaderMensualidad(SAESSContext context)
        {
            _context = context;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public Mensualidades Get()
        {
            //Mes Actual
            DateTime today = DateTime.UtcNow;
            int daysOfMonth = DateTime.DaysInMonth(today.Year, today.Month);
            DateTime startDate = new DateTime(today.Year, today.Month, 01);
            DateTime endDate = new DateTime(today.Year, today.Month, daysOfMonth);

            //Mes Anterior
            DateTime dateAnt = today.AddMonths(-1);           
            int daysOfMonthAnt = DateTime.DaysInMonth(dateAnt.Year, dateAnt.Month);
            DateTime startDateAnt = new DateTime(dateAnt.Year, dateAnt.Month, 01);
            DateTime endDateAnt = new DateTime(dateAnt.Year, dateAnt.Month, daysOfMonthAnt);

            //Mes siguiente
            DateTime dateSig = today.AddMonths(1);
            DateTime startDateSig = new DateTime(dateSig.Year, dateSig.Month, 01);
                      

            Mensualidades mensualidades = new Mensualidades();
            mensualidades.MesActual = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(today.Month);
            mensualidades.MesAnterior = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateAnt.Month);

            mensualidades.PagadasMesActual = (from f in _context.Factura
                                              where (f.FactFechaCreacion >= startDate && f.FactFechaCreacion <= endDate)
                                                  && f.EsfaId == 1
                                              select f).Count();

            mensualidades.PendientesMesActual = (from f in _context.Factura
                                              where (f.FactFechaCreacion >= startDate && f.FactFechaCreacion <= endDate)
                                                  && f.EsfaId == 2
                                              select f).Count();

            mensualidades.PendientesMesAnterior = (from f in _context.Factura
                                              where (f.FactFechaCreacion >= startDateAnt && f.FactFechaCreacion <= endDateAnt)
                                                  && f.EsfaId == 2
                                              select f).Count();

            mensualidades.Adelantadas = (from f in _context.Factura
                                                   where (f.FactFechaCreacion >= startDateSig) && f.EsfaId == 1
                                                   select f).Count();

            return mensualidades;
        }       
    }
}