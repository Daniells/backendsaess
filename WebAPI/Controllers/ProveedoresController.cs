﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProveedoresController : ControllerBase
    {
        private readonly SAESSContext _context;

        public ProveedoresController(SAESSContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<PersonaDto> GetProveedores()
        {
            var Persona = from p in _context.Persona
                          join s in _context.Sexo on
                          p.SexId equals s.SexId
                          join tp in _context.Tipopersona on
                          p.TipeId equals tp.TipeId
                          join td in _context.Tipodocumento on
                          p.TidoId equals td.TidoId
                          join ec in _context.EstadoCivil on
                          p.EsciId equals ec.EsciId
                          join pr in _context.PersonaRol on
                          p.PersId equals pr.PersId
                          join r in _context.Rol on
                          pr.RolId equals r.RolId

                          where
                          r.RolId  == 4
                          && p.PersActivo == true
                          && s.SexActivo == true
                          && tp.TipeActivo == true
                          && td.TidoActivo == true
                          && r.RolEstado == true

                          select new PersonaDto
                          {
                              PersId = p.PersId,
                              EsciId = p.EsciId,
                              EsciNombre = ec.EsciNombre,
                              PersIscontribuyente = p.PersIscontribuyente,
                              PersNombre = p.PersNombre,
                              PersApellido = p.PersApellido,
                              PersFechanacimiento = p.PersFechanacimiento,
                              PersDireccion = p.PersDireccion,
                              PersTelefono = p.PersTelefono,
                              PersCelular = p.PersCelular,
                              PersEmail = p.PersEmail,
                              PersSexo = s.SexNombre,
                              tipodocumento = td.TidoNombre,
                              TipoPersona = tp.TipeNombre,
                              PersNumDocumento = p.PersNumDocumento,
                              SexId = s.SexId,
                              tidoId = td.TidoId,
                              TipeId = tp.TipeId,
                              RolNombre = r.RolNombre
                          };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Persona, PersonaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<PersonaDto>>(Persona);
        }
    }
}
