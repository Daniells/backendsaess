﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartamentosController : ControllerBase
    {
        private readonly SAESSContext _context;

        public DepartamentosController(SAESSContext context)
        {
            _context = context;
        }

       
        [HttpGet]
        public IEnumerable<DepartamentoDto> GetDepartamento()
        {
            var modelo = from d in _context.Departamento
                          
                         select new DepartamentoDto
                         {
                             DepDepartamento = d.DepDepartamento,
                             DepDescripcion = d.DepDescripcion,
                             PaPais = d.PaPais                                                  
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Departamento, DepartamentoDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<DepartamentoDto>>(modelo);
        }

      
        [HttpGet("{id}")]
        public IEnumerable<DepartamentoDto> GetDepartamento([FromRoute] long id)
        {
            #region Municipio
            //Consultar las empresas
            List<CiudadMunicipiosDto> LMunicipio = new List<CiudadMunicipiosDto>();

            var Municipio = from cm in _context.CiudadMunicipio

                               where cm.DepDepartamento == id
                               && cm.CimuActivo == true
                               select cm;

            foreach (var obj in Municipio)
            {
                LMunicipio.Add(new CiudadMunicipiosDto
                {
                    CimuId = obj.CimuId,
                    CimuDescripcion = obj.CimuDescripcion,
                    DepDepartamento = obj.DepDepartamento                  
                });
            }
            #endregion Municipio

            var modelo = from d in _context.Departamento

                         where d.DepActivo == true
                         && d.DepDepartamento == id


                         select new DepartamentoDto
                         {
                             DepDepartamento = d.DepDepartamento,
                             DepDescripcion = d.DepDescripcion,
                             PaPais = d.PaPais,                             
                             CiudadMunicipios = LMunicipio
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Departamento, DepartamentoDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<DepartamentoDto>>(modelo);
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDepartamento([FromRoute] long id, [FromBody] Departamento departamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != departamento.DepDepartamento)
            {
                return BadRequest();
            }

            _context.Entry(departamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DepartamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostDepartamento([FromBody] Departamento departamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Departamento.Add(departamento);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DepartamentoExists(departamento.DepDepartamento))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetDepartamento", new { id = departamento.DepDepartamento }, departamento);
        }

        
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteDepartamento([FromRoute] long id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var departamento = await _context.Departamento.FindAsync(id);
        //    if (departamento == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Departamento.Remove(departamento);
        //    await _context.SaveChangesAsync();

        //    return Ok(departamento);
        //}


        private bool DepartamentoExists(long id)
        {
            return _context.Departamento.Any(e => e.DepDepartamento == id);
        }
    }
}