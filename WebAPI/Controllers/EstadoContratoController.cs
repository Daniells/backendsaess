﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoContratoController : ControllerBase
    {
        private readonly SAESSContext _context;

        public EstadoContratoController(SAESSContext context)
        {
            _context = context;
        }

        
        [HttpGet]
        public IEnumerable<EstadoContratoDto> GetEstadoContrato()
        {
            var modelo = from ec in _context.EstadoContrato

                         select ec;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<EstadoContrato, EstadoContratoDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<EstadoContratoDto>>(modelo);
        }


        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetEstadoContrato([FromRoute] short id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var estadoContrato = await _context.EstadoContrato.FindAsync(id);

        //    if (estadoContrato == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(estadoContrato);
        //}

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEstadoContrato([FromRoute] short id, [FromBody] EstadoContrato estadoContrato)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != estadoContrato.EscoId)
            {
                return BadRequest();
            }

            _context.Entry(estadoContrato).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstadoContratoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostEstadoContrato([FromBody] EstadoContrato estadoContrato)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.EstadoContrato.Add(estadoContrato);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEstadoContrato", new { id = estadoContrato.EscoId }, estadoContrato);
        }

       
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteEstadoContrato([FromRoute] short id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var estadoContrato = await _context.EstadoContrato.FindAsync(id);
        //    if (estadoContrato == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.EstadoContrato.Remove(estadoContrato);
        //    await _context.SaveChangesAsync();

        //    return Ok(estadoContrato);
        //}


        private bool EstadoContratoExists(short id)
        {
            return _context.EstadoContrato.Any(e => e.EscoId == id);
        }
    }
}