﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ContratoPorDocumentoPersonaController : ControllerBase
    {
        private readonly SAESSContext _context;

        public ContratoPorDocumentoPersonaController(SAESSContext context)
        {
            _context = context;
        }

        [HttpGet("{Numero}")]
        public IEnumerable<ContratoDto> GetContratoPorDocumentoPersona(string Numero)
        {
            var modelo = from p in _context.Persona
                         join c in _context.Contrato on
                         p.PersId equals c.PersId

                         where p.PersNumDocumento == Numero
                         && p.PersActivo == true
                         && c.ContActivo == true

                         select new ContratoDto
                         {
                             ContId = c.ContId,
                             ClclId = c.ClclId,
                             SectId = c.SectId,
                             EscoId = c.EscoId,
                             ContNumero = c.ContNumero,
                             ContCuota = c.ContCuota,
                             ContDireccion = c.ContDireccion,
                             ContTelefono = c.ContTelefono,
                             ContFechaInstalacion = c.ContFechaInstalacion,
                             PersId = c.PersId,
                             ContFechaCreacion = c.ContFechaCreacion,
                             Responsable = c.ContResponsable,
                             SerId = _context.Factura.
                                     Where(x => x.ContId == c.ContId).FirstOrDefault().SerId                          
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Contrato, ContratoDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ContratoDto>>(modelo);
        }
    }
}
