﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaconzonasController : ControllerBase
    {
        private readonly SAESSContext _context;

        public PersonaconzonasController(SAESSContext context)
        {
            _context = context;
        }
         
        [HttpGet]
        public IEnumerable<Personaconzona> GetPersonaconzona()
        {
            return _context.Personaconzona;
        }

      
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPersonaconzona([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var personaconzona = await _context.Personaconzona.FindAsync(id);

            if (personaconzona == null)
            {
                return NotFound();
            }

            return Ok(personaconzona);
        }

     
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersonaconzona([FromRoute] int id, [FromBody] Personaconzona personaconzona)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != personaconzona.PezoId)
            {
                return BadRequest();
            }

            _context.Entry(personaconzona).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonaconzonaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

       
        [HttpPost]
        public async Task<IActionResult> PostPersonaconzona([FromBody] Personaconzona personaconzona)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Personaconzona.Add(personaconzona);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersonaconzona", new { id = personaconzona.PezoId }, personaconzona);
        }

       
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeletePersonaconzona([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var personaconzona = await _context.Personaconzona.FindAsync(id);
        //    if (personaconzona == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Personaconzona.Remove(personaconzona);
        //    await _context.SaveChangesAsync();

        //    return Ok(personaconzona);
        //}


        private bool PersonaconzonaExists(int id)
        {
            return _context.Personaconzona.Any(e => e.PezoId == id);
        }
    }
}