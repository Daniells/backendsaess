﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using WebAPI.Models;


namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaPorIDController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public PersonaPorIDController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet("{Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public string GetPersonaPorId(string Id)
        {
            // PersonaPorIDData _persona = new PersonaPorIDData(_context, _mapper);
            //var response = _persona.ObtenerPersonaPorID(Id.Trim());
            // return _mapper.Map<IEnumerable<PersonaPorIDResponseDto>>(response); 

            var response = _context.Persona.Where(x => x.PersId.Contains(Id)).FirstOrDefault();

             if (response != null)
              return "{'valido' : 'false'}";
            else
               return "{'valido' : 'true'}"; 
        }
    }
}
