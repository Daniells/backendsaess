﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FacturasPorIDPersonaController : ControllerBase
    {
        private readonly SAESSContext _context;

        public FacturasPorIDPersonaController(SAESSContext context)
        {
            _context = context;
        }


        [HttpGet("{Id}")]
        public IEnumerable<FacturaDto> GetFacturasPorIDPersona(string Id)
        {
                         var modelo = from f in _context.Factura
                                      join ef in _context.Estadofactura on
                                      f.EsfaId equals ef.EsfaId
                                      join fp in _context.FormaPago on
                                      f.FopaId equals fp.FopaId
                                      join s in _context.Serie on
                                      f.SeriId equals s.SeriId
                                      join m in _context.Mes on
                                      f.MesId equals m.MesId

                                      where f.PersId == Id

                         select new FacturaDto
                         {
                             FactId = f.FactId,
                             FactNcontrol = f.FactNcontrol,
                             FactNfactura = f.FactNfactura,                            
                             FactIva = f.FactIva,
                             FactBase = f.FactBase,
                             FactMontoIva = f.FactMontoIva,
                             FactTotal = f.FactTotal,                            
                             TipoDeMovimiento = f.TipoDeMovimiento,
                             SeriId = s.SeriId,
                             SeriLetra = s.SeriLetra,
                             SeriDescripcion = s.SeriDescripcion,
                             MesId = m.MesId,
                             MesNombre = m.MesNombre,
                             FopaId = fp.FopaId,
                             FopaNombre = fp.FopaNombre,
                             EsfaId = ef.EsfaId,
                             EsfaNombre = ef.EsfaNombre                           
                         };


            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Factura, FacturaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<FacturaDto>>(modelo);
        } 
    }
}
