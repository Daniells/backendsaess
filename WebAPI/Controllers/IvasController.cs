﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{   
    [Route("api/[controller]")]
    [ApiController]
    public class IvasController : ControllerBase
    {
        private readonly SAESSContext _context;

        public IvasController(SAESSContext context)
        {
            _context = context;
        }


      
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<IvaDto> GetIva()
        {
            var modelo = from i in _context.Iva
                         where i.IvaActivo == true
                         select i;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Iva, IvaDto>();});
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<IvaDto>>(modelo);
        }


       
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<IvaDto> GetIva([FromRoute] int id)
        {
            var modelo = from i in _context.Iva
                         where i.IvaActivo == true
                         && i.IvaId == id
                         select i;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Iva, IvaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<IvaDto>>(modelo);
        }


        [Authorize]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutIva([FromRoute] int id, ObjectIva iva)
        {  
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            

            if (id != iva.IvaId)            
                return BadRequest();

            Iva objIva = objectoZona(iva);
            objIva.IvaId = iva.IvaId;
            _context.Entry(objIva).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                Iva _iva = await _context.Iva.FindAsync(id);
                if (_iva == null)
                    return NotFound();

                return Ok(iva);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IvaExists(id))                
                    return NotFound();                
                else                
                    throw;                
            }           
        }


        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostIva(ObjectIva iva)
        { 
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Iva objIva = objectoZona(iva);
            _context.Iva.Add(objIva);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (IvaExists(iva.IvaId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetIva", new { id = objIva.IvaId }, objIva);
        }

        [Authorize]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteIva([FromRoute] int id)
        {
            if (!ModelState.IsValid)            
               return BadRequest(ModelState);            

            var iva = await _context.Iva.FindAsync(id);
            if (iva == null || iva.IvaActivo == false)
                return NotFound();


            iva.IvaActivo = false;
            _context.Entry(iva).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(iva);
        }

        
        private bool IvaExists(int id)
        {
            return _context.Iva.Any(e => e.IvaId == id);
        }

        private Iva objectoZona(ObjectIva iva)
        {
            Iva _iva = new Iva();
            _iva.IvaNombre = iva.IvaNombre;
            _iva.IvaDescripcion = iva.IvaDescripcion;
            _iva.IvaRegistradopor = iva.IvaRegistradopor;
            _iva.IvaValor = iva.IvaValor;
            _iva.IvaActivo = true;
            _iva.IvaFechaCreacion = DateTime.UtcNow.Date;

            return _iva;
        }
    }
}