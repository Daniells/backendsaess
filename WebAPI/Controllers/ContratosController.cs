﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ContratosController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public ContratosController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<ResponseContrato> GetContrato()
        {
            var modelo = from c in _context.Contrato
                         join e in _context.EstadoContrato 
                         on c.EscoId equals e.EscoId
                         where c.ContActivo == true
                         
                         select new ResponseContrato
                         {
                             ContId = c.ContId,                             
                             ClclId = c.ClclId,                             
                             ContResponsable = c.ContResponsable,
                             ContNumero = c.ContNumero,
                             ContCuota = c.ContCuota,
                             ContDireccion = c.ContDireccion,
                             ContTelefono = c.ContTelefono,
                             ContFechaInstalacion = c.ContFechaInstalacion,
                             ContFechaCreacion = c.ContFechaCreacion,
                             //Persona = _context.Persona.Where(x => x.PersId == c.PersId).FirstOrDefault(),
                             //Servicio = (from s in _context.Servicio
                             //            join f in _context.Factura on s.SerId equals f.SerId
                             //            join so in _context.SolicitudServicio on f.FactId equals so.FactId
                             //            where s.SerActivo == true && so.SoseActivo == true && c.ContId == so.ContId
                             //            select s).FirstOrDefault(),
                             //Zona = (from s in _context.Sector
                             //        join z in _context.Zona on s.ZonaId equals z.ZonaId                                    
                             //        where s.SectId == c.SectId && c.ContActivo == true
                             //        && s.SectActivo == true && z.ZonaActivo == true
                             //        select z).FirstOrDefault(),
                             //Sector = _context.Sector.Where(x => x.SectId == c.SectId).FirstOrDefault(),
                             Estado = e.EscoNombre,
                             ContGeolocalizacion =  c.ContGeolocalizacion
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Contrato, ResponseContrato>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ResponseContrato>>(modelo);
        }


        [HttpGet("{numero}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<ResponseContrato> GetContrato(string numero)
        {
            var modelo = from c in _context.Contrato                        
                         where c.ContNumero.Contains(numero)
                         && c.ContActivo == true

                         select new ResponseContrato
                         {
                             ContId = c.ContId,                             
                             ClclId = c.ClclId,                             
                             ContResponsable = c.ContResponsable,
                             ContNumero = c.ContNumero,
                             ContCuota = c.ContCuota,
                             ContDireccion = c.ContDireccion,
                             ContTelefono = c.ContTelefono,
                             ContFechaInstalacion = c.ContFechaInstalacion,
                             ContFechaCreacion = c.ContFechaCreacion,
                             ContGeolocalizacion = c.ContGeolocalizacion,
                             Persona = _context.Persona.Where(x => x.PersId == c.PersId).FirstOrDefault(),
                             Servicio = (from s in _context.Servicio
                                         join f in _context.Factura on s.SerId equals f.SerId
                                         join so in _context.SolicitudServicio on f.FactId equals so.FactId
                                         where s.SerActivo == true && so.SoseActivo == true && c.ContId == so.ContId
                                         select s).FirstOrDefault(),
                             Zona = (from s in _context.Sector
                                     join z in _context.Zona on s.ZonaId equals z.ZonaId
                                     where s.SectId == c.SectId && c.ContActivo == true
                                     && s.SectActivo == true && z.ZonaActivo == true
                                     select z).FirstOrDefault(),
                             Sector = _context.Sector.Where(x => x.SectId == c.SectId).FirstOrDefault(),
                             EstadoContrato = _context.EstadoContrato.Where(x => x.EscoId == c.EscoId).FirstOrDefault()
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Contrato, ResponseContrato>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ResponseContrato>>(modelo);
        }


        [HttpPut("{id}")]
        public async Task<IEnumerable<ResponseContrato>> PutContrato([FromRoute] long id, objetoContrato contrato)
        {  
            Contrato objContrato = objectoContrato(contrato);
            actualizarServicio(contrato);
            await _context.SaveChangesAsync();
            objContrato.ContActivo = true;
            objContrato.ContFechaCreacion = DateTime.Now;
            _context.Entry(objContrato).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {           
                   throw ex;                
            }


            var modelo = from c in _context.Contrato
                         where c.ContNumero.Contains(contrato.ContNumero)
                         && c.ContActivo == true
                         && c.ContId == contrato.ContId

                         select new ResponseContrato
                         {
                             ContId = c.ContId,                             
                             ClclId = c.ClclId,
                             ContResponsable = c.ContResponsable,
                             ContNumero = c.ContNumero,
                             ContCuota = c.ContCuota,
                             ContDireccion = c.ContDireccion,
                             ContTelefono = c.ContTelefono,
                             ContFechaInstalacion = c.ContFechaInstalacion,
                             ContFechaCreacion = c.ContFechaCreacion,
                             Persona = _context.Persona.Where(x => x.PersId == c.PersId).FirstOrDefault(),
                             Servicio = (from s in _context.Servicio
                                         join f in _context.Factura on s.SerId equals f.SerId
                                         join so in _context.SolicitudServicio on f.FactId equals so.FactId
                                         where s.SerActivo == true && so.SoseActivo == true && c.ContId == so.ContId
                                         select s).FirstOrDefault(),
                             Zona = (from s in _context.Sector
                                     join z in _context.Zona on s.ZonaId equals z.ZonaId
                                     where s.SectId == c.SectId && c.ContActivo == true
                                     && s.SectActivo == true && z.ZonaActivo == true
                                     select z).FirstOrDefault(),
                             Sector = _context.Sector.Where(x => x.SectId == c.SectId).FirstOrDefault(),
                             EstadoContrato = _context.EstadoContrato.Where(x => x.EscoId == c.EscoId).FirstOrDefault()
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Contrato, ResponseContrato>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ResponseContrato>>(modelo);
        }


        [HttpPost]
        public async Task<IActionResult> PostContrato(ContratoDto _contratoDto)
        {
            Contrato AuxContrato = new Contrato();
             
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
             
            try
            {
              
                 //CONTRATO   
                _contratoDto.Activo = true;
                _contratoDto.ContFechaCreacion = DateTime.Now;                
                _contratoDto.ContNumero = letraAleatoria() + numeroUnico().ToString();
                AuxContrato = _mapper.Map<Contrato>(_contratoDto);
                
                _context.Contrato.Add(AuxContrato);
                await _context.SaveChangesAsync();

                //FACTURA
                var AuxFactura = setFactura(_contratoDto);
                AuxFactura.ContId = AuxContrato.ContId;
                var objectFactura = _context.Factura.Add(AuxFactura);
                await _context.SaveChangesAsync();

                //SOLICITUD DE SERVICIO
                var AuxSolicitud = setSolicitudServicio(_contratoDto);
                AuxSolicitud.FactId = objectFactura.Entity.FactId;
                AuxSolicitud.ContId = AuxContrato.ContId;
                var objectSolicitud = _context.SolicitudServicio.Add(AuxSolicitud);
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException ex)
            { 
                if (ContratoExists(AuxContrato.ContId))
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                else
                    throw ex;
            }           

            return CreatedAtAction("GetContrato", new { id = AuxContrato.ContId }, AuxContrato); 
        }


        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteContrato([FromRoute] long id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var contrato = await _context.Contrato.FindAsync(id);

            if (contrato == null || contrato.ContActivo == false)
                return NotFound();

            contrato.ContActivo = false;
            _context.Entry(contrato).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(contrato);
        }


        private bool ContratoExists(long id)
        {
            return _context.Contrato.Any(e => e.ContId == id);
        }


        private SolicitudServicio setSolicitudServicio(ContratoDto _contratoDto)
        {
            SolicitudServicio response = new SolicitudServicio();
            response.EssoId = 3; //pendiente de instalacion
            response.SoseEmpleado = 0;
            response.SoseNumero = letraAleatoria() + numeroUnico().ToString();
            response.SoseDescripcion = "creación de contrato";
            response.SoseFechaEjecucion = null;
            response.SoseRegistradopor = "user Admin";
            response.SoseFechaCreacion = DateTime.Now;
            response.SoseActivo = true;
            response.SosePrecio = 0;
            response.SoseResponsable = "";
            response.TisoId = 14; //orden de instalacion             
            return response;
        }


        private Factura setFactura(ContratoDto _contratoDto)
        {
            var fecha = DateTime.Today;
            Factura response = new Factura();
            response.SeriId = 1;
            response.FactNcontrol = "1";
            response.FactNfactura = letraAleatoria() + numeroUnico().ToString();
            response.FopaId = _contratoDto.Factura.FirstOrDefault().FopaId;
            response.FactIva = _contratoDto.Factura.FirstOrDefault().FactIva;
            response.FactBase = _contratoDto.Factura.FirstOrDefault().FactBase;
            response.FactMontoIva = response.FactBase * (((decimal)response.FactIva) / 100);
            response.FactTotal = response.FactBase + response.FactMontoIva;
            response.FactRegistradopor = _contratoDto.Registradopor;
            response.FactFechaCreacion = DateTime.Now;
            response.SerId = _contratoDto.Factura.FirstOrDefault().SerId;
            response.TipoDeMovimiento = "1";
            response.MesId = fecha.Month;
            response.PersId = _contratoDto.Factura.FirstOrDefault().PersId;
            response.EsfaId = 2;
            response.FactDescuento = 0;
            response.FactCodigoPago = _contratoDto.Factura.FirstOrDefault().FactCodigoPago;
            response.FactConcepto = _contratoDto.Factura.FirstOrDefault().FactConcepto;
            
            return response;
        }

        private string numeroUnico()
        {
            DateTime today = DateTime.Now;
            long number = today.Year + today.Month + today.Day + today.Hour;
            string response = number.ToString() + today.Minute.ToString() + today.Second.ToString() + today.Millisecond.ToString();
            return response;
        }

        private string letraAleatoria()
        {
            Random rnd = new Random();
            return ((char)rnd.Next('A', 'Z'))  + ((char)rnd.Next('A', 'Z')).ToString();
        }

        private void actualizarServicio(objetoContrato contrato)
        {

            var factura = (from s in _context.SolicitudServicio
                           join f in _context.Factura on
                           s.FactId equals f.FactId
                           where s.SoseId == contrato.SoseId
                           && s.SoseActivo == true                           
                           select f).FirstOrDefault();

            if (factura != null) {
                factura.SerId = contrato.Servicio.SerId;
                _context.Entry(factura).State = EntityState.Modified;
            }
        }

        private Contrato objectoContrato(objetoContrato contrato) {

            Contrato objContrato = new Contrato();

            objContrato.ContId = contrato.ContId;
            objContrato.ClclId = contrato.ClclId;
            objContrato.SectId = contrato.SectId;
            objContrato.EscoId = contrato.EscoId;
            objContrato.ContNumero = contrato.ContNumero;
            objContrato.ContCuota = contrato.ContCuota;
            objContrato.ContDireccion = contrato.ContDireccion;
            objContrato.ContTelefono = contrato.ContTelefono;            
            objContrato.ContRegistradopor = contrato.ContRegistradopor;
            objContrato.ContFechaCreacion = DateTime.Now;
            objContrato.PersId = contrato.PersId;             
            objContrato.ContActivo = true;
            objContrato.ContResponsable = contrato.ContResponsable;

            return objContrato;
        }
    }
}