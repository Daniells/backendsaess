﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoSolicitudController : ControllerBase
    {
        private readonly SAESSContext _context;

        public EstadoSolicitudController(SAESSContext context)
        {
            _context = context;
        }

 
        [HttpGet]
        public IEnumerable<EstadoSolicitudDto> GetEstadoSolicitud()
        {
            var modelo = from es in _context.EstadoSolicitud

                         where es.EssoActivo == true

                         select es;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<EstadoSolicitud, EstadoSolicitudDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<EstadoSolicitudDto>>(modelo);
        }


        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetEstadoSolicitud([FromRoute] short id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var estadoSolicitud = await _context.EstadoSolicitud.FindAsync(id);

        //    if (estadoSolicitud == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(estadoSolicitud);
        //}

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEstadoSolicitud([FromRoute] short id, [FromBody] EstadoSolicitud estadoSolicitud)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != estadoSolicitud.EssoId)
            {
                return BadRequest();
            }

            _context.Entry(estadoSolicitud).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstadoSolicitudExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostEstadoSolicitud([FromBody] EstadoSolicitud estadoSolicitud)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.EstadoSolicitud.Add(estadoSolicitud);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEstadoSolicitud", new { id = estadoSolicitud.EssoId }, estadoSolicitud);
        }


        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteEstadoSolicitud([FromRoute] short id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var estadoSolicitud = await _context.EstadoSolicitud.FindAsync(id);
        //    if (estadoSolicitud == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.EstadoSolicitud.Remove(estadoSolicitud);
        //    await _context.SaveChangesAsync();

        //    return Ok(estadoSolicitud);
        //}


        private bool EstadoSolicitudExists(short id)
        {
            return _context.EstadoSolicitud.Any(e => e.EssoId == id);
        }
    }
}