﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresasController : ControllerBase
    {
        private readonly SAESSContext _context;

        public EmpresasController(SAESSContext context)
        {
            _context = context;
        }
               
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<EmpresaDto> GetEmpresa()
        {
            var modelo = from e in _context.Empresa
                         join cm in _context.CiudadMunicipio on
                         e.CimuId equals cm.CimuId
                         join d in _context.Departamento on
                         cm.DepDepartamento equals d.DepDepartamento

                         where e.EmprEstado == true
                         && cm.CimuActivo == true
                         && d.DepActivo == true

                         select new EmpresaDto
                         {
                             EmprId = e.EmprId,
                             EmprNit = e.EmprNit,
                             EmprNombre = e.EmprNombre,
                             EmprDireccion = e.EmprDireccion,
                             EmprEmail = e.EmprEmail,
                             EmprTelefono = e.EmprTelefono,
                             EmprTelefax = e.EmprTelefax,
                             EmprFechaInicio = e.EmprFechaInicio,
                             EmprCiudad = cm.CimuDescripcion,
                             IdEmprCiudad = cm.CimuId,
                             Departamento = d.DepDescripcion,
                             IdDepartamento = d.DepDepartamento
                         };
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Empresa, EmpresaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<EmpresaDto>>(modelo);
        }
        

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<EmpresaDto> GetEmpresa(int id)
        {
            #region personas            
            List<PersonaDto> LPersona = new List<PersonaDto>();
            var Persona = from e in _context.Empresa
                          join ecp in _context.Empresaconpersona on
                          e.EmprId equals ecp.EmprId
                          join p in _context.Persona on
                          ecp.PersId equals p.PersId
                          join s in _context.Sexo on
                          p.SexId equals s.SexId
                          join tp in _context.Tipopersona on
                          p.TipeId equals tp.TipeId
                          join td in _context.Tipodocumento on
                          p.TidoId equals td.TidoId
                          join ec in _context.EstadoCivil on
                          p.EsciId equals ec.EsciId

                          where e.EmprId == id
                          && e.EmprEstado == true
                          && ecp.EmcpeActivo == true
                          && p.PersActivo == true
                          && s.SexActivo == true
                          && tp.TipeActivo == true
                          && td.TidoActivo == true

                          select new { p, s, td, tp, ec };

            foreach (var ob in Persona)
            {
                LPersona.Add(new PersonaDto
                {
                    PersId = ob.p.PersId,
                    EsciId = ob.p.EsciId,
                    EsciNombre = ob.ec.EsciNombre,
                    PersIscontribuyente = ob.p.PersIscontribuyente,
                    PersNombre = ob.p.PersNombre,
                    PersApellido = ob.p.PersApellido,
                    PersFechanacimiento = ob.p.PersFechanacimiento,
                    PersDireccion = ob.p.PersDireccion,
                    PersTelefono = ob.p.PersTelefono,
                    PersCelular = ob.p.PersCelular,
                    PersEmail = ob.p.PersEmail,
                    PersSexo = ob.s.SexNombre,
                    tipodocumento = ob.td.TidoNombre,
                    TipoPersona = ob.tp.TipeNombre,
                    PersNumDocumento = ob.p.PersNumDocumento,
                    SexId = ob.s.SexId,
                    tidoId = ob.td.TidoId,
                    TipeId = ob.tp.TipeId
                });
            }
            #endregion personas


            var modelo = from e in _context.Empresa
                         join cm in _context.CiudadMunicipio on
                         e.CimuId equals cm.CimuId
                         join d in _context.Departamento on
                         cm.DepDepartamento equals d.DepDepartamento

                         where e.EmprId == id
                         && e.EmprEstado == true
                         && cm.CimuActivo == true
                         && d.DepActivo == true

                         select new EmpresaDto
                         {
                             EmprId = e.EmprId,
                             EmprNit = e.EmprNit,
                             EmprNombre = e.EmprNombre,
                             EmprDireccion = e.EmprDireccion,
                             EmprEmail = e.EmprEmail,
                             EmprTelefono = e.EmprTelefono,
                             EmprTelefax = e.EmprTelefax,
                             EmprFechaInicio = e.EmprFechaInicio,
                             EmprCiudad = cm.CimuDescripcion,
                             IdEmprCiudad = cm.CimuId,
                             Departamento = d.DepDescripcion,
                             IdDepartamento = d.DepDepartamento,
                             Persona = LPersona
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Empresa, EmpresaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<EmpresaDto>>(modelo);
        }


        [Authorize]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutEmpresa([FromRoute] short id, ObjectEmpresa empresa)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            if (id != empresa.EmprId)
                return BadRequest();

            Empresa objEmpresa = objectEmpresa(empresa);
            objEmpresa.EmprId = empresa.EmprId;
            _context.Entry(objEmpresa).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

               Empresa _empresa = await _context.Empresa.FindAsync(id);
                if (_empresa == null)
                    return NotFound();

                return Ok(_empresa);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpresaExists(id))
                    return NotFound();
                else
                    throw;
            }
        }


        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostEmpresa(ObjectEmpresa empresa)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            Empresa objEmpresa = objectEmpresa(empresa);
            _context.Empresa.Add(objEmpresa);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EmpresaExists(empresa.EmprId))
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                else
                    throw;
            }

            return CreatedAtAction("GetEmpresa", new { id = objEmpresa.EmprId }, objEmpresa);
        }


        [Authorize]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteEmpresa([FromRoute] short id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var empresa = await _context.Empresa.FindAsync(id);
            if (empresa == null || empresa.EmprEstado == false)
                return NotFound();

            empresa.EmprEstado = false;
            _context.Entry(empresa).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(empresa);
        }



        private bool EmpresaExists(short id)
        {
            return _context.Empresa.Any(e => e.EmprId == id);
        }


        private Empresa objectEmpresa(ObjectEmpresa empresa)
        {
            Empresa _Empresa = new Empresa();
            _Empresa.EmprNit = empresa.EmprNit;
            _Empresa.EmprNombre = empresa.EmprNombre;
            _Empresa.EmprDireccion = empresa.EmprDireccion;
            _Empresa.EmprEmail = empresa.EmprEmail;
            _Empresa.EmprTelefono = empresa.EmprTelefono;
            _Empresa.EmprTelefax = empresa.EmprTelefax;
            _Empresa.EmprRegistradopor = empresa.EmprRegistradopor;
            _Empresa.EmprCiudad = empresa.EmprCiudad;
            _Empresa.CimuId = empresa.CimuId;
            _Empresa.EmprEstado = true;
            _Empresa.EmprFechaInicio = DateTime.UtcNow.Date;
            _Empresa.EmprFechaCreacion = DateTime.UtcNow.Date;

            return _Empresa;
        }
    }
}