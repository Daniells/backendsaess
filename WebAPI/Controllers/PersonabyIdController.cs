﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonabyIdController : ControllerBase
    {
        private readonly SAESSContext _context;

        public PersonabyIdController(SAESSContext context)
        {
            _context = context;
        }

        [HttpGet("{persId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<PersonaDto> GetPersonabyId(string persId)
        {
            #region Empresa           
            List<EmpresaDto> LEmpresa = new List<EmpresaDto>();
            var Empresa = from p in _context.Persona
                          join ecp in _context.Empresaconpersona on
                          p.PersId equals ecp.PersId
                          join e in _context.Empresa on
                          ecp.EmprId equals e.EmprId

                          where p.PersId == persId
                          && p.PersActivo == true
                          && ecp.EmcpeActivo == true
                          && e.EmprEstado == true

                          select e;

            foreach (var obj in Empresa)
            {
                LEmpresa.Add(new EmpresaDto
                {
                    EmprId = obj.EmprId,
                    EmprNit = obj.EmprNit,
                    EmprNombre = obj.EmprNombre
                });
            }
            #endregion Empresa

            #region Zona   
            List<ZonaDto> LZona = new List<ZonaDto>();
            var Zona = from p in _context.Persona
                       join pz in _context.Personaconzona on
                       p.PersId equals pz.PersId
                       join z in _context.Zona on
                       pz.ZonaId equals z.ZonaId

                       where p.PersId == persId
                       select z;

            foreach (var ob in Zona)
            {
                LZona.Add(new ZonaDto
                {
                    ZonaId = ob.ZonaId,
                    ZonaNombre = ob.ZonaNombre
                });
            }
            #endregion

            #region Rol   
            List<RolDto> LRol = new List<RolDto>();
            var Rol = from p in _context.Persona
                      join pr in _context.PersonaRol on
                       p.PersId equals pr.PersId
                      join r in _context.Rol on
                      pr.RolId equals r.RolId

                      where p.PersId == persId
                      select r;

            foreach (var ob in Rol)
            {
                LRol.Add(new RolDto
                {
                    RolId = ob.RolId,
                    RolNombre = ob.RolNombre
                });
            }
            #endregion

            #region Persona                        
            var modelo = from p in _context.Persona
                         join ec in _context.EstadoCivil on
                         p.EsciId equals ec.EsciId
                         join tp in _context.Tipopersona on
                         p.TipeId equals tp.TipeId
                         join td in _context.Tipodocumento on
                         p.TidoId equals td.TidoId
                         join s in _context.Sexo on
                         p.SexId equals s.SexId


                         where p.PersId == persId

                         select new PersonaDto
                         {
                             PersId = p.PersId,
                             PersNumDocumento = p.PersNumDocumento,
                             PersIscontribuyente = p.PersIscontribuyente,
                             PersNombre = p.PersNombre,
                             PersApellido = p.PersApellido,
                             PersFechanacimiento = p.PersFechanacimiento,
                             PersSexo = s.SexNombre,
                             PersDireccion = p.PersDireccion,
                             PersTelefono = p.PersTelefono,
                             PersCelular = p.PersCelular,
                             PersEmail = p.PersEmail,
                             EsciId = p.EsciId,
                             EsciNombre = ec.EsciNombre,
                             TipoPersona = tp.TipeNombre,
                             tipodocumento = td.TidoNombre,
                             PersPassword = p.PersPassword,
                             Empresa = LEmpresa,
                             Zona = LZona,
                             Rol = LRol,
                             SexId = s.SexId,
                             TipeId = tp.TipeId,
                             tidoId = td.TidoId,
                             ClclId = p.ClclId
                         };
            #endregion

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Persona, PersonaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<PersonaDto>>(modelo);
        }
    }
}