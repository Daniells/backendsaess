﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public MenuController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
                
        [HttpGet]
        public async Task<IEnumerable<MenuResponseDto>> GetMenu()
        {
            var response = await (from me in _context.Menu                                 
                                  where me.Activo == true                               
                                  select new MenuDto
                                  {
                                      Idmenu = me.Idmenu,
                                      Menu = me.Nombre,                                     
                                      Modulo = _context.Modulo
                                                       .Where(x => x.Idmenu == me.Idmenu
                                                           && x.ModuEstado == true)
                                                       .ToList()                                    
                                  }).ToListAsync();

            return _mapper.Map<IEnumerable<MenuResponseDto>>(response);
        }        
    }
}