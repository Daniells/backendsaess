﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]     
    [ApiController]
    public class FacturasController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public FacturasController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
         

        [HttpGet("{numeroContrato}/{nombrePersona}/{numeroDocumento}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<FacturaFiltrosResponseDto> GetFactura(string numeroContrato, string nombrePersona, string numeroDocumento)
        {
            FacturaData facturaFiltro = new FacturaData(_context, _mapper);
            List<FacturaFiltrosResponseDto> _null = new List<FacturaFiltrosResponseDto>();

            if (numeroContrato != "null")
            {
                var response = facturaFiltro.obtenerFacturasPorNumeroContrato(numeroContrato.Trim());
                return _mapper.Map<IEnumerable<FacturaFiltrosResponseDto>>(response);

            }
            else if (nombrePersona != "null")
            {
                var response = facturaFiltro.obtenerFacturasPorNombrePersona(nombrePersona.Trim());
                return _mapper.Map<IEnumerable<FacturaFiltrosResponseDto>>(response);
            }
            else if (numeroDocumento != "null")
            {
                var response = facturaFiltro.obtenerFacturasPorNumeroDocumento(numeroDocumento.Trim());
                return _mapper.Map<IEnumerable<FacturaFiltrosResponseDto>>(response);
            }
              
            return _null;
        }
              

        [HttpPut("{id}")]
        public FacturaFiltrosResponseDto PutFactura([FromRoute] long id, [FromBody] Factura factura)
        {
            FacturaData facturaFiltro = new FacturaData(_context, _mapper);
            List<FacturaFiltrosResponseDto> _null = new List<FacturaFiltrosResponseDto>();
            FacturasPorFiltrosDto facturaaux = new FacturasPorFiltrosDto();

            Factura objfactura = setFactura(factura);
            objfactura.FactId = factura.FactId;         
            _context.Entry(objfactura).State = EntityState.Modified;

            try
            {
                
                var item = (from f in _context.Factura
                            where f.FactId == objfactura.FactId
                            && f.EsfaId != 3
                            select f).FirstOrDefault();
                 
                facturaaux.FactId = item.FactId;
                facturaaux.FactNcontrol = item.FactNcontrol;
                facturaaux.FactNfactura = item.FactNfactura;
                facturaaux.FactIva = item.FactIva;
                facturaaux.FactBase = item.FactBase;
                facturaaux.FactMontoIva = item.FactMontoIva;
                facturaaux.FactTotal = item.FactTotal;
                facturaaux.FactFechaCreacion = item.FactFechaCreacion;
                facturaaux.TipoDeMovimiento = item.TipoDeMovimiento;
                facturaaux.FactDescuento = item.FactDescuento;
                facturaaux.FactCodigoPago = item.FactCodigoPago;
                facturaaux.FactConcepto = item.FactConcepto;
                facturaaux.PersId = item.PersId;
                facturaaux.estado = (from ef in _context.Estadofactura where ef.EsfaId == item.EsfaId select ef).FirstOrDefault();
                facturaaux.formaPago = (from fp in _context.FormaPago where fp.FopaId == item.FopaId && fp.FopaActivo == true select fp).FirstOrDefault();
                facturaaux.mes = (from m in _context.Mes where m.MesId == item.MesId && m.MesActivo == true select m).FirstOrDefault();
                facturaaux.serie = (from s in _context.Serie where s.SeriId == item.SeriId && s.SeriActivo == true select s).FirstOrDefault();
                facturaaux.contrato = (from c in _context.Contrato where c.PersId.Contains(item.PersId) && c.ContActivo == true select c).FirstOrDefault();
                facturaaux.servicio = (from s in _context.Servicio
                                       where item.SerId == s.SerId
                                       && s.SerActivo == true
                                       select s).ToList();


                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {                   
                throw ex;                
            }
 
           
            return _mapper.Map<FacturaFiltrosResponseDto>(facturaaux);
        }
           
        
        [HttpPost]
        public async Task<IActionResult> PostFactura([FromBody] Factura factura)
        {            
            FacturasPorFiltrosDto facturaaux = new FacturasPorFiltrosDto();

            Factura objfactura = setFactura(factura);
            _context.Factura.Add(objfactura);
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                if (FacturaExists(objfactura.FactId))                
                    return new StatusCodeResult(StatusCodes.Status409Conflict);                
                else                
                    throw ex;                
            }
             
            var item = (from f in _context.Factura
                        where f.FactId == objfactura.FactId
                        && f.EsfaId != 3
                        select f).FirstOrDefault();

            facturaaux.FactNcontrol = item.FactNcontrol;
            facturaaux.FactNfactura = item.FactNfactura;
            facturaaux.FactIva = item.FactIva;
            facturaaux.FactBase = item.FactBase;
            facturaaux.FactMontoIva = item.FactMontoIva;
            facturaaux.FactTotal = item.FactTotal;
            facturaaux.FactFechaCreacion = item.FactFechaCreacion;
            facturaaux.TipoDeMovimiento = item.TipoDeMovimiento;
            facturaaux.FactDescuento = item.FactDescuento;
            facturaaux.FactCodigoPago = item.FactCodigoPago;
            facturaaux.FactConcepto = item.FactConcepto;
            facturaaux.PersId = item.PersId;
            facturaaux.estado = (from ef in _context.Estadofactura where ef.EsfaId == item.EsfaId select ef).FirstOrDefault();
            facturaaux.formaPago = (from fp in _context.FormaPago where fp.FopaId == item.FopaId && fp.FopaActivo == true select fp).FirstOrDefault();
            facturaaux.mes = (from m in _context.Mes where m.MesId == item.MesId && m.MesActivo == true select m).FirstOrDefault();
            facturaaux.serie = (from s in _context.Serie where s.SeriId == item.SeriId && s.SeriActivo == true select s).FirstOrDefault();
            facturaaux.contrato = (from c in _context.Contrato where c.PersId.Contains(item.PersId) && c.ContActivo == true select c).FirstOrDefault();
            facturaaux.servicio = (from s in _context.Servicio
                                   where item.SerId == s.SerId
                                   && s.SerActivo == true
                                   select s).ToList();

            if (objfactura == null)
                return BadRequest();

            return Ok(item);
        }
            

        private bool FacturaExists(long id)
        {
            return _context.Factura.Any(e => e.FactId == id);
        }

        private string letraAleatoria()
        {
            Random rnd = new Random();
            return ((char)rnd.Next('A', 'Z')) + ((char)rnd.Next('A', 'Z')).ToString();
        }

        private long numeroUnico(int numero)
        {
            DateTime today = DateTime.UtcNow.Date;
            long number = today.Year + today.Month + today.Day + today.Hour + today.Minute + today.Second + today.Millisecond + numero;
            return number;
        }

        private Factura setFactura(Factura _contratoDto)
        {
            Factura response = new Factura();
            response.SeriId = 1;
            response.FactNcontrol = "1";
            response.FactNfactura = letraAleatoria() + numeroUnico(6).ToString();
            response.FopaId = _contratoDto.FopaId;
            response.FactIva = _contratoDto.FactIva;
            response.FactBase = _contratoDto.FactBase;
            response.FactMontoIva = response.FactBase * (((decimal)response.FactIva) / 100);
            response.FactTotal = response.FactBase + response.FactMontoIva;
            response.FactRegistradopor = _contratoDto.FactRegistradopor;
            response.FactFechaCreacion = DateTime.UtcNow.Date;
            response.SerId = _contratoDto.SerId;
            response.TipoDeMovimiento = "1";
            response.MesId = _contratoDto.MesId;
            response.PersId = _contratoDto.PersId;
            response.EsfaId = _contratoDto.EsfaId;
            response.FactDescuento = _contratoDto.FactDescuento;
            response.FactCodigoPago = _contratoDto.FactCodigoPago;
            response.FactConcepto = _contratoDto.FactConcepto;
            response.ContId = _contratoDto.ContId;

            return response;
        }
    }
}