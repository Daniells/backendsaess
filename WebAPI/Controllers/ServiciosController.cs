﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiciosController : ControllerBase
    {
        private readonly SAESSContext _context;

        public ServiciosController(SAESSContext context)
        {
            _context = context;
        }


       
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<ServicioDto> GetServicio()
        {
            var modelo = from s in _context.Servicio
                        
                         where s.SerActivo == true                         

                         select s;


            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Servicio, ServicioDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ServicioDto>>(modelo);
        }


   
        [HttpGet("{Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<ServicioDto> GetServicio(int Id)
        {            
            var modelo = from s in _context.Servicio  

                         where s.SerId == Id
                         && s.SerActivo == true                        

                         select s;

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Servicio, ServicioDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ServicioDto>>(modelo);
        }


        [Authorize]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutServicio([FromRoute] int id, ObjectoServicio servicio)
        {   
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            

            if (id != servicio.SerId)            
                return BadRequest();

            Servicio objServicio = objectoServicio(servicio);
            objServicio.SerId = servicio.SerId;
            _context.Entry(objServicio).State = EntityState.Modified;


            try
            {
                await _context.SaveChangesAsync();

               Servicio _servicio = await _context.Servicio.FindAsync(id);
                if (_servicio == null)
                    return NotFound();

                return Ok(_servicio);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServicioExists(id))                
                    return NotFound();                
                else                
                    throw;                
            }           
        }


        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostServicio(ObjectoServicio servicio)
        { 
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);

            Servicio objServicio = objectoServicio(servicio);
            _context.Servicio.Add(objServicio);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ServicioExists(servicio.SerId))                
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                else               
                    throw;               
            }

            return CreatedAtAction("GetServicio", new { id = objServicio.SerId }, objServicio);
        }


        [Authorize]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteServicio([FromRoute] int id)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            

            var servicio = await _context.Servicio.FindAsync(id);

            if (servicio == null || servicio.SerActivo == false)
                return NotFound();

            servicio.SerActivo = false;
            _context.Entry(servicio).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(servicio);
        }
        

        private bool ServicioExists(int id)
        {
            return _context.Servicio.Any(e => e.SerId == id);
        }

        private Servicio objectoServicio(ObjectoServicio servicio)
        {
            Servicio _servicio = new Servicio();
            _servicio.SerNombre = servicio.SerNombre;
            _servicio.SerDescripcion = servicio.SerDescripcion;
            _servicio.SerCodigo = servicio.SerCodigo;
            _servicio.SerRegistradopor = servicio.SerRegistradopor;
            _servicio.EmprId = servicio.EmprId;
            _servicio.SerValor = servicio.SerValor;
            _servicio.SerActivo = true;
            _servicio.SerFechaCreacion = DateTime.UtcNow.Date;

            return _servicio;
        }
    }
}