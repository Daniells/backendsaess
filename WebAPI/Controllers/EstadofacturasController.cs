﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadofacturasController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public EstadofacturasController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<EstadoFacturaDto> GetEstadofactura()
        {           
            var response = (from e in _context.Estadofactura
                            where e.EsfaActivo == true
                            select e).ToList();
            return _mapper.Map<IEnumerable<EstadoFacturaDto>>(response);
        }         
    }
}