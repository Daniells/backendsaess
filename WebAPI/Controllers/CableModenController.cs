﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CableModenController : ControllerBase
    {
        private readonly SAESSContext _context;

        public CableModenController(SAESSContext context)
        {
            _context = context;
        }

         
        [HttpGet]
        public IEnumerable<CableModen> GetCableModen()
        {
            return _context.CableModen;
        }

        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCableModen([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cableModen = await _context.CableModen.FindAsync(id);

            if (cableModen == null)
            {
                return NotFound();
            }

            return Ok(cableModen);
        }

        
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCableModen([FromRoute] int id, [FromBody] CableModen cableModen)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cableModen.CabModenId)
            {
                return BadRequest();
            }

            _context.Entry(cableModen).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CableModenExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostCableModen([FromBody] CableModen cableModen)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.CableModen.Add(cableModen);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CableModenExists(cableModen.CabModenId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCableModen", new { id = cableModen.CabModenId }, cableModen);
        }

        
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteCableModen([FromRoute] string id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var cableModen = await _context.CableModen.FindAsync(id);
        //    if (cableModen == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.CableModen.Remove(cableModen);
        //    await _context.SaveChangesAsync();

        //    return Ok(cableModen);
        //}

        private bool CableModenExists(int id)
        {
            return _context.CableModen.Any(e => e.CabModenId == id);
        }
    }
}