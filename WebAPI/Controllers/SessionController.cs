﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionController : ControllerBase
    {
        private readonly SAESSContext _context;

        private readonly TokenService _tokenService;

        public SessionController(SAESSContext context, TokenService tokenService)
        {
            _context = context;
            _tokenService = tokenService;
        }

        [HttpPost]
        [Route("Login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<string> PostSession(SessionDto sessionDto)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);

            if(UsuarioInactivo(sessionDto))
                return BadRequest("Usuario esta inactivo.");

            if (!ValidarDatosUsuario(sessionDto)) 
                return BadRequest("Usuario/Contraseña Inválidos.");
            
            try
            {
                return _tokenService.GenerarToken(sessionDto);
            }
            catch (DbUpdateException ex)
            {               
                  throw ex;                
            }         
        }
            
        private bool ValidarDatosUsuario(SessionDto sessionDto)
        {
            var response = (from p in _context.Persona
                            join ep in _context.Empresaconpersona on
                            p.PersId equals ep.PersId
                            join e in _context.Empresa on
                            ep.EmprId equals e.EmprId
                            where p.PersActivo == true
                            && ep.EmcpeActivo == true
                            && e.EmprEstado == true
                            && p.PersLogin.Trim() == sessionDto.User.Trim()
                            && p.PersPassword.Trim() == sessionDto.Password.Trim()
                            && e.EmprId == sessionDto.EmpresaId
                            select p).FirstOrDefault();

            if (response is null)
                return false;
            else
                return true;
        }

        private bool UsuarioInactivo(SessionDto sessionDto)
        {
            return _context.Persona
                            .Any(x => x.PersLogin.Trim() == sessionDto.User.Trim()
                                && x.PersPassword.Trim() == sessionDto.Password.Trim()
                                && x.PersActivo == false);
        }
    }
}