﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaisController : ControllerBase
    {
        private readonly SAESSContext _context;

        public PaisController(SAESSContext context)
        {
            _context = context;
        }

     
        [HttpGet]
        public IEnumerable<PaisDto> GetPais()
        {
            var modelo = from p in _context.Pais
                         
                         select new PaisDto
                         {
                             PaPais = p.PaPais,
                             PaDescripcion = p.PaDescripcion,
                             PaAbreviatura = p.PaAbreviatura                                                
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Pais, PaisDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<PaisDto>>(modelo);
        }

              
        [HttpGet("{id}")]
        public IEnumerable<PaisDto> GetPais([FromRoute] long id)
        {
            #region Departamentos
            //Consultar las empresas
            List<DepartamentoDto> LDepartamento = new List<DepartamentoDto>();

            var Departamento = from d in _context.Departamento
                                  
                               where d.PaPais == id                              
                               && d.DepActivo == true
                               select d;

            foreach (var obj in Departamento)
            {
                LDepartamento.Add(new DepartamentoDto
                {
                    DepDepartamento = obj.DepDepartamento,
                    DepDescripcion = obj.DepDescripcion,                  
                    PaPais = obj.PaPais
                });
            }
            #endregion Departamentos

            var modelo = from p in _context.Pais

                         where p.PaActivo == true
                         && p.PaPais == id

                        
                          select new PaisDto
                          {   
                              PaPais = p.PaPais,
                              PaDescripcion = p.PaDescripcion,
                              PaAbreviatura = p.PaAbreviatura,                             
                              Departamento = LDepartamento
                          };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Pais, PaisDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<PaisDto>>(modelo);
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPais([FromRoute] long id, [FromBody] Pais pais)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pais.PaPais)
            {
                return BadRequest();
            }

            _context.Entry(pais).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaisExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostPais([FromBody] Pais pais)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Pais.Add(pais);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PaisExists(pais.PaPais))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPais", new { id = pais.PaPais }, pais);
        }

     
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeletePais([FromRoute] long id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var pais = await _context.Pais.FindAsync(id);
        //    if (pais == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Pais.Remove(pais);
        //    await _context.SaveChangesAsync();

        //    return Ok(pais);
        //}

        private bool PaisExists(long id)
        {
            return _context.Pais.Any(e => e.PaPais == id);
        }
    }
}