﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ConfiguracionController : ControllerBase
    {
        private readonly SAESSContext _context;

        public ConfiguracionController(SAESSContext context)
        {
            _context = context;
        }
        
        [HttpGet("{parametro}")]
        public async Task<IActionResult> GetConfiguracion([FromRoute] string parametro)
        {
            var configuracion = await _context.Configuracion
                                    .Where(x => x.Id.ToString() == parametro || x.Nombre == parametro)
                                    .FirstOrDefaultAsync();

            if (configuracion == null)            
                return NotFound();            

            return Ok(configuracion);
        }         
    }
}