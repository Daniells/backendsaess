﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InventariosController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public InventariosController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
               

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<InventarioDto> GetInventario()
        {
            var inventario = from i in _context.Inventario
                             join mi in _context.MovimientoInventario on
                             i.InveId equals mi.InveId
                             join p in _context.Producto on
                             mi.ProdId equals p.ProdId

                             where p.ProdActivo == true
                             && mi.MoinActivo == true
                             && i.InveActivo == true

                             select new InventarioDto
                             {
                                 InveId = i.InveId,
                                 InveTipo = i.InveTipo,
                                 InveDescripcion = i.InveDescripcion,
                                 MoinFechaCreacion = mi.MoinFechaCreacion,
                                 ProdId = p.ProdId,
                                 ProdNombre = p.ProdNombre,
                                 MoinId = mi.MoinId,
                                 MoinDescripcion = mi.MoinDescripcion,
                                 MoinCantidad = mi.MoinCantidad
                             };

            return _mapper.Map<IEnumerable<InventarioDto>>(inventario);
        }
               

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<InventarioDto> GetInventario([FromRoute] long id)
        {                
            var inventario = from i in _context.Inventario
                             join mi in _context.MovimientoInventario on
                             i.InveId equals mi.InveId
                             join p in _context.Producto on
                             mi.ProdId equals p.ProdId

                             where p.ProdActivo == true
                             && mi.MoinActivo == true
                             && i.InveActivo == true
                             && i.InveId == id

                             select new InventarioDto
                             {
                                 InveId = i.InveId,
                                 InveTipo = i.InveTipo,
                                 InveDescripcion = i.InveDescripcion,
                                 MoinFechaCreacion = mi.MoinFechaCreacion,
                                 ProdId = p.ProdId,
                                 ProdNombre = p.ProdNombre,
                                 MoinId = mi.MoinId,
                                 MoinDescripcion = mi.MoinDescripcion,
                                 MoinCantidad = mi.MoinCantidad
                             };

            return _mapper.Map<IEnumerable<InventarioDto>>(inventario);            
        }

       
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutInventario(MovimientoInventarioDto minventarioDto)
        {   
            /*** metodo para actualizar el inventario
                 se consulta el inventario y el movimiento 
                 si ambos existen se actualiza sino respuesta NotFound
             ***/

            if (!ModelState.IsValid)            
                return BadRequest(ModelState);            
             
            Inventario inventario = _context.Inventario.Where(x => x.InveId == minventarioDto.InveId).FirstOrDefault();
            if (inventario is null) {
                return NotFound();
            } else {
                
                MovimientoInventario MoviInventario = _context.MovimientoInventario
                                                       .Where(x => x.MoinId == minventarioDto.MoinId).FirstOrDefault();
                if (MoviInventario is null) {
                    return NotFound();
                } else {
                    inventario.InveTipo = minventarioDto.InveTipo;
                    inventario.InveDescripcion = minventarioDto.InveDescripcion;
                    inventario.InveRegistradopor = minventarioDto.InveRegistradopor;
                    inventario.BodeId = minventarioDto.BodeId;
                    inventario.InveFechaCreacion = DateTime.Now;
                    _context.Entry(inventario).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    MoviInventario.ProdId = minventarioDto.ProdId;
                    MoviInventario.MoinCantidad = minventarioDto.MoinCantidad;
                    MoviInventario.MoinRegistradopor = minventarioDto.InveRegistradopor;
                    MoviInventario.ContId = minventarioDto.ContId;
                    MoviInventario.MoinFechaCreacion = DateTime.Now;
                    MoviInventario.MoinDescripcion = minventarioDto.MoinDescripcion;                    
                    MoviInventario.MoinPrecioCompra = minventarioDto.MoinPrecioCompra;
                    _context.Entry(inventario).State = EntityState.Modified;

                    minventarioDto.InveId = inventario.InveId;
                    minventarioDto.MoinId = MoviInventario.MoinId;
                }        
            }
             
            try
            {
                await _context.SaveChangesAsync();               
            }
            catch (DbUpdateConcurrencyException ex)
            {                
                throw ex;                
            }

            return CreatedAtAction("GetInventario", new { id = minventarioDto.InveId }, minventarioDto);
        }

       
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostInventario([FromBody] MovimientoInventarioDto minventarioDto)
        {    
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);

            Inventario inventario = setInventario(minventarioDto);
            _context.Inventario.Add(inventario);
            await _context.SaveChangesAsync();

            MovimientoInventario MoviInventario = setMovimientoInventario(minventarioDto);
            MoviInventario.InveId = inventario.InveId;
            _context.MovimientoInventario.Add(MoviInventario);

            try
            {
                await _context.SaveChangesAsync();
                minventarioDto.InveId = inventario.InveId;
                minventarioDto.MoinId = MoviInventario.MoinId;
            }
            catch (DbUpdateException)
            {
                if (InventarioExists(inventario.InveId))                
                    return new StatusCodeResult(StatusCodes.Status409Conflict);                
                else                
                    throw;                
            }

            return CreatedAtAction("GetInventario", new { id = minventarioDto.InveId }, minventarioDto);
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInventario([FromRoute] long id)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);            

            var movimientoInventario = await _context.MovimientoInventario.FindAsync(id);
            if (movimientoInventario == null)            
                return NotFound();

            movimientoInventario.MoinActivo = false;
            _context.Entry(movimientoInventario).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(movimientoInventario);
        }


        private bool InventarioExists(long id)
        {
            return _context.Inventario.Any(e => e.InveId == id);
        }

        private Inventario setInventario(MovimientoInventarioDto m_inventarioDto)
        {
            Inventario inventario = new Inventario();
            inventario.InveTipo = m_inventarioDto.InveTipo;
            inventario.InveDescripcion = m_inventarioDto.InveDescripcion;
            inventario.InveRegistradopor = m_inventarioDto.InveRegistradopor;
            inventario.BodeId = m_inventarioDto.BodeId;
            inventario.InveFechaCreacion = DateTime.Now;
            inventario.InveActivo = true;
            return inventario;
        }

        private MovimientoInventario setMovimientoInventario(MovimientoInventarioDto m_inventarioDto)
        {
            MovimientoInventario minventario = new MovimientoInventario();
            minventario.ProdId = m_inventarioDto.ProdId;
            minventario.MoinCantidad = m_inventarioDto.MoinCantidad;
            minventario.MoinRegistradopor = m_inventarioDto.InveRegistradopor;
            minventario.ContId = m_inventarioDto.ContId;
            minventario.MoinFechaCreacion = DateTime.Now;
            minventario.MoinDescripcion = m_inventarioDto.MoinDescripcion;
            minventario.MoinActivo = true;
            minventario.MoinPrecioCompra = m_inventarioDto.MoinPrecioCompra;
            return minventario;
        }
    }
}