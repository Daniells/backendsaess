﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using WebAPI.Data;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonasPorRolController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public PersonasPorRolController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        public IEnumerable<PersonaPorRolDto> GetPersonasPorRol(int id)
        {
            PersonasPorRolData persona = new PersonasPorRolData(_context, _mapper);

            var response = persona.ObtenerPersonasPorRol(id);

            return _mapper.Map<IEnumerable<PersonaPorRolDto>>(response); 
        }
    }
}