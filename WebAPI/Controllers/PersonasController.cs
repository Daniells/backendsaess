﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonasController : ControllerBase
    {
        private readonly SAESSContext _context;

        public PersonasController(SAESSContext context)
        {
            _context = context;
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<PersonaDto> GetPersona()
        {
            var Persona = from p in _context.Persona
                          join s in _context.Sexo on
                          p.SexId equals s.SexId
                          join tp in _context.Tipopersona on
                          p.TipeId equals tp.TipeId
                          join td in _context.Tipodocumento on
                          p.TidoId equals td.TidoId
                          join ec in _context.EstadoCivil on
                          p.EsciId equals ec.EsciId

                          where p.PersActivo == true
                          && s.SexActivo == true
                          && tp.TipeActivo == true
                          && td.TidoActivo == true

                          select new PersonaDto {
                              PersId = p.PersId,
                              EsciId = p.EsciId,
                              EsciNombre = ec.EsciNombre,
                              PersIscontribuyente = p.PersIscontribuyente,
                              PersNombre = p.PersNombre,
                              PersApellido = p.PersApellido,
                              PersFechanacimiento = p.PersFechanacimiento,
                              PersDireccion = p.PersDireccion,
                              PersTelefono = p.PersTelefono,
                              PersCelular = p.PersCelular,
                              PersEmail = p.PersEmail,
                              PersSexo = s.SexNombre,
                              tipodocumento = td.TidoNombre,
                              TipoPersona = tp.TipeNombre,
                              PersNumDocumento = p.PersNumDocumento,
                              SexId = s.SexId,
                              tidoId = td.TidoId,
                              TipeId = tp.TipeId
                          };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Persona, PersonaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<PersonaDto>>(Persona);
        }



        [HttpGet("{NroDocumento}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<PersonaDto> GetPersona(string NroDocumento)
        {               
            #region Persona                        
            var modelo = from p in _context.Persona
                         join ec in _context.EstadoCivil on
                         p.EsciId equals ec.EsciId
                         join tp in _context.Tipopersona on
                         p.TipeId equals tp.TipeId
                         join td in _context.Tipodocumento on
                         p.TidoId equals td.TidoId
                         join s in _context.Sexo on
                         p.SexId equals s.SexId

                         where (p.PersNumDocumento.Contains(NroDocumento) ||
                               p.PersNombre.Contains(NroDocumento) ||
                               p.PersApellido.Contains(NroDocumento))

                         select new PersonaDto
                         {
                             PersId = p.PersId,
                             PersNumDocumento = p.PersNumDocumento,
                             PersIscontribuyente = p.PersIscontribuyente,
                             PersNombre = p.PersNombre,
                             PersApellido = p.PersApellido,
                             PersFechanacimiento = p.PersFechanacimiento,
                             PersSexo = s.SexNombre,
                             PersDireccion = p.PersDireccion,
                             PersTelefono = p.PersTelefono,
                             PersCelular = p.PersCelular,
                             PersEmail = p.PersEmail,
                             EsciId = p.EsciId,
                             EsciNombre = ec.EsciNombre,
                             TipoPersona = tp.TipeNombre,
                             tipodocumento = td.TidoNombre,
                             PersPassword = p.PersPassword,                            
                             SexId = s.SexId,
                             TipeId= tp.TipeId,
                             tidoId = td.TidoId,
                             ClclId = p.ClclId
                         };
            #endregion
            
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Persona, PersonaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<PersonaDto>>(modelo);
        }
         


        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutPersona([FromRoute] string id, [FromBody] Persona persona)
        {          
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);            

            if (id != persona.PersId)            
                return BadRequest("Id persona no corresponde");

            if (!PersonaExists(persona.PersId))
                return BadRequest("Id persona no existe");

            if (!PersonaUpdateExists(persona.PersId, persona.PersNumDocumento))
                return BadRequest("Numero de documento registrado en otro usuario.");

            deletePersonaRol(persona, _context);
            await _context.SaveChangesAsync();
            guardarPersonaRol(persona, _context);
            await _context.SaveChangesAsync();
            deleteEmpresaconpersona(persona, _context);
            await _context.SaveChangesAsync();
            guardarEmpresapersona(persona, _context);
            await _context.SaveChangesAsync();
                        
            var getPersona = _context.Persona.Where(e => e.PersId == id).FirstOrDefault();
            _context.Entry(setUpdatePersona(getPersona, persona)).State = EntityState.Modified;   
            
            try
            {
                await _context.SaveChangesAsync();

                persona = await _context.Persona.FindAsync(id);
                if (persona == null)
                    return NotFound();

                return Ok(persona);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!PersonaExists(id))                
                    return NotFound();                
                else     
                    throw ex;                
            }          
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostPersona(Persona persona)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (PersonaExists(persona.PersId) || PersonaExistsbyNumDocumento(persona.PersNumDocumento))
                return BadRequest("Persona Id/NumeroDocumento ya ha sido registrado.");

            foreach (var element in persona.Empresaconpersona)
            {
                element.EmprFechaRegistro = DateTime.UtcNow.Date;
            }

            foreach (var element in persona.PersonaRol)
            {
                element.PeroFechaCreacion = DateTime.UtcNow.Date;
            }

            persona.PersFechaCreacion = DateTime.UtcNow.Date;            
            persona.PersActivo = true;
            _context.Persona.Add(persona);
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                if (PersonaExists(persona.PersId)){                   
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }else{
                    
                    throw ex;
                }
            }

            return CreatedAtAction("GetPersona", new { id = persona.PersId }, persona);
        }


        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeletePersona([FromRoute] string id)
        {

            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            

            var persona = await _context.Persona.FindAsync(id);
            if (persona == null || persona.PersActivo == false)
                return NotFound();


            persona.PersActivo = false;
            _context.Entry(persona).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(persona);
        }


        private bool PersonaExists(string id)
        {
            return _context.Persona.Any(e => e.PersId == id);
        }

        private bool PersonaExistsbyNumDocumento(string numDocumento)
        {
            return _context.Persona.Any(e => e.PersNumDocumento == numDocumento && e.PersActivo == true);
        }

        private bool PersonaUpdateExists(string id, string numDocumento)
        {
            //Metodo validar que no se permita actualizar el numero_documento  si existe
            var persona = _context.Persona.Where(e => e.PersId == id).FirstOrDefault();
            if (persona != null)
            {
                var objectNumDoc = _context.Persona.Where(e => e.PersNumDocumento == numDocumento && e.PersActivo == true).FirstOrDefault();
                if (objectNumDoc != null)
                {
                    if (persona.PersId == objectNumDoc.PersId)
                        return true; //existe num_documento le pertenece al mismo usuario
                    else
                        return false; //existe num_documento le pertenece a otro usuario
                }
                else {                    
                    return true; //numero de documento es valido no existe
                }                   
            }
            else {
                return false; //no existe el usuario para actualizarlo
            }
        }

        private void guardarPersonaRol(Persona persona, SAESSContext context)
        {
            foreach (var element in persona.PersonaRol)
            {
                PersonaRol personaRol = new PersonaRol();
                personaRol.PersId = element.PersId;
                personaRol.RolId = element.RolId;
                personaRol.PeroRegistradopor = element.PeroRegistradopor;
                personaRol.PeroEstado = true;
                personaRol.PeroFechaCreacion = DateTime.UtcNow.Date;
                context.PersonaRol.Add(personaRol);                
            }
        }

        private Persona objectoPersona(objectoPersona persona)
        {
            PersonaRol personaRol = new PersonaRol();

            foreach (var element in persona.PersonaRol)
            {
                personaRol.PeroId = element.PeroId;
                personaRol.PersId = element.PersId;
                personaRol.RolId = element.RolId;
                personaRol.PeroRegistradopor = element.PeroRegistradopor;
                personaRol.PeroEstado = true;
                personaRol.PeroFechaCreacion = DateTime.UtcNow.Date;
            }

            Persona _Persona = new Persona();
            _Persona.PersId = persona.PersId;
            _Persona.EsciId = persona.EsciId;
            _Persona.PersIscontribuyente = persona.PersIscontribuyente;
            _Persona.PersNombre = persona.PersNombre;
            _Persona.PersApellido = persona.PersApellido;
            _Persona.PersFechanacimiento = persona.PersFechanacimiento;
            _Persona.PersDireccion = persona.PersDireccion;
            _Persona.PersTelefono = persona.PersTelefono;
            _Persona.PersCelular = persona.PersCelular;
            _Persona.PersEmail = persona.PersEmail;
            _Persona.PersLogin = persona.PersLogin;
            _Persona.PersPassword = persona.PersPassword;
            _Persona.PersRegistradopor = persona.PersRegistradopor;
            _Persona.ClclId = persona.ClclId;
            _Persona.TidoId = persona.TidoId;
            _Persona.TipeId = persona.TipeId;
            _Persona.SexId = persona.SexId;
            _Persona.PersNumDocumento = persona.PersNumDocumento;            
            _Persona.PersActivo = true;
            _Persona.PersFechaCreacion = DateTime.UtcNow.Date;

            return _Persona;
        }

        private void deletePersonaRol(Persona persona, SAESSContext context)
        {
            var DeletepersonaRol = _context.PersonaRol.Where(x => x.PersId == persona.PersId);
            foreach (var element in DeletepersonaRol)
            {
                context.PersonaRol.Remove(element);                
            }            
        }

        private void guardarEmpresapersona(Persona persona, SAESSContext context)
        {
            foreach (var element in persona.Empresaconpersona)
            {
                Empresaconpersona empresaconpersona = new Empresaconpersona();
                empresaconpersona.PersId = element.PersId;
                empresaconpersona.EmprId = element.EmprId;
                empresaconpersona.EmprRegistradopor = element.EmprRegistradopor;
                empresaconpersona.EmcpeActivo = true;
                empresaconpersona.EmprActivo = true;
                empresaconpersona.EmprFechaRegistro = DateTime.UtcNow.Date;
                context.Empresaconpersona.Add(empresaconpersona);               
            }
        }

        private void deleteEmpresaconpersona(Persona persona, SAESSContext context)
        {
            var DeleteEmpresaconpersona = _context.Empresaconpersona.Where(x => x.PersId == persona.PersId);
            foreach (var element in DeleteEmpresaconpersona)
            {
                context.Empresaconpersona.Remove(element);               
            }             
        }

        private Persona setUpdatePersona(Persona persona, Persona PersonaDto)
        {                       
            persona.PersId = PersonaDto.PersId;
            persona.EsciId = PersonaDto.EsciId;
            persona.PersIscontribuyente = PersonaDto.PersIscontribuyente;
            persona.PersNombre = PersonaDto.PersNombre;
            persona.PersApellido = PersonaDto.PersApellido;
            persona.PersFechanacimiento = PersonaDto.PersFechanacimiento;
            persona.SexId = PersonaDto.SexId;
            persona.PersCelular = PersonaDto.PersCelular;
            persona.PersDireccion = PersonaDto.PersDireccion;
            persona.PersTelefono = PersonaDto.PersTelefono;
            persona.PersEmail = PersonaDto.PersEmail;
            persona.PersLogin = PersonaDto.PersLogin;
            persona.PersPassword = PersonaDto.PersPassword;                  
            persona.ClclId = PersonaDto.ClclId;
            persona.TidoId = PersonaDto.TidoId;
            persona.TipeId = PersonaDto.TipeId;
            persona.PersNumDocumento = PersonaDto.PersNumDocumento;

            return persona;
        }
    }
}