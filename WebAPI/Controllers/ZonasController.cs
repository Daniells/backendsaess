﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ZonasController : ControllerBase
    {
        private readonly SAESSContext _context;

        public ZonasController(SAESSContext context)
        {
            _context = context;
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<ZonaDto> GetZona()
        {
            var modelo = from z in _context.Zona
                         join cm in _context.CiudadMunicipio on
                         z.CimuId equals cm.CimuId
                         join dpto in _context.Departamento on
                         cm.DepDepartamento equals dpto.DepDepartamento

                         where cm.CimuActivo == true
                            && dpto.DepActivo == true
                            && z.ZonaActivo == true

                         select new ZonaDto
                         {
                             ZonaId = z.ZonaId,
                             ZonaNombre = z.ZonaNombre,
                             ZonaDescripcion = z.ZonaDescripcion,
                             CimuId = cm.CimuId,
                             CimuDescripcion = cm.CimuDescripcion,
                             DepDepartamento = dpto.DepDepartamento,
                             DepDescripcion = dpto.DepDescripcion
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Zona, ZonaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ZonaDto>>(modelo);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<ZonaDto>  GetZona([FromRoute] int id)
        {
            #region sector           
            List<SectorDto> LSector = new List<SectorDto>();
            var Sector = from s in _context.Sector                     

                          where s.ZonaId == id
                          && s.SectActivo == true
                          select s;

            foreach (var ob in Sector)
            {
                LSector.Add(new SectorDto
                {
                    SectId = ob.SectId,                   
                    SectRecargo = ob.SectRecargo,
                    SectNombre = ob.SectNombre,
                    SectDescripcion = ob.SectDescripcion,
                    SectAbreviatura = ob.SectAbreviatura
                });
            }
            #endregion sector


            var modelo = from z in _context.Zona
                         join cm in _context.CiudadMunicipio on
                         z.CimuId equals cm.CimuId
                         join dpto in _context.Departamento on
                         cm.DepDepartamento equals dpto.DepDepartamento

                         where z.ZonaId == id
                         && cm.CimuActivo == true
                         && dpto.DepActivo == true
                         && z.ZonaActivo == true

                         select new ZonaDto {
                             ZonaId = z.ZonaId,
                             ZonaNombre = z.ZonaNombre,
                             ZonaDescripcion = z.ZonaDescripcion,
                             CimuId = cm.CimuId,
                             CimuDescripcion = cm.CimuDescripcion,
                             DepDepartamento = dpto.DepDepartamento,
                             DepDescripcion = dpto.DepDescripcion,
                             Sector = LSector
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Zona, ZonaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ZonaDto>>(modelo);
        }

        [Authorize]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ZonaDto>> PutZona([FromRoute] int id, ObjectZona zona)
        {  
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            
            if (id != zona.ZonaId)            
                return BadRequest();

            Zona objZona = objectoZona(zona);
            objZona.ZonaId = zona.ZonaId;
            _context.Entry(objZona).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync(); 

                #region sector                 
                List<SectorDto> LSector = new List<SectorDto>();
                var Sector = from s in _context.Sector

                             where s.ZonaId == id
                             && s.SectActivo == true
                             select s;

                foreach (var ob in Sector)
                {
                    LSector.Add(new SectorDto
                    {
                        SectId = ob.SectId,
                        SectRecargo = ob.SectRecargo,
                        SectNombre = ob.SectNombre,
                        SectDescripcion = ob.SectDescripcion,
                        SectAbreviatura = ob.SectAbreviatura
                    });
                }
                #endregion sector


                var modelo = from z in _context.Zona
                             join cm in _context.CiudadMunicipio on
                             z.CimuId equals cm.CimuId
                             join dpto in _context.Departamento on
                             cm.DepDepartamento equals dpto.DepDepartamento

                             where z.ZonaId == id
                             && cm.CimuActivo == true
                             && dpto.DepActivo == true
                             && z.ZonaActivo == true

                             select new ZonaDto
                             {
                                 ZonaId = z.ZonaId,
                                 ZonaNombre = z.ZonaNombre,
                                 ZonaDescripcion = z.ZonaDescripcion,
                                 CimuId = cm.CimuId,
                                 CimuDescripcion = cm.CimuDescripcion,
                                 DepDepartamento = dpto.DepDepartamento,
                                 DepDescripcion = dpto.DepDescripcion,
                                 Sector = LSector
                             };

                var config = new MapperConfiguration(cfg => { cfg.CreateMap<Zona, ZonaDto>(); });
                IMapper mapper = config.CreateMapper();
                var responseZona = mapper.Map<IEnumerable<ZonaDto>>(modelo);

                if (responseZona == null)
                    return NotFound();

                return Ok(responseZona);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!ZonaExists(id))                
                    return NotFound();                
                else                
                    throw ex;                
            }       
        }

        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ZonaDto>> PostZona(ObjectZona zona)
        {  
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);

            Zona objZona = objectoZona(zona);
            _context.Zona.Add(objZona);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ZonaExists(zona.ZonaId))                
                    return new StatusCodeResult(StatusCodes.Status409Conflict);                
                else                
                    throw;                
            }
             
           
            return CreatedAtAction("GetZona", new { id = objZona.ZonaId }, objZona);             
        }

        [Authorize]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteZona([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var zona = await _context.Zona.FindAsync(id);

            if (zona == null || zona.ZonaActivo == false)
                return NotFound();

            zona.ZonaActivo = false;
            _context.Entry(zona).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(zona);
        }


        private bool ZonaExists(int id)
        {
            return _context.Zona.Any(e => e.ZonaId == id);
        }

        private Zona objectoZona(ObjectZona zona)
        {
            Zona _Zona = new Zona();
            _Zona.ZonaNombre = zona.ZonaNombre;
            _Zona.ZonaDescripcion = zona.ZonaDescripcion;
            _Zona.ZonaRegistradopor = zona.ZonaRegistradopor;
            _Zona.CimuId = zona.CimuId;
            _Zona.ZonaActivo = true;
            _Zona.ZonaFechaCreacion = DateTime.UtcNow.Date;

            return _Zona;
        }
    }
}