﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GrupoproductoController : ControllerBase
    {
        private readonly SAESSContext _context;

        public GrupoproductoController(SAESSContext context)
        {
            _context = context;
        }
       
        [HttpGet]
        public IEnumerable<Grupoproducto> GetGrupoproducto()
        {
            return _context.Grupoproducto;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostGrupoproducto([FromBody] Grupoproducto grupoproducto)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);            

            _context.Grupoproducto.Add(grupoproducto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGrupoproducto", new { id = grupoproducto.Idgrupo }, grupoproducto);
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGrupoproducto([FromRoute] int id)
        {
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);            

            var grupoproducto = await _context.Grupoproducto.FindAsync(id);
            if (grupoproducto == null)            
                return NotFound();            

            _context.Grupoproducto.Remove(grupoproducto);
            await _context.SaveChangesAsync();

            return Ok(grupoproducto);
        }

        private bool GrupoproductoExists(int id)
        {
            return _context.Grupoproducto.Any(e => e.Idgrupo == id);
        }
    }
}