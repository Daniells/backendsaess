﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaProductosController : ControllerBase
    {
        private readonly SAESSContext _context;

        public CategoriaProductosController(SAESSContext context)
        {
            _context = context;
        }

       
        [HttpGet]
        public IEnumerable<CategoriaProducto> GetCategoriaProducto()
        {          
            return _context.CategoriaProducto;         
        }

     
        [HttpGet("{id}")]
        public IEnumerable<ProductoDto> GetCategoriaProducto([FromRoute] short id)
        {
            var modelo = from p in _context.Producto    
                         join i in _context.Iva on
                         p.IvaId equals i.IvaId

                         where p.CaprId == id
                         && p.ProdActivo == true

                         select new ProductoDto
                         {
                             ProdId = p.ProdId,                             
                             ProdCodigo = p.ProdCodigo,
                             ProdNombre= p.ProdNombre,                             
                             ProdPrecioVenta = p.ProdPrecioVenta,
                             ProdCminimo = p.ProdCminimo                            
                         };


            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Producto, ProductoDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ProductoDto>>(modelo);
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategoriaProducto([FromRoute] short id, [FromBody] CategoriaProducto categoriaProducto)
        {  
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categoriaProducto.CaprId)
            {
                return BadRequest();
            }

            _context.Entry(categoriaProducto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoriaProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostCategoriaProducto([FromBody] CategoriaProducto categoriaProducto)
        { 
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.CategoriaProducto.Add(categoriaProducto);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CategoriaProductoExists(categoriaProducto.CaprId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCategoriaProducto", new { id = categoriaProducto.CaprId }, categoriaProducto);
        }

       
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteCategoriaProducto([FromRoute] short id)
        //{  
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var categoriaProducto = await _context.CategoriaProducto.FindAsync(id);
        //    if (categoriaProducto == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.CategoriaProducto.Remove(categoriaProducto);
        //    await _context.SaveChangesAsync();

        //    return Ok(categoriaProducto);
        //}


        private bool CategoriaProductoExists(short id)
        {
            return _context.CategoriaProducto.Any(e => e.CaprId == id);
        }
    }
}