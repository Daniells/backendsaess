﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaRolController : ControllerBase
    {
        private readonly SAESSContext _context;

        public PersonaRolController(SAESSContext context)
        {
            _context = context;
        }
         

        [HttpGet("{id}")]
        public IEnumerable<PersonaDto> GetPersonaRol([FromRoute] int id)
        {

            var Persona = from p in _context.Persona
                          join pr in _context.PersonaRol on
                          p.PersId equals pr.PersId
                          join r in _context.Rol on
                          pr.RolId equals r.RolId

                          where
                          r.RolId == id
                          && p.PersActivo == true
                          && r.RolEstado == true
                          && pr.PeroEstado == true

                          select p;
                          

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Persona, PersonaDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<PersonaDto>>(Persona);             
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersonaRol([FromRoute] int id, [FromBody] PersonaRol personaRol)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != personaRol.PeroId)
            {
                return BadRequest();
            }

            _context.Entry(personaRol).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonaRolExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

 
        [HttpPost]
        public async Task<IActionResult> PostPersonaRol([FromBody] PersonaRol personaRol)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PersonaRol.Add(personaRol);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersonaRol", new { id = personaRol.PeroId }, personaRol);
        }
         
        private bool PersonaRolExists(int id)
        {
            return _context.PersonaRol.Any(e => e.PeroId == id);
        }
    }
}