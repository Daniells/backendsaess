﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MesController : ControllerBase
    {
        private readonly SAESSContext _context;
        private readonly IMapper _mapper;

        public MesController(SAESSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
         
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<MesesDto> GetMes()
        {
           
            var response = (from m in _context.Mes
                            where m.MesActivo == true
                            select m).ToList();
            return _mapper.Map<IEnumerable<MesesDto>>(response);          
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMes([FromRoute] int id, [FromBody] Mes mes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mes.MesId)
            {
                return BadRequest();
            }

            _context.Entry(mes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostMes([FromBody] Mes mes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Mes.Add(mes);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMes", new { id = mes.MesId }, mes);
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var mes = await _context.Mes.FindAsync(id);
            if (mes == null)
            {
                return NotFound();
            }

            _context.Mes.Remove(mes);
            await _context.SaveChangesAsync();

            return Ok(mes);
        }

        private bool MesExists(int id)
        {
            return _context.Mes.Any(e => e.MesId == id);
        }
    }
}