﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly SAESSContext _context;
        
        public RolesController(SAESSContext context)
        {
            _context = context;
        }
          
        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<Rol> GetRol()
        {   
            return _context.Rol.Where(x => x.RolEstado == true).OrderBy(item => item.RolNombre);
        }
                    
        
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<RolDto> GetRol(int id)
        {
            #region Funcionalidad           
            List<FuncionalidadDto> LFuncionalidad = new List<FuncionalidadDto>();
            var Funcionalidad = from r in _context.Rol
                          join fr in _context.FuncionalidadRol on
                          r.RolId equals fr.RolId
                          join f in _context.Funcionalidad on
                          fr.FuncId equals f.FuncId
                          join m in _context.Modulo on
                          f.ModuId equals m.ModuId

                          where r.RolId == id
                          && r.RolEstado == true
                          && fr.FuroEstado == true
                          && f.FuncEstado == true
                          && m.ModuEstado == true
                          select new { f, m};

            foreach (var obj in Funcionalidad)
            {
                LFuncionalidad.Add(new FuncionalidadDto
                {
                    FuncId = obj.f.FuncId,
                    ModuId = obj.m.ModuId,
                    ModuNombre = obj.m.ModuNombre,
                    FuncNombre = obj.f.FuncNombre,
                    FuncPosicion = obj.f.FuncPosicion,
                    FuncUrl = obj.f.FuncUrl,
                    FuncEstado = obj.f.FuncEstado
                });
            }
            #endregion

            var modelo = from r in _context.Rol
                         where r.RolId == id

                         select new RolDto
                         {
                             RolId = r.RolId,
                             RolEstado = r.RolEstado,
                             RolNombre = r.RolNombre,
                             RolDescripcion = r.RolDescripcion,
                             Funcionalidad = LFuncionalidad
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Rol, RolDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<RolDto>>(modelo);
        }

        [Authorize]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PutRol([FromRoute] int id, RolObjectDto rol)
        { 

            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            

            if (id != rol.RolId)            
                return BadRequest();

            Rol objRol = objectoRol(rol);
            objRol.RolId = rol.RolId;
            _context.Entry(objRol).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                Rol _rol = await _context.Rol.FindAsync(id);
                if (_rol == null)
                    return NotFound();

                return Ok(_rol);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RolExists(id))                
                    return NotFound();                
                else                
                    throw;                
            }           
        }

        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostRol(RolObjectDto rol)
        { 
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);

            Rol objRol = objectoRol(rol);
            _context.Rol.Add(objRol);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RolExists(rol.RolId))                
                    return new StatusCodeResult(StatusCodes.Status409Conflict);                
                else
                    throw;                
            }

            return CreatedAtAction("GetRol", new { id = objRol.RolId }, objRol);
        }

        [Authorize]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteRol([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var rol = await _context.Rol.FindAsync(id);

            if (rol == null || rol.RolEstado == false)
                return NotFound();

            rol.RolEstado = false;
            _context.Entry(rol).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(rol);
        }


        private bool RolExists(int id)
        {
            return _context.Rol.Any(e => e.RolId == id);
        }

        private Rol objectoRol(RolObjectDto rol)
        {
            Rol _rol = new Rol();
            _rol.RolNombre = rol.RolNombre;
            _rol.RolDescripcion = rol.RolDescripcion;
            _rol.RolRegistradopor = rol.RolRegistradopor;
            _rol.RolEstado = true;
            _rol.RolFechaCreacion = DateTime.UtcNow.Date;

            return _rol;
        }
    }
}