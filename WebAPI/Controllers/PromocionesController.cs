﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromocionesController : ControllerBase
    {
        private readonly SAESSContext _context;

        public PromocionesController(SAESSContext context)
        {
            _context = context;
        }

         
        [HttpGet]
        public IEnumerable<Promociones> GetPromociones()
        {
            return _context.Promociones;
        }

       
        [HttpGet("{Codigo}")]
        public IEnumerable<PromocionDto> GetPromociones([FromRoute] string Codigo)
        {
            var modelo = from p in _context.Promociones
                         
                         where p.PromCodigo == Codigo
                         && p.PromActivo == true

                         select new PromocionDto
                         {
                             PromId = p.PromId,
                             PromNumDocumento = p.PromNumDocumento,
                             PromNombre = p.PromNombre,
                             PromApellido = p.PromApellido,
                             PromTelefono = p.PromTelefono,
                             PromDescuento = p.PromDescuento,
                             PromCodigo = p.PromCodigo
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Promociones, PromocionDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<PromocionDto>>(modelo);
        }


        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPromociones([FromRoute] int id, [FromBody] Promociones promociones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != promociones.PromId)
            {
                return BadRequest();
            }

            _context.Entry(promociones).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PromocionesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostPromociones([FromBody] Promociones promociones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Promociones.Add(promociones);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PromocionesExists(promociones.PromId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPromociones", new { id = promociones.PromId }, promociones);
        }

        


        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeletePromociones([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var promociones = await _context.Promociones.FindAsync(id);
        //    if (promociones == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Promociones.Remove(promociones);
        //    await _context.SaveChangesAsync();

        //    return Ok(promociones);
        //}


        private bool PromocionesExists(int id)
        {
            return _context.Promociones.Any(e => e.PromId == id);
        }
    }
}