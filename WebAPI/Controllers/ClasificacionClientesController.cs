﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models; 
using WebAPI.Dto;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClasificacionClientesController : ControllerBase
    {
        private readonly SAESSContext _context;

        public ClasificacionClientesController(SAESSContext context)
        {
            _context = context;
        }
            
        [HttpGet]
        public IEnumerable<ClasificacionCliente> GetClasificacionCliente()
        {    // GET: api/ClasificacionClientes
            return _context.ClasificacionCliente;
        }
              
        [HttpGet("{id}")]
        public async Task<IActionResult> GetClasificacionCliente([FromRoute] short id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var clasificacionCliente = await _context.ClasificacionCliente.FindAsync(id);

            if (clasificacionCliente == null)
            {
                return NotFound();
            }

            return Ok(clasificacionCliente);
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClasificacionCliente([FromRoute] short id, [FromBody] ClasificacionCliente clasificacionCliente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != clasificacionCliente.ClclId)
            {
                return BadRequest();
            }

            _context.Entry(clasificacionCliente).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClasificacionClienteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostClasificacionCliente([FromBody] ClasificacionCliente clasificacionCliente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ClasificacionCliente.Add(clasificacionCliente);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetClasificacionCliente", new { id = clasificacionCliente.ClclId }, clasificacionCliente);
        }
               
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteClasificacionCliente([FromRoute] short id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var clasificacionCliente = await _context.ClasificacionCliente.FindAsync(id);
        //    if (clasificacionCliente == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.ClasificacionCliente.Remove(clasificacionCliente);
        //    await _context.SaveChangesAsync();

        //    return Ok(clasificacionCliente);
        //}

        private bool ClasificacionClienteExists(short id)
        {
            return _context.ClasificacionCliente.Any(e => e.ClclId == id);
        }
    }
}