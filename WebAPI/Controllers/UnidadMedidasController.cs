﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnidadMedidasController : ControllerBase
    {
        private readonly SAESSContext _context;

        public UnidadMedidasController(SAESSContext context)
        {
            _context = context;
        }

      
        [HttpGet]
        public IEnumerable<UnidadMedida> GetUnidadMedida()
        {   
            return _context.UnidadMedida;
        }

      
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUnidadMedida([FromRoute] short id)
        {  
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            
            var unidadMedida = await _context.UnidadMedida.FindAsync(id);

            if (unidadMedida == null)            
                return NotFound();            

            return Ok(unidadMedida);
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUnidadMedida([FromRoute] short id, [FromBody] UnidadMedida unidadMedida)
        {  
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);
            

            if (id != unidadMedida.UnmeId)            
                return BadRequest();           

            _context.Entry(unidadMedida).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UnidadMedidaExists(id))                
                    return NotFound();                
                else                
                    throw;                
            }

            return NoContent();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostUnidadMedida([FromBody] UnidadMedida unidadMedida)
        {  
            if (!ModelState.IsValid)            
                return BadRequest(ModelState);            

            _context.UnidadMedida.Add(unidadMedida);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UnidadMedidaExists(unidadMedida.UnmeId))                
                    return new StatusCodeResult(StatusCodes.Status409Conflict);                
                else                
                    throw;                
            }

            return CreatedAtAction("GetUnidadMedida", new { id = unidadMedida.UnmeId }, unidadMedida);
        }
         
        private bool UnidadMedidaExists(short id)
        {
            return _context.UnidadMedida.Any(e => e.UnmeId == id);
        }
    }
}