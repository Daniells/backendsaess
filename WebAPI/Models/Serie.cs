﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Serie
    {
        public Serie()
        {
            Factura = new HashSet<Factura>();
        }

        public int SeriId { get; set; }
        public string SeriLetra { get; set; }
        public string SeriDescripcion { get; set; }
        public string SeriRegistradopor { get; set; }
        public DateTime SeriFechaCreacion { get; set; }
        public int SeriCorrelativo { get; set; }
        public bool? SeriActivo { get; set; }

        public ICollection<Factura> Factura { get; set; }
    }
}
