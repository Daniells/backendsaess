﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Sector
    {
        public int SectId { get; set; }
        public int ZonaId { get; set; }
        public string SectAbreviatura { get; set; }
        public string SectNombre { get; set; }
        public string SectDescripcion { get; set; }
        public float SectRecargo { get; set; }
        public string SectRegistradopor { get; set; }
        public DateTime SectFechaCreacion { get; set; }
        public bool? SectActivo { get; set; }

        public Zona Zona { get; set; }
    }
}
