﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Personaconzona
    {
        public int PezoId { get; set; }
        public bool? PezoActivo { get; set; }
        public DateTime? PezoFechaRegistro { get; set; }
        public string PezoRegistradopor { get; set; }
        public string PersId { get; set; }
        public int? ZonaId { get; set; }

        public Persona Pers { get; set; }
        public Zona Zona { get; set; }
    }
}
