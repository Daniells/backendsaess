﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Sexo
    {
        public Sexo()
        {
            Persona = new HashSet<Persona>();
        }

        public int SexId { get; set; }
        public string SexNombre { get; set; }
        public string SexDescripcion { get; set; }
        public bool? SexActivo { get; set; }
        public string SexRegistradopor { get; set; }
        public DateTime? SexFechaRegistro { get; set; }

        public ICollection<Persona> Persona { get; set; }
    }
}
