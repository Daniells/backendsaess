﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class CiudadMunicipio
    {
        public CiudadMunicipio()
        {
            Empresa = new HashSet<Empresa>();
            Zona = new HashSet<Zona>();
        }

        public long CimuId { get; set; }
        public string CimuDescripcion { get; set; }
        public long DepDepartamento { get; set; }
        public bool CimuActivo { get; set; }
        public string CimuRegistradopor { get; set; }
        public DateTime CimuFechaCreacion { get; set; }

        public Departamento DepDepartamentoNavigation { get; set; }
        public ICollection<Empresa> Empresa { get; set; }
        public ICollection<Zona> Zona { get; set; }
    }
}
