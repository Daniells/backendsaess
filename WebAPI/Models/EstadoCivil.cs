﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class EstadoCivil
    {
        public EstadoCivil()
        {
            Persona = new HashSet<Persona>();
        }

        public short EsciId { get; set; }
        public string EsciNombre { get; set; }
        public string EsciDescripcion { get; set; }
        public string EsciRegistradopor { get; set; }
        public DateTime EsciFechaCreacion { get; set; }
        public bool? EsciActivo { get; set; }

        public ICollection<Persona> Persona { get; set; }
    }
}
