﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Funcionalidad
    {
        public Funcionalidad()
        {
            FuncionalidadRol = new HashSet<FuncionalidadRol>();
        }

        public int FuncId { get; set; }
        public int ModuId { get; set; }
        public short? FuncPosicion { get; set; }
        public string FuncNombre { get; set; }
        public string FuncDescripcion { get; set; }
        public string FuncUrl { get; set; }
        public bool FuncEstado { get; set; }
        public string FuncRegistradopor { get; set; }
        public DateTime FuncFechaCreacion { get; set; }

        public Modulo Modu { get; set; }
        public ICollection<FuncionalidadRol> FuncionalidadRol { get; set; }
    }
}
