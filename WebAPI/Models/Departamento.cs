﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Departamento
    {
        public Departamento()
        {
            CiudadMunicipio = new HashSet<CiudadMunicipio>();
        }

        public long DepDepartamento { get; set; }
        public string DepDescripcion { get; set; }
        public long PaPais { get; set; }
        public bool DepActivo { get; set; }
        public string DepRegistradopor { get; set; }
        public DateTime DepFechaCreacion { get; set; }

        public Pais PaPaisNavigation { get; set; }
        public ICollection<CiudadMunicipio> CiudadMunicipio { get; set; }
    }
}
