﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class MovimientoInventario
    {
        public long MoinId { get; set; }
        public long InveId { get; set; }
        public int ProdId { get; set; }
        public decimal MoinCantidad { get; set; }
        public string MoinRegistradopor { get; set; }
        public DateTime MoinFechaCreacion { get; set; }
        public long? ContId { get; set; }
        public string MoinDescripcion { get; set; }
        public bool? MoinActivo { get; set; }
        public decimal? MoinPrecioCompra { get; set; }

        public Contrato Cont { get; set; }
        public Inventario Inve { get; set; }
        public Producto Prod { get; set; }
    }
}
