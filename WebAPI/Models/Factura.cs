﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Factura
    {
        public Factura()
        {
            DetalleFactura = new HashSet<DetalleFactura>();
        }

        public long FactId { get; set; }
        public int SeriId { get; set; }
        public string FactNcontrol { get; set; }
        public string FactNfactura { get; set; }
        public short FopaId { get; set; }
        public float? FactIva { get; set; }
        public decimal FactBase { get; set; }
        public decimal FactMontoIva { get; set; }
        public decimal FactTotal { get; set; }
        public string FactRegistradopor { get; set; }
        public DateTime FactFechaCreacion { get; set; }
        public int? SerId { get; set; }
        public string TipoDeMovimiento { get; set; }
        public int MesId { get; set; }
        public string PersId { get; set; }
        public int? EsfaId { get; set; }
        public decimal? FactDescuento { get; set; }
        public string FactCodigoPago { get; set; }
        public string FactConcepto { get; set; }
        public long? ContId { get; set; }

        public Estadofactura Esfa { get; set; }
        public FormaPago Fopa { get; set; }
        public Mes Mes { get; set; }
        public Persona Pers { get; set; }
        public Serie Seri { get; set; }
        public ICollection<DetalleFactura> DetalleFactura { get; set; }
    }
}
