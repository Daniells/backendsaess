﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class CajaMenor
    {
        public int CameId { get; set; }
        public string PersId { get; set; }
        public string CameDescripcion { get; set; }
        public decimal? CameIngreso { get; set; }
        public decimal? CameEgreso { get; set; }
        public bool CameActivo { get; set; }
        public DateTime CameFechaRegistro { get; set; }
        public string CameRegistradopor { get; set; }
    }
}
