﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Pais
    {
        public Pais()
        {
            Departamento = new HashSet<Departamento>();
        }

        public long PaPais { get; set; }
        public string PaDescripcion { get; set; }
        public string PaAbreviatura { get; set; }
        public bool PaActivo { get; set; }
        public string PaRegistradopor { get; set; }
        public DateTime PaFechaCreacion { get; set; }

        public ICollection<Departamento> Departamento { get; set; }
    }
}
