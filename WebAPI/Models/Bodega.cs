﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Bodega
    {
        public Bodega()
        {
            Inventario = new HashSet<Inventario>();
        }

        public int BodeId { get; set; }
        public string BodeNombre { get; set; }
        public string BodeDescripcion { get; set; }
        public bool BodeActivo { get; set; }
        public string BodeRegistradopor { get; set; }
        public DateTime BodeFechaRegistro { get; set; }
        public int? ZonaId { get; set; }
        public short? EmprId { get; set; }
        public string BodeResponsable { get; set; }
        public string BodeDireccion { get; set; }
        public string BodeTelefono { get; set; }

        public Empresa Empr { get; set; }
        public Zona Zona { get; set; }
        public ICollection<Inventario> Inventario { get; set; }
    }
}
