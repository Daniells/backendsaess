﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class FuncionalidadRol
    {
        public int FuroId { get; set; }
        public int RolId { get; set; }
        public int FuncId { get; set; }
        public bool FuroEstado { get; set; }
        public string FuroRegistradopor { get; set; }
        public DateTime FuroFechaCreacion { get; set; }

        public Funcionalidad Func { get; set; }
        public Rol Rol { get; set; }
    }
}
