﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class PersonaRol
    {
        public int PeroId { get; set; }
        public string PersId { get; set; }
        public int RolId { get; set; }
        public bool PeroEstado { get; set; }
        public string PeroRegistradopor { get; set; }
        public DateTime PeroFechaCreacion { get; set; }

        public Persona Pers { get; set; }
        public Rol Rol { get; set; }
    }
}
