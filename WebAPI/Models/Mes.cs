﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Mes
    {
        public Mes()
        {
            Factura = new HashSet<Factura>();
        }

        public int MesId { get; set; }
        public string MesNombre { get; set; }
        public string MesDescripcion { get; set; }
        public string MesRegistradopor { get; set; }
        public DateTime? MesFechaCreacion { get; set; }
        public bool? MesActivo { get; set; }

        public ICollection<Factura> Factura { get; set; }
    }
}
