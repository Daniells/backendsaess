﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Tipopersona
    {
        public Tipopersona()
        {
            Persona = new HashSet<Persona>();
        }

        public int TipeId { get; set; }
        public string TipeNombre { get; set; }
        public string TipeDescrpcion { get; set; }
        public bool? TipeActivo { get; set; }
        public string TipeRegistradopor { get; set; }
        public string TipeFechaRegistro { get; set; }

        public ICollection<Persona> Persona { get; set; }
    }
}
