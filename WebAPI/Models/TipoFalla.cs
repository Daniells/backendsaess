﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class TipoFalla
    {
        public TipoFalla()
        {
            Reclamo = new HashSet<Reclamo>();
        }

        public short TifaId { get; set; }
        public string TifaNombre { get; set; }
        public string TifaDescripcion { get; set; }
        public string TifaRegistradopor { get; set; }
        public DateTime TifaFechaCreacion { get; set; }
        public bool? TifaActivo { get; set; }

        public ICollection<Reclamo> Reclamo { get; set; }
    }
}
