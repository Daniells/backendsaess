﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Rol
    {
        public Rol()
        {
            FuncionalidadRol = new HashSet<FuncionalidadRol>();
            PersonaRol = new HashSet<PersonaRol>();
        }

        public int RolId { get; set; }
        public string RolNombre { get; set; }
        public string RolDescripcion { get; set; }
        public bool RolEstado { get; set; }
        public string RolRegistradopor { get; set; }
        public DateTime RolFechaCreacion { get; set; }

        public ICollection<FuncionalidadRol> FuncionalidadRol { get; set; }
        public ICollection<PersonaRol> PersonaRol { get; set; }
    }
}
