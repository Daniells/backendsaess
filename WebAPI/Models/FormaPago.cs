﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class FormaPago
    {
        public FormaPago()
        {
            Factura = new HashSet<Factura>();
        }

        public short FopaId { get; set; }
        public string FopaNombre { get; set; }
        public string FopaDescripcion { get; set; }
        public string FopaRegistradopor { get; set; }
        public DateTime FopaFechaCreacion { get; set; }
        public bool? FopaActivo { get; set; }

        public ICollection<Factura> Factura { get; set; }
    }
}
