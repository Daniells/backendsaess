﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Configuracion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string RegistradoPor { get; set; }
    }
}
