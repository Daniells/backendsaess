﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Productosolicitud
    {
        public int SoprId { get; set; }
        public int? ProdId { get; set; }
        public long? SoseId { get; set; }
        public bool? SoprActivo { get; set; }
        public string SoprRegistradopor { get; set; }
        public DateTime SoprFechaRegistro { get; set; }

        public Producto Prod { get; set; }
        public SolicitudServicio Sose { get; set; }
    }
}
