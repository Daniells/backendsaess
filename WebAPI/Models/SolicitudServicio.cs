﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class SolicitudServicio
    {
        public SolicitudServicio()
        {
            Productosolicitud = new HashSet<Productosolicitud>();
            Solicitudconservicio = new HashSet<Solicitudconservicio>();
        }

        public long SoseId { get; set; }
        public short EssoId { get; set; }
        public long? FactId { get; set; }
        public int SoseEmpleado { get; set; }
        public string SoseNumero { get; set; }
        public string SoseDescripcion { get; set; }
        public DateTime? SoseFechaEjecucion { get; set; }
        public string SoseRegistradopor { get; set; }
        public DateTime SoseFechaCreacion { get; set; }
        public bool? SoseActivo { get; set; }
        public decimal? SosePrecio { get; set; }
        public int? SoseTipo { get; set; }
        public string SoseResponsable { get; set; }
        public int? TisoId { get; set; }
        public long? ContId { get; set; }

        public Contrato Cont { get; set; }
        public EstadoSolicitud Esso { get; set; }
        public TipoSolicitud Tiso { get; set; }
        public ICollection<Productosolicitud> Productosolicitud { get; set; }
        public ICollection<Solicitudconservicio> Solicitudconservicio { get; set; }
    }
}
