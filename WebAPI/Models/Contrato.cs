﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Contrato
    {
        public Contrato()
        {
            CableModen = new HashSet<CableModen>();
            MovimientoInventario = new HashSet<MovimientoInventario>();
            Reclamo = new HashSet<Reclamo>();
            SolicitudServicio = new HashSet<SolicitudServicio>();
        }

        public long ContId { get; set; }
        public short ClclId { get; set; }
        public int SectId { get; set; }
        public short EscoId { get; set; }
        public string ContNumero { get; set; }
        public decimal ContCuota { get; set; }
        public string ContDireccion { get; set; }
        public string ContTelefono { get; set; }
        public DateTime? ContFechaInstalacion { get; set; }
        public string ContRegistradopor { get; set; }
        public DateTime ContFechaCreacion { get; set; }
        public string PersId { get; set; }
        public string ContGeolocalizacion { get; set; }
        public bool? ContActivo { get; set; }
        public string ContResponsable { get; set; }

        public ClasificacionCliente Clcl { get; set; }
        public EstadoContrato Esco { get; set; }
        public Persona Pers { get; set; }
        public ICollection<CableModen> CableModen { get; set; }
        public ICollection<MovimientoInventario> MovimientoInventario { get; set; }
        public ICollection<Reclamo> Reclamo { get; set; }
        public ICollection<SolicitudServicio> SolicitudServicio { get; set; }
    }
}
