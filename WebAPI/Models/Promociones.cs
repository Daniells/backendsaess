﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Promociones
    {
        public int PromId { get; set; }
        public int PromNumDocumento { get; set; }
        public string PromNombre { get; set; }
        public string PromApellido { get; set; }
        public int PromTelefono { get; set; }
        public decimal PromDescuento { get; set; }
        public string PromCodigo { get; set; }
        public DateTime PromFechaCreacion { get; set; }
        public string PromRegistradopor { get; set; }
        public DateTime PromFechaModificacion { get; set; }
        public bool PromActivo { get; set; }
    }
}
