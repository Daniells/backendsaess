﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class EstadoContrato
    {
        public EstadoContrato()
        {
            Contrato = new HashSet<Contrato>();
        }

        public short EscoId { get; set; }
        public string EscoNombre { get; set; }
        public string EscoDescripcion { get; set; }
        public string EscoRegistradopor { get; set; }
        public DateTime EscoFechaCreacion { get; set; }
        public bool? EscoActivo { get; set; }

        public ICollection<Contrato> Contrato { get; set; }
    }
}
