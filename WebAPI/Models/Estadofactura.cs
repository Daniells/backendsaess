﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Estadofactura
    {
        public Estadofactura()
        {
            Factura = new HashSet<Factura>();
        }

        public int EsfaId { get; set; }
        public string EsfaNombre { get; set; }
        public string EsfaDescrpcion { get; set; }
        public bool? EsfaActivo { get; set; }
        public string EsfaRegistradopor { get; set; }
        public DateTime? EsfaFachaRegistro { get; set; }

        public ICollection<Factura> Factura { get; set; }
    }
}
