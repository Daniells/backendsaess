﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Iva
    {
        public Iva()
        {
            Producto = new HashSet<Producto>();
        }

        public int IvaId { get; set; }
        public string IvaNombre { get; set; }
        public string IvaDescripcion { get; set; }
        public string IvaRegistradopor { get; set; }
        public DateTime IvaFechaCreacion { get; set; }
        public decimal IvaValor { get; set; }
        public bool? IvaActivo { get; set; }

        public ICollection<Producto> Producto { get; set; }
    }
}
