﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class NotificacionPersona
    {
        public int NotipId { get; set; }
        public int NotiId { get; set; }
        public string PersId { get; set; }
        public bool NotipActivo { get; set; }
        public string NotipRegistradopor { get; set; }
        public DateTime NotipFechaCreacion { get; set; }

        public Notificacion Noti { get; set; }
        public Persona Pers { get; set; }
    }
}
