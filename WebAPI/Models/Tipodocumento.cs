﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Tipodocumento
    {
        public Tipodocumento()
        {
            Persona = new HashSet<Persona>();
        }

        public int TidoId { get; set; }
        public bool? TidoActivo { get; set; }
        public string TidoRegistradopor { get; set; }
        public string TidoFachaRegistro { get; set; }
        public string TidoNombre { get; set; }
        public string TidoDescripcion { get; set; }

        public ICollection<Persona> Persona { get; set; }
    }
}
