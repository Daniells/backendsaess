﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class ClasificacionCliente
    {
        public ClasificacionCliente()
        {
            Contrato = new HashSet<Contrato>();
            Persona = new HashSet<Persona>();
        }

        public short ClclId { get; set; }
        public string ClclNombre { get; set; }
        public string ClclDescripcion { get; set; }
        public bool ClclExento { get; set; }
        public string ClclRegistradopor { get; set; }
        public DateTime ClclFechaCreacion { get; set; }
        public bool? ClclActivo { get; set; }

        public ICollection<Contrato> Contrato { get; set; }
        public ICollection<Persona> Persona { get; set; }
    }
}
