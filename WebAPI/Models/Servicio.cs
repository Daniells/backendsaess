﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Servicio
    {
        public Servicio()
        {
            Paquete = new HashSet<Paquete>();
            Solicitudconservicio = new HashSet<Solicitudconservicio>();
        }

        public int SerId { get; set; }
        public string SerNombre { get; set; }
        public string SerDescripcion { get; set; }
        public long SerCodigo { get; set; }
        public DateTime SerFechaCreacion { get; set; }
        public string SerRegistradopor { get; set; }
        public bool? SerActivo { get; set; }
        public decimal? SerValor { get; set; }
        public short? EmprId { get; set; }

        public Empresa Empr { get; set; }
        public ICollection<Paquete> Paquete { get; set; }
        public ICollection<Solicitudconservicio> Solicitudconservicio { get; set; }
    }
}
