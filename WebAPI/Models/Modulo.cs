﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Modulo
    {
        public Modulo()
        {
            Funcionalidad = new HashSet<Funcionalidad>();
        }

        public int ModuId { get; set; }
        public short? ModuPosicion { get; set; }
        public string ModuNombre { get; set; }
        public string ModuDescripcion { get; set; }
        public bool ModuEstado { get; set; }
        public string ModuRegistradopor { get; set; }
        public DateTime ModuFechaCreacion { get; set; }
        public int? Idmenu { get; set; }

        public ICollection<Funcionalidad> Funcionalidad { get; set; }
    }
}
