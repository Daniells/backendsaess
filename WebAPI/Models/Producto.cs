﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Producto
    {
        public Producto()
        {
            DetalleFactura = new HashSet<DetalleFactura>();
            MovimientoInventario = new HashSet<MovimientoInventario>();
            Productosolicitud = new HashSet<Productosolicitud>();
        }

        public int ProdId { get; set; }
        public short CaprId { get; set; }
        public short UnmeId { get; set; }
        public int? IvaId { get; set; }
        public string ProdCodigo { get; set; }
        public string ProdNombre { get; set; }
        public decimal ProdPrecioVenta { get; set; }
        public string ProdRegistradopor { get; set; }
        public DateTime ProdFechaCreacion { get; set; }
        public decimal ProdCminimo { get; set; }
        public bool? ProdActivo { get; set; }
        public int? Idgrupo { get; set; }
        public int? Idmarca { get; set; }

        public CategoriaProducto Capr { get; set; }
        public Grupoproducto IdgrupoNavigation { get; set; }
        public Marca IdmarcaNavigation { get; set; }
        public Iva Iva { get; set; }
        public UnidadMedida Unme { get; set; }
        public ICollection<DetalleFactura> DetalleFactura { get; set; }
        public ICollection<MovimientoInventario> MovimientoInventario { get; set; }
        public ICollection<Productosolicitud> Productosolicitud { get; set; }
    }
}
