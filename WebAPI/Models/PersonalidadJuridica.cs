﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class PersonalidadJuridica
    {
        public short PejuId { get; set; }
        public string PejuLetra { get; set; }
        public string PejuNombre { get; set; }
        public string PejuDescripcion { get; set; }
        public string PejuRegistradopor { get; set; }
        public DateTime PejuFechaCreacion { get; set; }
        public bool? PejuActivo { get; set; }
    }
}
