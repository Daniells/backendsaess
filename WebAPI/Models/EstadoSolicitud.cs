﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class EstadoSolicitud
    {
        public EstadoSolicitud()
        {
            Reclamo = new HashSet<Reclamo>();
            SolicitudServicio = new HashSet<SolicitudServicio>();
        }

        public short EssoId { get; set; }
        public string EssoNombre { get; set; }
        public string EssoDescripcion { get; set; }
        public string EssoRegistradopor { get; set; }
        public DateTime EssoFechaCreacion { get; set; }
        public bool? EssoActivo { get; set; }

        public ICollection<Reclamo> Reclamo { get; set; }
        public ICollection<SolicitudServicio> SolicitudServicio { get; set; }
    }
}
