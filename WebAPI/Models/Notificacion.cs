﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Notificacion
    {
        public Notificacion()
        {
            NotificacionPersona = new HashSet<NotificacionPersona>();
        }

        public int NotiId { get; set; }
        public string NotiHtml { get; set; }
        public string NotiRegistradopor { get; set; }
        public DateTime? NotiFechaCreacion { get; set; }
        public string NotiNombre { get; set; }
        public string NotiDescripcion { get; set; }
        public bool NotiActivo { get; set; }

        public ICollection<NotificacionPersona> NotificacionPersona { get; set; }
    }
}
