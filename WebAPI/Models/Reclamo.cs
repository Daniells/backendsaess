﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Reclamo
    {
        public int ReclId { get; set; }
        public long ContId { get; set; }
        public short TifaId { get; set; }
        public short EssoId { get; set; }
        public int ReclEmpleado { get; set; }
        public DateTime? ReclFechaEjecucion { get; set; }
        public string ReclDescripcion { get; set; }
        public string ReclRegistradopor { get; set; }
        public DateTime ReclFechaCreacion { get; set; }
        public bool? ReclActivo { get; set; }

        public Contrato Cont { get; set; }
        public EstadoSolicitud Esso { get; set; }
        public TipoFalla Tifa { get; set; }
    }
}
