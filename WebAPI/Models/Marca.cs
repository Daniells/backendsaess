﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Marca
    {
        public Marca()
        {
            Producto = new HashSet<Producto>();
        }

        public int Idmarca { get; set; }
        public string Nombre { get; set; }
        public DateTime? Fecharegistro { get; set; }
        public bool Activo { get; set; }

        public ICollection<Producto> Producto { get; set; }
    }
}
