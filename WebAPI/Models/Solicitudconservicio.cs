﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Solicitudconservicio
    {
        public int SocseId { get; set; }
        public bool? SocseActivo { get; set; }
        public string SocseRegistradopor { get; set; }
        public DateTime SocseFechaRegistro { get; set; }
        public long? SoseId { get; set; }
        public int? SerId { get; set; }

        public Servicio Ser { get; set; }
        public SolicitudServicio Sose { get; set; }
    }
}
