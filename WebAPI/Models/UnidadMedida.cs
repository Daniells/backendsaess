﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class UnidadMedida
    {
        public UnidadMedida()
        {
            Producto = new HashSet<Producto>();
        }

        public short UnmeId { get; set; }
        public string UnmeAbreviatura { get; set; }
        public string UnmeNombre { get; set; }
        public string UnmeDescripcion { get; set; }
        public string UnmeRegistradopor { get; set; }
        public DateTime UnmeFechaCreacion { get; set; }
        public bool? UnmeActivo { get; set; }

        public ICollection<Producto> Producto { get; set; }
    }
}
