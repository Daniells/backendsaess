﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Empresa
    {
        public Empresa()
        {
            Bodega = new HashSet<Bodega>();
            Empresaconpersona = new HashSet<Empresaconpersona>();
            Servicio = new HashSet<Servicio>();
        }

        public short EmprId { get; set; }
        public string EmprNit { get; set; }
        public string EmprNombre { get; set; }
        public string EmprDireccion { get; set; }
        public string EmprEmail { get; set; }
        public string EmprTelefono { get; set; }
        public string EmprTelefax { get; set; }
        public DateTime EmprFechaInicio { get; set; }
        public string EmprRegistradopor { get; set; }
        public DateTime EmprFechaCreacion { get; set; }
        public string EmprCiudad { get; set; }
        public bool EmprEstado { get; set; }
        public long? CimuId { get; set; }

        public CiudadMunicipio Cimu { get; set; }
        public ICollection<Bodega> Bodega { get; set; }
        public ICollection<Empresaconpersona> Empresaconpersona { get; set; }
        public ICollection<Servicio> Servicio { get; set; }
    }
}
