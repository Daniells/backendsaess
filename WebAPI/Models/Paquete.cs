﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Paquete
    {
        public Paquete()
        {
            DetalleFactura = new HashSet<DetalleFactura>();
        }

        public int PaqId { get; set; }
        public string PaqNombre { get; set; }
        public string PaqDescripcion { get; set; }
        public DateTime PaqFechaCreacion { get; set; }
        public string PaqRegistradopor { get; set; }
        public int SerId { get; set; }
        public float PaqPrecio { get; set; }
        public bool? PaqActivo { get; set; }

        public Servicio Ser { get; set; }
        public ICollection<DetalleFactura> DetalleFactura { get; set; }
    }
}
