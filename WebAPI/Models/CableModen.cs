﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class CableModen
    {
        public int CabModenId { get; set; }
        public string CabMarca { get; set; }
        public string CabIdMac { get; set; }
        public string CabSerial { get; set; }
        public DateTime CabFecha { get; set; }
        public string CabRegistradopor { get; set; }
        public bool CabActivo { get; set; }
        public long ContId { get; set; }

        public Contrato Cont { get; set; }
    }
}
