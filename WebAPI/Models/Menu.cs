﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Menu
    {
        public int Idmenu { get; set; }
        public string Nombre { get; set; }
        public DateTime? Fecharegistro { get; set; }
        public bool Activo { get; set; }
    }
}
