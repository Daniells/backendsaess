﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Empresaconpersona
    {
        public int EmcpeId { get; set; }
        public bool? EmcpeActivo { get; set; }
        public short EmprId { get; set; }
        public string PersId { get; set; }
        public string EmprRegistradopor { get; set; }
        public DateTime EmprFechaRegistro { get; set; }
        public bool? EmprActivo { get; set; }

        public Empresa Empr { get; set; }
        public Persona Pers { get; set; }
    }
}
