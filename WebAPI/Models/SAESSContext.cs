﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebAPI.Models
{
    public partial class SAESSContext : DbContext
    {
        public SAESSContext()
        {
        }

        public SAESSContext(DbContextOptions<SAESSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bodega> Bodega { get; set; }
        public virtual DbSet<CableModen> CableModen { get; set; }
        public virtual DbSet<CajaMenor> CajaMenor { get; set; }
        public virtual DbSet<CategoriaProducto> CategoriaProducto { get; set; }
        public virtual DbSet<CiudadMunicipio> CiudadMunicipio { get; set; }
        public virtual DbSet<ClasificacionCliente> ClasificacionCliente { get; set; }
        public virtual DbSet<Configuracion> Configuracion { get; set; }
        public virtual DbSet<Contrato> Contrato { get; set; }
        public virtual DbSet<Departamento> Departamento { get; set; }
        public virtual DbSet<DetalleFactura> DetalleFactura { get; set; }
        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Empresaconpersona> Empresaconpersona { get; set; }
        public virtual DbSet<EstadoCivil> EstadoCivil { get; set; }
        public virtual DbSet<EstadoContrato> EstadoContrato { get; set; }
        public virtual DbSet<Estadofactura> Estadofactura { get; set; }
        public virtual DbSet<EstadoSolicitud> EstadoSolicitud { get; set; }
        public virtual DbSet<Factura> Factura { get; set; }
        public virtual DbSet<FormaPago> FormaPago { get; set; }
        public virtual DbSet<Funcionalidad> Funcionalidad { get; set; }
        public virtual DbSet<FuncionalidadRol> FuncionalidadRol { get; set; }
        public virtual DbSet<Grupoproducto> Grupoproducto { get; set; }
        public virtual DbSet<Inventario> Inventario { get; set; }
        public virtual DbSet<Iva> Iva { get; set; }
        public virtual DbSet<Marca> Marca { get; set; }
        public virtual DbSet<Menu> Menu { get; set; }
        public virtual DbSet<Mes> Mes { get; set; }
        public virtual DbSet<Modulo> Modulo { get; set; }
        public virtual DbSet<MovimientoInventario> MovimientoInventario { get; set; }
        public virtual DbSet<Notificacion> Notificacion { get; set; }
        public virtual DbSet<NotificacionPersona> NotificacionPersona { get; set; }
        public virtual DbSet<Pais> Pais { get; set; }
        public virtual DbSet<Paquete> Paquete { get; set; }
        public virtual DbSet<Persona> Persona { get; set; }
        public virtual DbSet<Personaconzona> Personaconzona { get; set; }
        public virtual DbSet<PersonalidadJuridica> PersonalidadJuridica { get; set; }
        public virtual DbSet<PersonaRol> PersonaRol { get; set; }
        public virtual DbSet<Producto> Producto { get; set; }
        public virtual DbSet<Productosolicitud> Productosolicitud { get; set; }
        public virtual DbSet<Promociones> Promociones { get; set; }
        public virtual DbSet<Reclamo> Reclamo { get; set; }
        public virtual DbSet<Rol> Rol { get; set; }
        public virtual DbSet<Sector> Sector { get; set; }
        public virtual DbSet<Serie> Serie { get; set; }
        public virtual DbSet<Servicio> Servicio { get; set; }
        public virtual DbSet<Sexo> Sexo { get; set; }
        public virtual DbSet<Solicitudconservicio> Solicitudconservicio { get; set; }
        public virtual DbSet<SolicitudServicio> SolicitudServicio { get; set; }
        public virtual DbSet<Tipodocumento> Tipodocumento { get; set; }
        public virtual DbSet<TipoFalla> TipoFalla { get; set; }
        public virtual DbSet<Tipopersona> Tipopersona { get; set; }
        public virtual DbSet<TipoSolicitud> TipoSolicitud { get; set; }
        public virtual DbSet<UnidadMedida> UnidadMedida { get; set; }
        public virtual DbSet<Zona> Zona { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=LAPTOP-J8SM3CPQ;Initial Catalog=SAESS;Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bodega>(entity =>
            {
                entity.HasKey(e => e.BodeId);

                entity.ToTable("bodega");

                entity.Property(e => e.BodeId).HasColumnName("bode_id");

                entity.Property(e => e.BodeActivo).HasColumnName("bode_activo");

                entity.Property(e => e.BodeDescripcion)
                    .HasColumnName("bode_descripcion")
                    .HasMaxLength(50);

                entity.Property(e => e.BodeDireccion)
                    .HasColumnName("bode_direccion")
                    .HasColumnType("text");

                entity.Property(e => e.BodeFechaRegistro)
                    .HasColumnName("bode_fecha_registro")
                    .HasColumnType("datetime");

                entity.Property(e => e.BodeNombre)
                    .IsRequired()
                    .HasColumnName("bode_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.BodeRegistradopor)
                    .HasColumnName("bode_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.BodeResponsable)
                    .HasColumnName("bode_responsable")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BodeTelefono)
                    .HasColumnName("bode_telefono")
                    .HasMaxLength(50);

                entity.Property(e => e.EmprId).HasColumnName("empr_id");

                entity.Property(e => e.ZonaId).HasColumnName("zona_id");

                entity.HasOne(d => d.Empr)
                    .WithMany(p => p.Bodega)
                    .HasForeignKey(d => d.EmprId)
                    .HasConstraintName("FK_bodega_empresa");

                entity.HasOne(d => d.Zona)
                    .WithMany(p => p.Bodega)
                    .HasForeignKey(d => d.ZonaId)
                    .HasConstraintName("FK_bodega_zona");
            });

            modelBuilder.Entity<CableModen>(entity =>
            {
                entity.HasKey(e => e.CabModenId);

                entity.ToTable("cable_moden");

                entity.Property(e => e.CabModenId).HasColumnName("cab_moden_id");

                entity.Property(e => e.CabActivo).HasColumnName("cab_activo");

                entity.Property(e => e.CabFecha)
                    .HasColumnName("cab_fecha")
                    .HasColumnType("date");

                entity.Property(e => e.CabIdMac)
                    .IsRequired()
                    .HasColumnName("cab_id_mac")
                    .HasMaxLength(20);

                entity.Property(e => e.CabMarca)
                    .IsRequired()
                    .HasColumnName("cab_marca")
                    .HasMaxLength(50);

                entity.Property(e => e.CabRegistradopor)
                    .IsRequired()
                    .HasColumnName("cab_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.CabSerial)
                    .IsRequired()
                    .HasColumnName("cab_serial")
                    .HasMaxLength(20);

                entity.Property(e => e.ContId).HasColumnName("cont_id");

                entity.HasOne(d => d.Cont)
                    .WithMany(p => p.CableModen)
                    .HasForeignKey(d => d.ContId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_cable_moden_contrato");
            });

            modelBuilder.Entity<CajaMenor>(entity =>
            {
                entity.HasKey(e => e.CameId);

                entity.ToTable("caja_menor");

                entity.Property(e => e.CameId).HasColumnName("came_id");

                entity.Property(e => e.CameActivo).HasColumnName("came_activo");

                entity.Property(e => e.CameDescripcion)
                    .IsRequired()
                    .HasColumnName("came_descripcion");

                entity.Property(e => e.CameEgreso)
                    .HasColumnName("came_egreso")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.CameFechaRegistro)
                    .HasColumnName("came_fecha_registro")
                    .HasColumnType("datetime");

                entity.Property(e => e.CameIngreso)
                    .HasColumnName("came_ingreso")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.CameRegistradopor)
                    .HasColumnName("came_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.PersId)
                    .IsRequired()
                    .HasColumnName("pers_id")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CategoriaProducto>(entity =>
            {
                entity.HasKey(e => e.CaprId);

                entity.ToTable("categoria_producto");

                entity.Property(e => e.CaprId).HasColumnName("capr_id");

                entity.Property(e => e.CaprActivo).HasColumnName("capr_activo");

                entity.Property(e => e.CaprDescripcion)
                    .HasColumnName("capr_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.CaprFechaCreacion)
                    .HasColumnName("capr_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.CaprNombre)
                    .IsRequired()
                    .HasColumnName("capr_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.CaprRegistradopor)
                    .IsRequired()
                    .HasColumnName("capr_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.CaprTipo).HasColumnName("capr_tipo");
            });

            modelBuilder.Entity<CiudadMunicipio>(entity =>
            {
                entity.HasKey(e => e.CimuId);

                entity.ToTable("ciudad_municipio");

                entity.Property(e => e.CimuId)
                    .HasColumnName("cimu_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CimuActivo).HasColumnName("cimu_activo");

                entity.Property(e => e.CimuDescripcion)
                    .IsRequired()
                    .HasColumnName("cimu_descripcion")
                    .HasMaxLength(100);

                entity.Property(e => e.CimuFechaCreacion)
                    .HasColumnName("cimu_fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.CimuRegistradopor)
                    .HasColumnName("cimu_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.DepDepartamento).HasColumnName("dep_departamento");

                entity.HasOne(d => d.DepDepartamentoNavigation)
                    .WithMany(p => p.CiudadMunicipio)
                    .HasForeignKey(d => d.DepDepartamento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ciudad_municipio_departamento");
            });

            modelBuilder.Entity<ClasificacionCliente>(entity =>
            {
                entity.HasKey(e => e.ClclId);

                entity.ToTable("clasificacion_cliente");

                entity.Property(e => e.ClclId).HasColumnName("clcl_id");

                entity.Property(e => e.ClclActivo).HasColumnName("clcl_activo");

                entity.Property(e => e.ClclDescripcion)
                    .HasColumnName("clcl_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.ClclExento).HasColumnName("clcl_exento");

                entity.Property(e => e.ClclFechaCreacion)
                    .HasColumnName("clcl_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.ClclNombre)
                    .IsRequired()
                    .HasColumnName("clcl_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.ClclRegistradopor)
                    .IsRequired()
                    .HasColumnName("clcl_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Configuracion>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Descripcion).IsRequired();

                entity.Property(e => e.FechaRegistro).HasColumnType("date");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RegistradoPor)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Valor)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Contrato>(entity =>
            {
                entity.HasKey(e => e.ContId);

                entity.ToTable("contrato");

                entity.Property(e => e.ContId).HasColumnName("cont_id");

                entity.Property(e => e.ClclId).HasColumnName("clcl_id");

                entity.Property(e => e.ContActivo).HasColumnName("cont_activo");

                entity.Property(e => e.ContCuota)
                    .HasColumnName("cont_cuota")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ContDireccion)
                    .IsRequired()
                    .HasColumnName("cont_direccion")
                    .HasColumnType("text");

                entity.Property(e => e.ContFechaCreacion)
                    .HasColumnName("cont_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.ContFechaInstalacion)
                    .HasColumnName("cont_fecha_instalacion")
                    .HasColumnType("date");

                entity.Property(e => e.ContGeolocalizacion)
                    .HasColumnName("cont_geolocalizacion")
                    .HasMaxLength(50);

                entity.Property(e => e.ContNumero)
                    .IsRequired()
                    .HasColumnName("cont_numero")
                    .HasMaxLength(20);

                entity.Property(e => e.ContRegistradopor)
                    .HasColumnName("cont_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.ContResponsable)
                    .HasColumnName("cont_responsable")
                    .HasMaxLength(50);

                entity.Property(e => e.ContTelefono)
                    .HasColumnName("cont_telefono")
                    .HasMaxLength(15);

                entity.Property(e => e.EscoId).HasColumnName("esco_id");

                entity.Property(e => e.PersId)
                    .IsRequired()
                    .HasColumnName("pers_id")
                    .HasMaxLength(50);

                entity.Property(e => e.SectId).HasColumnName("sect_id");

                entity.HasOne(d => d.Clcl)
                    .WithMany(p => p.Contrato)
                    .HasForeignKey(d => d.ClclId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__contrato__clcl_i__2180FB33");

                entity.HasOne(d => d.Esco)
                    .WithMany(p => p.Contrato)
                    .HasForeignKey(d => d.EscoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__contrato__esco_i__07C12930");

                entity.HasOne(d => d.Pers)
                    .WithMany(p => p.Contrato)
                    .HasForeignKey(d => d.PersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__contrato__pers_i__5070F446");
            });

            modelBuilder.Entity<Departamento>(entity =>
            {
                entity.HasKey(e => e.DepDepartamento);

                entity.ToTable("departamento");

                entity.Property(e => e.DepDepartamento)
                    .HasColumnName("dep_departamento")
                    .ValueGeneratedNever();

                entity.Property(e => e.DepActivo).HasColumnName("dep_activo");

                entity.Property(e => e.DepDescripcion)
                    .IsRequired()
                    .HasColumnName("dep_descripcion")
                    .HasMaxLength(100);

                entity.Property(e => e.DepFechaCreacion)
                    .HasColumnName("dep_fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.DepRegistradopor)
                    .HasColumnName("dep_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.PaPais).HasColumnName("pa_pais");

                entity.HasOne(d => d.PaPaisNavigation)
                    .WithMany(p => p.Departamento)
                    .HasForeignKey(d => d.PaPais)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_departamento_pais");
            });

            modelBuilder.Entity<DetalleFactura>(entity =>
            {
                entity.HasKey(e => e.DefaId);

                entity.ToTable("detalle_factura");

                entity.Property(e => e.DefaId).HasColumnName("defa_id");

                entity.Property(e => e.DefaActivo).HasColumnName("defa_activo");

                entity.Property(e => e.DefaBase)
                    .HasColumnName("defa_base")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.DefaCantidad)
                    .HasColumnName("defa_cantidad")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.DefaDescripcion)
                    .HasColumnName("defa_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.DefaFechaCreacion)
                    .HasColumnName("defa_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.DefaIva).HasColumnName("defa_iva");

                entity.Property(e => e.DefaMontoIva)
                    .HasColumnName("defa_monto_iva")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.DefaRegistradopor)
                    .IsRequired()
                    .HasColumnName("defa_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.DefaTotal)
                    .HasColumnName("defa_total")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.DefaVunitario)
                    .HasColumnName("defa_vunitario")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.FactId).HasColumnName("fact_id");

                entity.Property(e => e.PaqId).HasColumnName("paq_id");

                entity.Property(e => e.ProdId).HasColumnName("prod_id");

                entity.HasOne(d => d.Fact)
                    .WithMany(p => p.DetalleFactura)
                    .HasForeignKey(d => d.FactId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__detalle_f__fact___1BC821DD");

                entity.HasOne(d => d.Paq)
                    .WithMany(p => p.DetalleFactura)
                    .HasForeignKey(d => d.PaqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__detalle_f__paq_i__534D60F1");

                entity.HasOne(d => d.Prod)
                    .WithMany(p => p.DetalleFactura)
                    .HasForeignKey(d => d.ProdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__detalle_f__prod___1AD3FDA4");
            });

            modelBuilder.Entity<Empresa>(entity =>
            {
                entity.HasKey(e => e.EmprId);

                entity.ToTable("empresa");

                entity.Property(e => e.EmprId).HasColumnName("empr_id");

                entity.Property(e => e.CimuId).HasColumnName("cimu_id");

                entity.Property(e => e.EmprCiudad)
                    .HasColumnName("empr_ciudad")
                    .HasMaxLength(50);

                entity.Property(e => e.EmprDireccion)
                    .IsRequired()
                    .HasColumnName("empr_direccion")
                    .HasColumnType("text");

                entity.Property(e => e.EmprEmail)
                    .IsRequired()
                    .HasColumnName("empr_email")
                    .HasMaxLength(50);

                entity.Property(e => e.EmprEstado).HasColumnName("empr_estado");

                entity.Property(e => e.EmprFechaCreacion)
                    .HasColumnName("empr_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.EmprFechaInicio)
                    .HasColumnName("empr_fecha_inicio")
                    .HasColumnType("date");

                entity.Property(e => e.EmprNit)
                    .IsRequired()
                    .HasColumnName("empr_nit")
                    .HasMaxLength(50);

                entity.Property(e => e.EmprNombre)
                    .IsRequired()
                    .HasColumnName("empr_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.EmprRegistradopor)
                    .IsRequired()
                    .HasColumnName("empr_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.EmprTelefax)
                    .IsRequired()
                    .HasColumnName("empr_telefax")
                    .HasMaxLength(15);

                entity.Property(e => e.EmprTelefono)
                    .IsRequired()
                    .HasColumnName("empr_telefono")
                    .HasMaxLength(15);

                entity.HasOne(d => d.Cimu)
                    .WithMany(p => p.Empresa)
                    .HasForeignKey(d => d.CimuId)
                    .HasConstraintName("FK_empresa_municipio");
            });

            modelBuilder.Entity<Empresaconpersona>(entity =>
            {
                entity.HasKey(e => e.EmcpeId);

                entity.ToTable("empresaconpersona");

                entity.Property(e => e.EmcpeId).HasColumnName("emcpe_id");

                entity.Property(e => e.EmcpeActivo).HasColumnName("emcpe_activo");

                entity.Property(e => e.EmprActivo).HasColumnName("empr_activo");

                entity.Property(e => e.EmprFechaRegistro)
                    .HasColumnName("empr_fecha_registro")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmprId).HasColumnName("empr_id");

                entity.Property(e => e.EmprRegistradopor)
                    .HasColumnName("empr_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.PersId)
                    .IsRequired()
                    .HasColumnName("pers_id")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Empr)
                    .WithMany(p => p.Empresaconpersona)
                    .HasForeignKey(d => d.EmprId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_empresaconpersona_empresa");

                entity.HasOne(d => d.Pers)
                    .WithMany(p => p.Empresaconpersona)
                    .HasForeignKey(d => d.PersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_empresaconpersona_persona");
            });

            modelBuilder.Entity<EstadoCivil>(entity =>
            {
                entity.HasKey(e => e.EsciId);

                entity.ToTable("estado_civil");

                entity.Property(e => e.EsciId).HasColumnName("esci_id");

                entity.Property(e => e.EsciActivo).HasColumnName("esci_activo");

                entity.Property(e => e.EsciDescripcion)
                    .HasColumnName("esci_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.EsciFechaCreacion)
                    .HasColumnName("esci_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.EsciNombre)
                    .IsRequired()
                    .HasColumnName("esci_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.EsciRegistradopor)
                    .IsRequired()
                    .HasColumnName("esci_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EstadoContrato>(entity =>
            {
                entity.HasKey(e => e.EscoId);

                entity.ToTable("estado_contrato");

                entity.Property(e => e.EscoId).HasColumnName("esco_id");

                entity.Property(e => e.EscoActivo).HasColumnName("esco_activo");

                entity.Property(e => e.EscoDescripcion)
                    .HasColumnName("esco_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.EscoFechaCreacion)
                    .HasColumnName("esco_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.EscoNombre)
                    .IsRequired()
                    .HasColumnName("esco_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.EscoRegistradopor)
                    .IsRequired()
                    .HasColumnName("esco_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Estadofactura>(entity =>
            {
                entity.HasKey(e => e.EsfaId);

                entity.ToTable("estadofactura");

                entity.Property(e => e.EsfaId).HasColumnName("esfa_id");

                entity.Property(e => e.EsfaActivo).HasColumnName("esfa_activo");

                entity.Property(e => e.EsfaDescrpcion)
                    .HasColumnName("esfa_descrpcion")
                    .HasMaxLength(50);

                entity.Property(e => e.EsfaFachaRegistro)
                    .HasColumnName("esfa_facha_registro")
                    .HasColumnType("datetime");

                entity.Property(e => e.EsfaNombre)
                    .IsRequired()
                    .HasColumnName("esfa_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.EsfaRegistradopor)
                    .HasColumnName("esfa_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EstadoSolicitud>(entity =>
            {
                entity.HasKey(e => e.EssoId);

                entity.ToTable("estado_solicitud");

                entity.Property(e => e.EssoId).HasColumnName("esso_id");

                entity.Property(e => e.EssoActivo).HasColumnName("esso_activo");

                entity.Property(e => e.EssoDescripcion)
                    .HasColumnName("esso_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.EssoFechaCreacion)
                    .HasColumnName("esso_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.EssoNombre)
                    .IsRequired()
                    .HasColumnName("esso_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.EssoRegistradopor)
                    .IsRequired()
                    .HasColumnName("esso_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Factura>(entity =>
            {
                entity.HasKey(e => e.FactId);

                entity.ToTable("factura");

                entity.Property(e => e.FactId).HasColumnName("fact_id");

                entity.Property(e => e.ContId).HasColumnName("cont_id");

                entity.Property(e => e.EsfaId).HasColumnName("esfa_id");

                entity.Property(e => e.FactBase)
                    .HasColumnName("fact_base")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.FactCodigoPago)
                    .HasColumnName("fact_codigo_pago")
                    .HasMaxLength(20);

                entity.Property(e => e.FactConcepto).HasColumnName("fact_concepto");

                entity.Property(e => e.FactDescuento)
                    .HasColumnName("fact_descuento")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.FactFechaCreacion)
                    .HasColumnName("fact_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.FactIva).HasColumnName("fact_iva");

                entity.Property(e => e.FactMontoIva)
                    .HasColumnName("fact_monto_iva")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.FactNcontrol)
                    .IsRequired()
                    .HasColumnName("fact_ncontrol")
                    .HasMaxLength(50);

                entity.Property(e => e.FactNfactura)
                    .IsRequired()
                    .HasColumnName("fact_nfactura")
                    .HasMaxLength(50);

                entity.Property(e => e.FactRegistradopor)
                    .IsRequired()
                    .HasColumnName("fact_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.FactTotal)
                    .HasColumnName("fact_total")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.FopaId).HasColumnName("fopa_id");

                entity.Property(e => e.MesId).HasColumnName("mes_id");

                entity.Property(e => e.PersId)
                    .HasColumnName("pers_id")
                    .HasMaxLength(50);

                entity.Property(e => e.SerId).HasColumnName("ser_id");

                entity.Property(e => e.SeriId).HasColumnName("seri_id");

                entity.Property(e => e.TipoDeMovimiento)
                    .HasColumnName("tipo_de_movimiento")
                    .HasMaxLength(20);

                entity.HasOne(d => d.Esfa)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.EsfaId)
                    .HasConstraintName("FK_factura_estadofactura");

                entity.HasOne(d => d.Fopa)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.FopaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__factura__fopa_id__5629CD9C");

                entity.HasOne(d => d.Mes)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.MesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_factura_meses");

                entity.HasOne(d => d.Pers)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.PersId)
                    .HasConstraintName("FK_factura_persona");

                entity.HasOne(d => d.Seri)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.SeriId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__factura__seri_id__571DF1D5");
            });

            modelBuilder.Entity<FormaPago>(entity =>
            {
                entity.HasKey(e => e.FopaId);

                entity.ToTable("forma_pago");

                entity.Property(e => e.FopaId).HasColumnName("fopa_id");

                entity.Property(e => e.FopaActivo).HasColumnName("fopa_activo");

                entity.Property(e => e.FopaDescripcion)
                    .HasColumnName("fopa_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.FopaFechaCreacion)
                    .HasColumnName("fopa_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.FopaNombre)
                    .IsRequired()
                    .HasColumnName("fopa_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.FopaRegistradopor)
                    .IsRequired()
                    .HasColumnName("fopa_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Funcionalidad>(entity =>
            {
                entity.HasKey(e => e.FuncId);

                entity.ToTable("funcionalidad");

                entity.Property(e => e.FuncId).HasColumnName("func_id");

                entity.Property(e => e.FuncDescripcion)
                    .HasColumnName("func_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.FuncEstado).HasColumnName("func_estado");

                entity.Property(e => e.FuncFechaCreacion)
                    .HasColumnName("func_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.FuncNombre)
                    .IsRequired()
                    .HasColumnName("func_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.FuncPosicion).HasColumnName("func_posicion");

                entity.Property(e => e.FuncRegistradopor)
                    .IsRequired()
                    .HasColumnName("func_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.FuncUrl)
                    .IsRequired()
                    .HasColumnName("func_url")
                    .HasMaxLength(80);

                entity.Property(e => e.ModuId).HasColumnName("modu_id");

                entity.HasOne(d => d.Modu)
                    .WithMany(p => p.Funcionalidad)
                    .HasForeignKey(d => d.ModuId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__funcional__modu___5812160E");
            });

            modelBuilder.Entity<FuncionalidadRol>(entity =>
            {
                entity.HasKey(e => e.FuroId);

                entity.ToTable("funcionalidad_rol");

                entity.Property(e => e.FuroId).HasColumnName("furo_id");

                entity.Property(e => e.FuncId).HasColumnName("func_id");

                entity.Property(e => e.FuroEstado).HasColumnName("furo_estado");

                entity.Property(e => e.FuroFechaCreacion)
                    .HasColumnName("furo_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.FuroRegistradopor)
                    .IsRequired()
                    .HasColumnName("furo_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.RolId).HasColumnName("rol_id");

                entity.HasOne(d => d.Func)
                    .WithMany(p => p.FuncionalidadRol)
                    .HasForeignKey(d => d.FuncId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__funcional__func___59063A47");

                entity.HasOne(d => d.Rol)
                    .WithMany(p => p.FuncionalidadRol)
                    .HasForeignKey(d => d.RolId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__funcional__rol_i__59FA5E80");
            });

            modelBuilder.Entity<Grupoproducto>(entity =>
            {
                entity.HasKey(e => e.Idgrupo);

                entity.ToTable("grupoproducto");

                entity.Property(e => e.Idgrupo).HasColumnName("idgrupo");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Fecharegistro)
                    .HasColumnName("fecharegistro")
                    .HasColumnType("date");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Inventario>(entity =>
            {
                entity.HasKey(e => e.InveId);

                entity.ToTable("inventario");

                entity.Property(e => e.InveId).HasColumnName("inve_id");

                entity.Property(e => e.BodeId).HasColumnName("bode_id");

                entity.Property(e => e.InveActivo).HasColumnName("inve_activo");

                entity.Property(e => e.InveDescripcion)
                    .IsRequired()
                    .HasColumnName("inve_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.InveFechaCreacion)
                    .HasColumnName("inve_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.InveRegistradopor)
                    .IsRequired()
                    .HasColumnName("inve_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.InveTipo).HasColumnName("inve_tipo");

                entity.HasOne(d => d.Bode)
                    .WithMany(p => p.Inventario)
                    .HasForeignKey(d => d.BodeId)
                    .HasConstraintName("FK_inventario_bodega");
            });

            modelBuilder.Entity<Iva>(entity =>
            {
                entity.ToTable("iva");

                entity.Property(e => e.IvaId).HasColumnName("iva_id");

                entity.Property(e => e.IvaActivo).HasColumnName("iva_activo");

                entity.Property(e => e.IvaDescripcion)
                    .HasColumnName("iva_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.IvaFechaCreacion)
                    .HasColumnName("iva_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.IvaNombre)
                    .IsRequired()
                    .HasColumnName("iva_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.IvaRegistradopor)
                    .IsRequired()
                    .HasColumnName("iva_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.IvaValor)
                    .HasColumnName("iva_valor")
                    .HasColumnType("decimal(18, 1)");
            });

            modelBuilder.Entity<Marca>(entity =>
            {
                entity.HasKey(e => e.Idmarca);

                entity.ToTable("marca");

                entity.Property(e => e.Idmarca).HasColumnName("idmarca");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Fecharegistro)
                    .HasColumnName("fecharegistro")
                    .HasColumnType("date");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Menu>(entity =>
            {
                entity.HasKey(e => e.Idmenu);

                entity.ToTable("menu");

                entity.Property(e => e.Idmenu).HasColumnName("idmenu");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Fecharegistro)
                    .HasColumnName("fecharegistro")
                    .HasColumnType("date");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Mes>(entity =>
            {
                entity.ToTable("mes");

                entity.Property(e => e.MesId).HasColumnName("mes_id");

                entity.Property(e => e.MesActivo).HasColumnName("mes_activo");

                entity.Property(e => e.MesDescripcion).HasColumnName("mes_descripcion");

                entity.Property(e => e.MesFechaCreacion)
                    .HasColumnName("mes_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.MesNombre)
                    .IsRequired()
                    .HasColumnName("mes_nombre")
                    .HasMaxLength(20);

                entity.Property(e => e.MesRegistradopor)
                    .HasColumnName("mes_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Modulo>(entity =>
            {
                entity.HasKey(e => e.ModuId);

                entity.ToTable("modulo");

                entity.Property(e => e.ModuId).HasColumnName("modu_id");

                entity.Property(e => e.Idmenu).HasColumnName("idmenu");

                entity.Property(e => e.ModuDescripcion)
                    .HasColumnName("modu_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.ModuEstado).HasColumnName("modu_estado");

                entity.Property(e => e.ModuFechaCreacion)
                    .HasColumnName("modu_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.ModuNombre)
                    .IsRequired()
                    .HasColumnName("modu_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.ModuPosicion).HasColumnName("modu_posicion");

                entity.Property(e => e.ModuRegistradopor)
                    .IsRequired()
                    .HasColumnName("modu_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MovimientoInventario>(entity =>
            {
                entity.HasKey(e => e.MoinId);

                entity.ToTable("movimiento_inventario");

                entity.Property(e => e.MoinId).HasColumnName("moin_id");

                entity.Property(e => e.ContId).HasColumnName("cont_id");

                entity.Property(e => e.InveId).HasColumnName("inve_id");

                entity.Property(e => e.MoinActivo).HasColumnName("moin_activo");

                entity.Property(e => e.MoinCantidad)
                    .HasColumnName("moin_cantidad")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.MoinDescripcion)
                    .HasColumnName("moin_descripcion")
                    .HasMaxLength(50);

                entity.Property(e => e.MoinFechaCreacion)
                    .HasColumnName("moin_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.MoinPrecioCompra)
                    .HasColumnName("moin_precio_compra")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.MoinRegistradopor)
                    .IsRequired()
                    .HasColumnName("moin_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.ProdId).HasColumnName("prod_id");

                entity.HasOne(d => d.Cont)
                    .WithMany(p => p.MovimientoInventario)
                    .HasForeignKey(d => d.ContId)
                    .HasConstraintName("FK__movimient__cont___7A672E12");

                entity.HasOne(d => d.Inve)
                    .WithMany(p => p.MovimientoInventario)
                    .HasForeignKey(d => d.InveId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__movimient__inve___5BE2A6F2");

                entity.HasOne(d => d.Prod)
                    .WithMany(p => p.MovimientoInventario)
                    .HasForeignKey(d => d.ProdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__movimient__prod___5CD6CB2B");
            });

            modelBuilder.Entity<Notificacion>(entity =>
            {
                entity.HasKey(e => e.NotiId);

                entity.Property(e => e.NotiId).HasColumnName("noti_id");

                entity.Property(e => e.NotiActivo).HasColumnName("noti_activo");

                entity.Property(e => e.NotiDescripcion)
                    .HasColumnName("noti_descripcion")
                    .HasMaxLength(50);

                entity.Property(e => e.NotiFechaCreacion)
                    .HasColumnName("noti_fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.NotiHtml).HasColumnName("noti_html");

                entity.Property(e => e.NotiNombre)
                    .IsRequired()
                    .HasColumnName("noti_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.NotiRegistradopor)
                    .HasColumnName("noti_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<NotificacionPersona>(entity =>
            {
                entity.HasKey(e => e.NotipId);

                entity.ToTable("Notificacion_persona");

                entity.Property(e => e.NotipId)
                    .HasColumnName("notip_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.NotiId).HasColumnName("noti_id");

                entity.Property(e => e.NotipActivo).HasColumnName("notip_activo");

                entity.Property(e => e.NotipFechaCreacion)
                    .HasColumnName("notip_fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.NotipRegistradopor)
                    .HasColumnName("notip_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.PersId)
                    .IsRequired()
                    .HasColumnName("pers_id")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Noti)
                    .WithMany(p => p.NotificacionPersona)
                    .HasForeignKey(d => d.NotiId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Notificacion_Notificacion_persona");

                entity.HasOne(d => d.Pers)
                    .WithMany(p => p.NotificacionPersona)
                    .HasForeignKey(d => d.PersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__persona_Notificacion_persona");
            });

            modelBuilder.Entity<Pais>(entity =>
            {
                entity.HasKey(e => e.PaPais);

                entity.ToTable("pais");

                entity.Property(e => e.PaPais)
                    .HasColumnName("pa_pais")
                    .ValueGeneratedNever();

                entity.Property(e => e.PaAbreviatura)
                    .IsRequired()
                    .HasColumnName("pa_abreviatura")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.PaActivo).HasColumnName("pa_activo");

                entity.Property(e => e.PaDescripcion)
                    .IsRequired()
                    .HasColumnName("pa_descripcion")
                    .HasMaxLength(100);

                entity.Property(e => e.PaFechaCreacion)
                    .HasColumnName("pa_fecha_creacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaRegistradopor)
                    .HasColumnName("pa_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Paquete>(entity =>
            {
                entity.HasKey(e => e.PaqId);

                entity.Property(e => e.PaqId).HasColumnName("paq_id");

                entity.Property(e => e.PaqActivo).HasColumnName("paq_activo");

                entity.Property(e => e.PaqDescripcion)
                    .IsRequired()
                    .HasColumnName("paq_descripcion")
                    .HasMaxLength(50);

                entity.Property(e => e.PaqFechaCreacion)
                    .HasColumnName("paq_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.PaqNombre)
                    .IsRequired()
                    .HasColumnName("paq_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.PaqPrecio).HasColumnName("paq_precio");

                entity.Property(e => e.PaqRegistradopor)
                    .IsRequired()
                    .HasColumnName("paq_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.SerId).HasColumnName("ser_id");

                entity.HasOne(d => d.Ser)
                    .WithMany(p => p.Paquete)
                    .HasForeignKey(d => d.SerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Paquete__ser_id__3587F3E0");
            });

            modelBuilder.Entity<Persona>(entity =>
            {
                entity.HasKey(e => e.PersId);

                entity.ToTable("persona");

                entity.Property(e => e.PersId)
                    .HasColumnName("pers_id")
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.ClclId).HasColumnName("clcl_id");

                entity.Property(e => e.EsciId).HasColumnName("esci_id");

                entity.Property(e => e.PersActivo).HasColumnName("pers_activo");

                entity.Property(e => e.PersApellido)
                    .IsRequired()
                    .HasColumnName("pers_apellido")
                    .HasMaxLength(50);

                entity.Property(e => e.PersCelular)
                    .HasColumnName("pers_celular")
                    .HasMaxLength(15);

                entity.Property(e => e.PersDireccion)
                    .IsRequired()
                    .HasColumnName("pers_direccion")
                    .HasColumnType("text");

                entity.Property(e => e.PersEmail)
                    .HasColumnName("pers_email")
                    .HasMaxLength(50);

                entity.Property(e => e.PersFechaCreacion)
                    .HasColumnName("pers_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.PersFechanacimiento)
                    .HasColumnName("pers_fechanacimiento")
                    .HasColumnType("date");

                entity.Property(e => e.PersIscontribuyente).HasColumnName("pers_iscontribuyente");

                entity.Property(e => e.PersLogin)
                    .HasColumnName("pers_login")
                    .HasMaxLength(15);

                entity.Property(e => e.PersNombre)
                    .IsRequired()
                    .HasColumnName("pers_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.PersNumDocumento)
                    .HasColumnName("pers_num_documento")
                    .HasMaxLength(50);

                entity.Property(e => e.PersPassword)
                    .HasColumnName("pers_password")
                    .HasMaxLength(15);

                entity.Property(e => e.PersRegistradopor)
                    .IsRequired()
                    .HasColumnName("pers_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.PersTelefono)
                    .HasColumnName("pers_telefono")
                    .HasMaxLength(15);

                entity.Property(e => e.SexId).HasColumnName("sex_id");

                entity.Property(e => e.TidoId).HasColumnName("tido_id");

                entity.Property(e => e.TipeId).HasColumnName("tipe_id");

                entity.HasOne(d => d.Clcl)
                    .WithMany(p => p.Persona)
                    .HasForeignKey(d => d.ClclId)
                    .HasConstraintName("FK_persona_clasificacion");

                entity.HasOne(d => d.Esci)
                    .WithMany(p => p.Persona)
                    .HasForeignKey(d => d.EsciId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__persona__esci_id__5FB337D6");

                entity.HasOne(d => d.Sex)
                    .WithMany(p => p.Persona)
                    .HasForeignKey(d => d.SexId)
                    .HasConstraintName("FK_persona_sexo");

                entity.HasOne(d => d.Tido)
                    .WithMany(p => p.Persona)
                    .HasForeignKey(d => d.TidoId)
                    .HasConstraintName("FK_persona_tipodocumento");

                entity.HasOne(d => d.Tipe)
                    .WithMany(p => p.Persona)
                    .HasForeignKey(d => d.TipeId)
                    .HasConstraintName("FK_persona_tipopersona");
            });

            modelBuilder.Entity<Personaconzona>(entity =>
            {
                entity.HasKey(e => e.PezoId);

                entity.ToTable("personaconzona");

                entity.Property(e => e.PezoId).HasColumnName("pezo_id");

                entity.Property(e => e.PersId)
                    .HasColumnName("pers_id")
                    .HasMaxLength(50);

                entity.Property(e => e.PezoActivo).HasColumnName("pezo_activo");

                entity.Property(e => e.PezoFechaRegistro)
                    .HasColumnName("pezo_fecha_registro")
                    .HasColumnType("datetime");

                entity.Property(e => e.PezoRegistradopor)
                    .HasColumnName("pezo_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.ZonaId).HasColumnName("zona_id");

                entity.HasOne(d => d.Pers)
                    .WithMany(p => p.Personaconzona)
                    .HasForeignKey(d => d.PersId)
                    .HasConstraintName("FK_personaconzona_persona");

                entity.HasOne(d => d.Zona)
                    .WithMany(p => p.Personaconzona)
                    .HasForeignKey(d => d.ZonaId)
                    .HasConstraintName("FK_personaconzona_zona");
            });

            modelBuilder.Entity<PersonalidadJuridica>(entity =>
            {
                entity.HasKey(e => e.PejuId);

                entity.ToTable("personalidad_juridica");

                entity.Property(e => e.PejuId).HasColumnName("peju_id");

                entity.Property(e => e.PejuActivo).HasColumnName("peju_activo");

                entity.Property(e => e.PejuDescripcion)
                    .HasColumnName("peju_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.PejuFechaCreacion)
                    .HasColumnName("peju_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.PejuLetra)
                    .IsRequired()
                    .HasColumnName("peju_letra")
                    .HasMaxLength(2);

                entity.Property(e => e.PejuNombre)
                    .IsRequired()
                    .HasColumnName("peju_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.PejuRegistradopor)
                    .IsRequired()
                    .HasColumnName("peju_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PersonaRol>(entity =>
            {
                entity.HasKey(e => e.PeroId);

                entity.ToTable("persona_rol");

                entity.Property(e => e.PeroId).HasColumnName("pero_id");

                entity.Property(e => e.PeroEstado).HasColumnName("pero_estado");

                entity.Property(e => e.PeroFechaCreacion)
                    .HasColumnName("pero_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.PeroRegistradopor)
                    .IsRequired()
                    .HasColumnName("pero_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.PersId)
                    .IsRequired()
                    .HasColumnName("pers_id")
                    .HasMaxLength(50);

                entity.Property(e => e.RolId).HasColumnName("rol_id");

                entity.HasOne(d => d.Pers)
                    .WithMany(p => p.PersonaRol)
                    .HasForeignKey(d => d.PersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__persona_r__pers___619B8048");

                entity.HasOne(d => d.Rol)
                    .WithMany(p => p.PersonaRol)
                    .HasForeignKey(d => d.RolId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__persona_r__rol_i__628FA481");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.ProdId);

                entity.ToTable("producto");

                entity.Property(e => e.ProdId).HasColumnName("prod_id");

                entity.Property(e => e.CaprId).HasColumnName("capr_id");

                entity.Property(e => e.Idgrupo).HasColumnName("idgrupo");

                entity.Property(e => e.Idmarca).HasColumnName("idmarca");

                entity.Property(e => e.IvaId).HasColumnName("iva_id");

                entity.Property(e => e.ProdActivo).HasColumnName("prod_activo");

                entity.Property(e => e.ProdCminimo)
                    .HasColumnName("prod_cminimo")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ProdCodigo)
                    .IsRequired()
                    .HasColumnName("prod_codigo")
                    .HasMaxLength(20);

                entity.Property(e => e.ProdFechaCreacion)
                    .HasColumnName("prod_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.ProdNombre)
                    .IsRequired()
                    .HasColumnName("prod_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.ProdPrecioVenta)
                    .HasColumnName("prod_precio_venta")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ProdRegistradopor)
                    .IsRequired()
                    .HasColumnName("prod_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.UnmeId).HasColumnName("unme_id");

                entity.HasOne(d => d.Capr)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.CaprId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__producto__capr_i__6383C8BA");

                entity.HasOne(d => d.IdgrupoNavigation)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.Idgrupo)
                    .HasConstraintName("FK_grupoproducto_producto");

                entity.HasOne(d => d.IdmarcaNavigation)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.Idmarca)
                    .HasConstraintName("FK_marca_producto");

                entity.HasOne(d => d.Iva)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.IvaId)
                    .HasConstraintName("FK__producto__iva_id__6477ECF3");

                entity.HasOne(d => d.Unme)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.UnmeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__producto__unme_i__656C112C");
            });

            modelBuilder.Entity<Productosolicitud>(entity =>
            {
                entity.HasKey(e => e.SoprId);

                entity.ToTable("productosolicitud");

                entity.Property(e => e.SoprId).HasColumnName("sopr_id");

                entity.Property(e => e.ProdId).HasColumnName("prod_id");

                entity.Property(e => e.SoprActivo).HasColumnName("sopr_activo");

                entity.Property(e => e.SoprFechaRegistro)
                    .HasColumnName("sopr_fecha_registro")
                    .HasColumnType("datetime");

                entity.Property(e => e.SoprRegistradopor)
                    .HasColumnName("sopr_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.SoseId).HasColumnName("sose_id");

                entity.HasOne(d => d.Prod)
                    .WithMany(p => p.Productosolicitud)
                    .HasForeignKey(d => d.ProdId)
                    .HasConstraintName("FK_productosolicitud_producto");

                entity.HasOne(d => d.Sose)
                    .WithMany(p => p.Productosolicitud)
                    .HasForeignKey(d => d.SoseId)
                    .HasConstraintName("FK_solicitud_servicio_solicitudproducto");
            });

            modelBuilder.Entity<Promociones>(entity =>
            {
                entity.HasKey(e => e.PromId);

                entity.Property(e => e.PromId)
                    .HasColumnName("prom_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.PromActivo).HasColumnName("prom_activo");

                entity.Property(e => e.PromApellido)
                    .IsRequired()
                    .HasColumnName("prom_apellido")
                    .HasMaxLength(50);

                entity.Property(e => e.PromCodigo)
                    .IsRequired()
                    .HasColumnName("prom_codigo")
                    .HasMaxLength(50);

                entity.Property(e => e.PromDescuento)
                    .HasColumnName("prom_descuento")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PromFechaCreacion)
                    .HasColumnName("prom_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.PromFechaModificacion)
                    .HasColumnName("prom_fecha_modificacion")
                    .HasColumnType("date");

                entity.Property(e => e.PromNombre)
                    .IsRequired()
                    .HasColumnName("prom_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.PromNumDocumento).HasColumnName("prom_Num_documento");

                entity.Property(e => e.PromRegistradopor)
                    .IsRequired()
                    .HasColumnName("prom_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.PromTelefono).HasColumnName("prom_telefono");
            });

            modelBuilder.Entity<Reclamo>(entity =>
            {
                entity.HasKey(e => e.ReclId);

                entity.ToTable("reclamo");

                entity.Property(e => e.ReclId).HasColumnName("recl_id");

                entity.Property(e => e.ContId).HasColumnName("cont_id");

                entity.Property(e => e.EssoId).HasColumnName("esso_id");

                entity.Property(e => e.ReclActivo).HasColumnName("recl_activo");

                entity.Property(e => e.ReclDescripcion)
                    .HasColumnName("recl_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.ReclEmpleado).HasColumnName("recl_empleado");

                entity.Property(e => e.ReclFechaCreacion)
                    .HasColumnName("recl_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.ReclFechaEjecucion)
                    .HasColumnName("recl_fecha_ejecucion")
                    .HasColumnType("date");

                entity.Property(e => e.ReclRegistradopor)
                    .IsRequired()
                    .HasColumnName("recl_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.TifaId).HasColumnName("tifa_id");

                entity.HasOne(d => d.Cont)
                    .WithMany(p => p.Reclamo)
                    .HasForeignKey(d => d.ContId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__reclamo__cont_id__09A971A2");

                entity.HasOne(d => d.Esso)
                    .WithMany(p => p.Reclamo)
                    .HasForeignKey(d => d.EssoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__reclamo__esso_id__6754599E");

                entity.HasOne(d => d.Tifa)
                    .WithMany(p => p.Reclamo)
                    .HasForeignKey(d => d.TifaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__reclamo__tifa_id__68487DD7");
            });

            modelBuilder.Entity<Rol>(entity =>
            {
                entity.ToTable("rol");

                entity.Property(e => e.RolId).HasColumnName("rol_id");

                entity.Property(e => e.RolDescripcion)
                    .HasColumnName("rol_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.RolEstado).HasColumnName("rol_estado");

                entity.Property(e => e.RolFechaCreacion)
                    .HasColumnName("rol_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.RolNombre)
                    .IsRequired()
                    .HasColumnName("rol_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.RolRegistradopor)
                    .IsRequired()
                    .HasColumnName("rol_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Sector>(entity =>
            {
                entity.HasKey(e => e.SectId);

                entity.ToTable("sector");

                entity.Property(e => e.SectId).HasColumnName("sect_id");

                entity.Property(e => e.SectAbreviatura)
                    .IsRequired()
                    .HasColumnName("sect_abreviatura")
                    .HasMaxLength(10);

                entity.Property(e => e.SectActivo).HasColumnName("sect_activo");

                entity.Property(e => e.SectDescripcion)
                    .HasColumnName("sect_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.SectFechaCreacion)
                    .HasColumnName("sect_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.SectNombre)
                    .IsRequired()
                    .HasColumnName("sect_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.SectRecargo).HasColumnName("sect_recargo");

                entity.Property(e => e.SectRegistradopor)
                    .IsRequired()
                    .HasColumnName("sect_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.ZonaId).HasColumnName("zona_id");

                entity.HasOne(d => d.Zona)
                    .WithMany(p => p.Sector)
                    .HasForeignKey(d => d.ZonaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__sector__zona_id__693CA210");
            });

            modelBuilder.Entity<Serie>(entity =>
            {
                entity.HasKey(e => e.SeriId);

                entity.ToTable("serie");

                entity.Property(e => e.SeriId).HasColumnName("seri_id");

                entity.Property(e => e.SeriActivo).HasColumnName("seri_activo");

                entity.Property(e => e.SeriCorrelativo).HasColumnName("seri_correlativo");

                entity.Property(e => e.SeriDescripcion)
                    .HasColumnName("seri_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.SeriFechaCreacion)
                    .HasColumnName("seri_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.SeriLetra)
                    .IsRequired()
                    .HasColumnName("seri_letra")
                    .HasMaxLength(3);

                entity.Property(e => e.SeriRegistradopor)
                    .IsRequired()
                    .HasColumnName("seri_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Servicio>(entity =>
            {
                entity.HasKey(e => e.SerId);

                entity.ToTable("servicio");

                entity.Property(e => e.SerId).HasColumnName("ser_id");

                entity.Property(e => e.EmprId).HasColumnName("empr_id");

                entity.Property(e => e.SerActivo).HasColumnName("ser_activo");

                entity.Property(e => e.SerCodigo).HasColumnName("ser_codigo");

                entity.Property(e => e.SerDescripcion)
                    .IsRequired()
                    .HasColumnName("ser_descripcion")
                    .HasMaxLength(50);

                entity.Property(e => e.SerFechaCreacion)
                    .HasColumnName("ser_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.SerNombre)
                    .IsRequired()
                    .HasColumnName("ser_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.SerRegistradopor)
                    .IsRequired()
                    .HasColumnName("ser_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.SerValor)
                    .HasColumnName("ser_valor")
                    .HasColumnType("decimal(18, 1)");

                entity.HasOne(d => d.Empr)
                    .WithMany(p => p.Servicio)
                    .HasForeignKey(d => d.EmprId)
                    .HasConstraintName("FK_servicio_empresa");
            });

            modelBuilder.Entity<Sexo>(entity =>
            {
                entity.HasKey(e => e.SexId);

                entity.ToTable("sexo");

                entity.Property(e => e.SexId).HasColumnName("sex_id");

                entity.Property(e => e.SexActivo).HasColumnName("sex_activo");

                entity.Property(e => e.SexDescripcion)
                    .HasColumnName("sex_descripcion")
                    .HasMaxLength(50);

                entity.Property(e => e.SexFechaRegistro)
                    .HasColumnName("sex_fecha_registro")
                    .HasColumnType("datetime");

                entity.Property(e => e.SexNombre)
                    .IsRequired()
                    .HasColumnName("sex_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.SexRegistradopor)
                    .HasColumnName("sex_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Solicitudconservicio>(entity =>
            {
                entity.HasKey(e => e.SocseId);

                entity.ToTable("solicitudconservicio");

                entity.Property(e => e.SocseId).HasColumnName("socse_id");

                entity.Property(e => e.SerId).HasColumnName("ser_id");

                entity.Property(e => e.SocseActivo).HasColumnName("socse_activo");

                entity.Property(e => e.SocseFechaRegistro)
                    .HasColumnName("socse_fecha_registro")
                    .HasColumnType("datetime");

                entity.Property(e => e.SocseRegistradopor)
                    .HasColumnName("socse_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.SoseId).HasColumnName("sose_id");

                entity.HasOne(d => d.Ser)
                    .WithMany(p => p.Solicitudconservicio)
                    .HasForeignKey(d => d.SerId)
                    .HasConstraintName("FK_solicitudconservicio_servicio");

                entity.HasOne(d => d.Sose)
                    .WithMany(p => p.Solicitudconservicio)
                    .HasForeignKey(d => d.SoseId)
                    .HasConstraintName("FK_solicitudconservicio_solicitud_servicio");
            });

            modelBuilder.Entity<SolicitudServicio>(entity =>
            {
                entity.HasKey(e => e.SoseId);

                entity.ToTable("solicitud_servicio");

                entity.Property(e => e.SoseId).HasColumnName("sose_id");

                entity.Property(e => e.ContId).HasColumnName("cont_id");

                entity.Property(e => e.EssoId).HasColumnName("esso_id");

                entity.Property(e => e.FactId).HasColumnName("fact_id");

                entity.Property(e => e.SoseActivo).HasColumnName("sose_activo");

                entity.Property(e => e.SoseDescripcion)
                    .HasColumnName("sose_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.SoseEmpleado).HasColumnName("sose_empleado");

                entity.Property(e => e.SoseFechaCreacion)
                    .HasColumnName("sose_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.SoseFechaEjecucion)
                    .HasColumnName("sose_fecha_ejecucion")
                    .HasColumnType("date");

                entity.Property(e => e.SoseNumero)
                    .IsRequired()
                    .HasColumnName("sose_numero")
                    .HasMaxLength(20);

                entity.Property(e => e.SosePrecio).HasColumnName("sose_precio");

                entity.Property(e => e.SoseRegistradopor)
                    .IsRequired()
                    .HasColumnName("sose_registradopor")
                    .HasMaxLength(50);

                entity.Property(e => e.SoseResponsable)
                    .HasColumnName("sose_responsable")
                    .HasMaxLength(50);

                entity.Property(e => e.SoseTipo).HasColumnName("sose_tipo");

                entity.Property(e => e.TisoId).HasColumnName("tiso_id");

                entity.HasOne(d => d.Cont)
                    .WithMany(p => p.SolicitudServicio)
                    .HasForeignKey(d => d.ContId)
                    .HasConstraintName("FK_solicitud_servicio_contrato");

                entity.HasOne(d => d.Esso)
                    .WithMany(p => p.SolicitudServicio)
                    .HasForeignKey(d => d.EssoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__solicitud__esso___6A30C649");

                entity.HasOne(d => d.Tiso)
                    .WithMany(p => p.SolicitudServicio)
                    .HasForeignKey(d => d.TisoId)
                    .HasConstraintName("FK_solicitud_servicio_tipo_solicitud");
            });

            modelBuilder.Entity<Tipodocumento>(entity =>
            {
                entity.HasKey(e => e.TidoId);

                entity.ToTable("tipodocumento");

                entity.Property(e => e.TidoId).HasColumnName("tido_id");

                entity.Property(e => e.TidoActivo).HasColumnName("tido_activo");

                entity.Property(e => e.TidoDescripcion)
                    .HasColumnName("tido_descripcion")
                    .HasMaxLength(50);

                entity.Property(e => e.TidoFachaRegistro)
                    .HasColumnName("tido_facha_registro")
                    .HasMaxLength(50);

                entity.Property(e => e.TidoNombre)
                    .IsRequired()
                    .HasColumnName("tido_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.TidoRegistradopor)
                    .HasColumnName("tido_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TipoFalla>(entity =>
            {
                entity.HasKey(e => e.TifaId);

                entity.ToTable("tipo_falla");

                entity.Property(e => e.TifaId).HasColumnName("tifa_id");

                entity.Property(e => e.TifaActivo).HasColumnName("tifa_activo");

                entity.Property(e => e.TifaDescripcion)
                    .HasColumnName("tifa_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.TifaFechaCreacion)
                    .HasColumnName("tifa_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.TifaNombre)
                    .IsRequired()
                    .HasColumnName("tifa_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.TifaRegistradopor)
                    .IsRequired()
                    .HasColumnName("tifa_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Tipopersona>(entity =>
            {
                entity.HasKey(e => e.TipeId);

                entity.ToTable("tipopersona");

                entity.Property(e => e.TipeId).HasColumnName("tipe_id");

                entity.Property(e => e.TipeActivo).HasColumnName("tipe_activo");

                entity.Property(e => e.TipeDescrpcion)
                    .HasColumnName("tipe_descrpcion")
                    .HasMaxLength(50);

                entity.Property(e => e.TipeFechaRegistro)
                    .HasColumnName("tipe_fecha_registro")
                    .HasMaxLength(50);

                entity.Property(e => e.TipeNombre)
                    .IsRequired()
                    .HasColumnName("tipe_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.TipeRegistradopor)
                    .HasColumnName("tipe_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TipoSolicitud>(entity =>
            {
                entity.HasKey(e => e.TisoId);

                entity.Property(e => e.TisoId).HasColumnName("tiso_id");

                entity.Property(e => e.TisoActivo).HasColumnName("tiso_activo");

                entity.Property(e => e.TisoDescripcion)
                    .HasColumnName("tiso_descripcion")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TisoFechaCreacion)
                    .HasColumnName("tiso_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.TisoNombre)
                    .IsRequired()
                    .HasColumnName("tiso_nombre")
                    .HasMaxLength(30);

                entity.Property(e => e.TisoRegistradopor)
                    .IsRequired()
                    .HasColumnName("tiso_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<UnidadMedida>(entity =>
            {
                entity.HasKey(e => e.UnmeId);

                entity.ToTable("unidad_medida");

                entity.Property(e => e.UnmeId).HasColumnName("unme_id");

                entity.Property(e => e.UnmeAbreviatura)
                    .IsRequired()
                    .HasColumnName("unme_abreviatura")
                    .HasMaxLength(6);

                entity.Property(e => e.UnmeActivo).HasColumnName("unme_activo");

                entity.Property(e => e.UnmeDescripcion)
                    .HasColumnName("unme_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.UnmeFechaCreacion)
                    .HasColumnName("unme_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.UnmeNombre)
                    .IsRequired()
                    .HasColumnName("unme_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.UnmeRegistradopor)
                    .IsRequired()
                    .HasColumnName("unme_registradopor")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Zona>(entity =>
            {
                entity.ToTable("zona");

                entity.Property(e => e.ZonaId).HasColumnName("zona_id");

                entity.Property(e => e.CimuId).HasColumnName("cimu_id");

                entity.Property(e => e.ZonaActivo).HasColumnName("zona_activo");

                entity.Property(e => e.ZonaDescripcion)
                    .HasColumnName("zona_descripcion")
                    .HasColumnType("text");

                entity.Property(e => e.ZonaFechaCreacion)
                    .HasColumnName("zona_fecha_creacion")
                    .HasColumnType("date");

                entity.Property(e => e.ZonaNombre)
                    .IsRequired()
                    .HasColumnName("zona_nombre")
                    .HasMaxLength(50);

                entity.Property(e => e.ZonaRegistradopor)
                    .IsRequired()
                    .HasColumnName("zona_registradopor")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Cimu)
                    .WithMany(p => p.Zona)
                    .HasForeignKey(d => d.CimuId)
                    .HasConstraintName("FK_zona_municipio");
            });
        }
    }
}
