﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Zona
    {
        public Zona()
        {
            Bodega = new HashSet<Bodega>();
            Personaconzona = new HashSet<Personaconzona>();
            Sector = new HashSet<Sector>();
        }

        public int ZonaId { get; set; }
        public string ZonaNombre { get; set; }
        public string ZonaDescripcion { get; set; }
        public string ZonaRegistradopor { get; set; }
        public DateTime ZonaFechaCreacion { get; set; }
        public long? CimuId { get; set; }
        public bool? ZonaActivo { get; set; }

        public CiudadMunicipio Cimu { get; set; }
        public ICollection<Bodega> Bodega { get; set; }
        public ICollection<Personaconzona> Personaconzona { get; set; }
        public ICollection<Sector> Sector { get; set; }
    }
}
