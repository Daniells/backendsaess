﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Persona
    {
        public Persona()
        {
            Contrato = new HashSet<Contrato>();
            Empresaconpersona = new HashSet<Empresaconpersona>();
            Factura = new HashSet<Factura>();
            NotificacionPersona = new HashSet<NotificacionPersona>();
            PersonaRol = new HashSet<PersonaRol>();
            Personaconzona = new HashSet<Personaconzona>();
        }

        public string PersId { get; set; }
        public short EsciId { get; set; }
        public bool PersIscontribuyente { get; set; }
        public string PersNombre { get; set; }
        public string PersApellido { get; set; }
        public DateTime? PersFechanacimiento { get; set; }
        public string PersDireccion { get; set; }
        public string PersTelefono { get; set; }
        public string PersCelular { get; set; }
        public string PersEmail { get; set; }
        public string PersLogin { get; set; }
        public string PersPassword { get; set; }
        public string PersRegistradopor { get; set; }
        public DateTime PersFechaCreacion { get; set; }
        public bool? PersActivo { get; set; }
        public short? ClclId { get; set; }
        public int? TidoId { get; set; }
        public int? TipeId { get; set; }
        public int? SexId { get; set; }
        public string PersNumDocumento { get; set; }

        public ClasificacionCliente Clcl { get; set; }
        public EstadoCivil Esci { get; set; }
        public Sexo Sex { get; set; }
        public Tipodocumento Tido { get; set; }
        public Tipopersona Tipe { get; set; }
        public ICollection<Contrato> Contrato { get; set; }
        public ICollection<Empresaconpersona> Empresaconpersona { get; set; }
        public ICollection<Factura> Factura { get; set; }
        public ICollection<NotificacionPersona> NotificacionPersona { get; set; }
        public ICollection<PersonaRol> PersonaRol { get; set; }
        public ICollection<Personaconzona> Personaconzona { get; set; }
    }
}
