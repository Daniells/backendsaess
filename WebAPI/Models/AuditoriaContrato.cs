﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class AuditoriaContrato
    {
        public long AucoId { get; set; }
        public long ContId { get; set; }
        public int ProdId { get; set; }
        public int AucoEmpleado { get; set; }
        public DateTime AucoFechaEmision { get; set; }
        public decimal AucoMonto { get; set; }
        public string AucoOperacion { get; set; }
        public string AucoComentario { get; set; }
        public string AucoRegistradopor { get; set; }
        public DateTime AucoFechaCreacion { get; set; }

        public Contrato Cont { get; set; }
        public Producto Prod { get; set; }
    }
}
