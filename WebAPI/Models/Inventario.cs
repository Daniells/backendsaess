﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class Inventario
    {
        public Inventario()
        {
            MovimientoInventario = new HashSet<MovimientoInventario>();
        }

        public long InveId { get; set; }
        public int InveTipo { get; set; }
        public string InveDescripcion { get; set; }
        public string InveRegistradopor { get; set; }
        public DateTime InveFechaCreacion { get; set; }
        public int? BodeId { get; set; }
        public bool? InveActivo { get; set; }

        public Bodega Bode { get; set; }
        public ICollection<MovimientoInventario> MovimientoInventario { get; set; }
    }
}
