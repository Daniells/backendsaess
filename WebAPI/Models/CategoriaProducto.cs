﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class CategoriaProducto
    {
        public CategoriaProducto()
        {
            Producto = new HashSet<Producto>();
        }

        public short CaprId { get; set; }
        public short CaprTipo { get; set; }
        public string CaprNombre { get; set; }
        public string CaprDescripcion { get; set; }
        public string CaprRegistradopor { get; set; }
        public DateTime CaprFechaCreacion { get; set; }
        public bool? CaprActivo { get; set; }

        public ICollection<Producto> Producto { get; set; }
    }
}
