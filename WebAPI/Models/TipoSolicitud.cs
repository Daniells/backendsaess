﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class TipoSolicitud
    {
        public TipoSolicitud()
        {
            SolicitudServicio = new HashSet<SolicitudServicio>();
        }

        public int TisoId { get; set; }
        public string TisoNombre { get; set; }
        public string TisoDescripcion { get; set; }
        public DateTime TisoFechaCreacion { get; set; }
        public bool TisoActivo { get; set; }
        public string TisoRegistradopor { get; set; }

        public ICollection<SolicitudServicio> SolicitudServicio { get; set; }
    }
}
