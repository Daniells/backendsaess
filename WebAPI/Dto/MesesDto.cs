﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class MesesDto
    {
        public int MesId { get; set; }

        public string MesNombre { get; set; }

        public string MesDescripcion { get; set; }       
    }
}
