﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ObjectZona
    {
        public int ZonaId { get; set; }

        public string ZonaNombre { get; set; }

        public string ZonaDescripcion { get; set; }

        public string ZonaRegistradopor { get; set; }
       
        public long? CimuId { get; set; }        
    }
}
