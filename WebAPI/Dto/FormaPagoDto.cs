﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class FormaPagoDto
    {
        public short FopaId { get; set; }

        public string FopaNombre { get; set; }

        public string FopaDescripcion { get; set; }     
    }
}
