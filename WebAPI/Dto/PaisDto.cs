﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class PaisDto
    {
        public long PaPais { get; set; }

        public string PaDescripcion { get; set; }

        public string PaAbreviatura { get; set; }
         
        //departamento
        public List<DepartamentoDto> Departamento { get; set; }
    }
} 
 