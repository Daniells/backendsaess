﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class CajaMenorResponseDto
    {
        public int CameId { get; set; }

        public string CameDescripcion { get; set; }

        public decimal? CameIngreso { get; set; }

        public decimal? CameEgreso { get; set; }

        public DateTime CameFechaRegistro { get; set; }

        public ProveedorDto proveedor { get; set; }
    }
}
