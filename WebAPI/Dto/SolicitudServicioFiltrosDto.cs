﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Dto
{
    public class SolicitudServicioFiltrosDto
    {
        public long SoseId { get; set; }

        public string SoseNumero { get; set; }

        public string SoseDescripcion { get; set; }

        public decimal? SosePrecio { get; set; }       

        public DateTime? SoseFechaEjecucion { get; set; }

        public DateTime SoseFechaCreacion { get; set; }

        public TipoSolicitud Tiso { get; set; }

        public EstadoSolicitud Esso { get; set; }

        public Contrato Contrato { get; set; }

        public string persId { get; set; }

        public string UltimoNumeroSolicitudBD { get; set; }
    }
}
