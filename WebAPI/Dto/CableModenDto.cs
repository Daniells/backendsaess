﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class CableModenDto
    {

        public string CabModenId { get; set; }

        public string CabMarca { get; set; }

        public string CabIdMac { get; set; }

        public string CabSerial { get; set; }

        public DateTime CabFecha { get; set; }

        public List<ContratoDto> Contrato { get; set; }
    }
}
