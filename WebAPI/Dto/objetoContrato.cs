﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Dto
{
    public class objetoContrato
    {
        public long ContId { get; set; }

        public short ClclId { get; set; }

        public int SectId { get; set; }

        public short EscoId { get; set; }

        public string ContNumero { get; set; }

        public decimal ContCuota { get; set; }

        public string ContDireccion { get; set; }

        public string ContTelefono { get; set; }

        public DateTime ContFechaInstalacion { get; set; }

        public string ContRegistradopor { get; set; }
         
        public long? SoseId { get; set; }

        public string PersId { get; set; }
         
        public string ContResponsable { get; set; }

        public Servicio Servicio { get; set; }
    }
}
