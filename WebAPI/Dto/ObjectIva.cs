﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ObjectIva
    {
        public int IvaId { get; set; }

        public string IvaNombre { get; set; }

        public string IvaDescripcion { get; set; }

        public string IvaRegistradopor { get; set; }        

        public decimal IvaValor { get; set; }
    }
}
