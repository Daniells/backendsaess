﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class RolDto
    {
        public int RolId { get; set; }

        public string RolNombre { get; set; }

        public string RolDescripcion { get; set; }

        public bool RolEstado { get; set; }

        public string RolRegistradopor { get; set; }

        public DateTime RolFechaCreacion { get; set; }

        public List<FuncionalidadRolDto> FuncionalidadRol { get; set; }

        public List<PersonaRolDto> PersonaRol { get; set; }

        //funcionalidad
        public List<FuncionalidadDto> Funcionalidad { get; set; }
    }
}
