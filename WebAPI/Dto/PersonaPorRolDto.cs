﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class PersonaPorRolDto
    {
        public string PersId { get; set; } 

        public string PersNombre { get; set; }

        public string PersApellido { get; set; } 

        public string PersNumDocumento { get; set; } 
    }
}
