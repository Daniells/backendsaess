﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Dto
{
    public class MenuDto
    {
        public int Idmenu { get; set; }

        public string Menu { get; set; }
       
        public List<Modulo> Modulo { get; set; }
    }
}
