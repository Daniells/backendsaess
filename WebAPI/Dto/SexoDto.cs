﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class SexoDto
    {
        public int SexId { get; set; }

        public string SexNombre { get; set; }

        public string SexDescripcion { get; set; }     
    }
}
