﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ContratoSolicitudDto
    {
        public long ContId { get; set; }

        public string ContNumero { get; set; }         
    }
}
