﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class AuditoriaContratoDto
    {
        public long AucoId { get; set; }

        public long ContId { get; set; }

        public int ProdId { get; set; }

        public int AucoEmpleado { get; set; }

        public DateTime AucoFechaEmision { get; set; }

        public decimal AucoMonto { get; set; }

        public string AucoOperacion { get; set; }

        public string AucoComentario { get; set; }
                
        public string AucoRegistradopor { get; set; }

        public DateTime AucoFechaCreacion { get; set; }

        public ContratoDto Cont { get; set; }

        public ProductoDto Prod { get; set; }
    }
}
