﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class TipoDocumentoDto
    {
        public int TidoId { get; set; } 

        public string TidoNombre { get; set; }

        public string TidoDescripcion { get; set; }
        
    }
}
