﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class InventarioDto
    {
        public long InveId { get; set; }

        public int InveTipo { get; set; }
         
        public string InveDescripcion { get; set; }        

        public DateTime MoinFechaCreacion { get; set; }

        public int ProdId { get; set; }

        public string ProdNombre { get; set; }

        public long MoinId { get; set; }

        public string MoinDescripcion { get; set; }

        public decimal MoinCantidad { get; set; }
    }
}
