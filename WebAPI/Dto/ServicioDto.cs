﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ServicioDto
    {
        public int SerId { get; set; }

        public string SerNombre { get; set; }

        public string SerDescripcion { get; set; }

        public long SerCodigo { get; set; }

        public decimal? SerValor { get; set; }

        public short? EmprId { get; set; }         
    }
}
