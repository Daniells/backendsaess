﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class DepartamentoDto
    {
        public long DepDepartamento { get; set; }

        public string DepDescripcion { get; set; }

        public long PaPais { get; set; }       

        public List<CiudadMunicipiosDto> CiudadMunicipios { get; set; }

    }
}
