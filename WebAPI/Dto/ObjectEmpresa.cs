﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ObjectEmpresa
    {
        public short EmprId { get; set; }

        public string EmprNit { get; set; }

        public string EmprNombre { get; set; }

        public string EmprDireccion { get; set; }

        public string EmprEmail { get; set; }

        public string EmprTelefono { get; set; }

        public string EmprTelefax { get; set; }
          
        public string EmprRegistradopor { get; set; }        

        public string EmprCiudad { get; set; }
         
        public long? CimuId { get; set; }
    }
}
