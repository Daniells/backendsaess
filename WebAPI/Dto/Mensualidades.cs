﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class Mensualidades
    {
        public string MesActual { get; set; }

        public string MesAnterior { get; set; }

        public int PagadasMesActual { get; set; }

        public int PendientesMesActual { get; set; }

        public int PendientesMesAnterior { get; set; }

        public int Adelantadas { get; set; }
    }
}
