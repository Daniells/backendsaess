﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ProveedorDto
    {
        public string PersId { get; set; }

        public string PersNumDocumento { get; set; }

        public string PersNombre { get; set; }

        public string PersApellido { get; set; }
    }
}
