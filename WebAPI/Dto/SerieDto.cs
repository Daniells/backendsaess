﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class SerieDto
    {
        public int SeriId { get; set; }

        public string SeriLetra { get; set; }

        public string SeriDescripcion { get; set; }     
      
        public int SeriCorrelativo { get; set; }      
    }
}
