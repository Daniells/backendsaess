﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Dto
{
    public class PersonaPorIdDto
    {
        public string PersId { get; set; }  
        
        public string PersNombre { get; set; }

        public string PersApellido { get; set; }

        public DateTime? PersFechanacimiento { get; set; }

        public string PersDireccion { get; set; }

        public string PersTelefono { get; set; }

        public string PersCelular { get; set; }

        public string PersEmail { get; set; }      

        public string PersNumDocumento { get; set; }

        public string Contribuyente { get; set; }

        public EstadoCivil Esci { get; set; }

        public Sexo Sex { get; set; }

        public Tipodocumento Tido { get; set; }

        public ClasificacionCliente Clasificacion { get; set; }

        public Tipopersona Tipo { get; set; }        

        public List<Zona> zona { get; set; }

        public List<Sector> sector { get; set; }
    }
}
