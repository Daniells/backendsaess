﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Dto
{
    public class SolicitudServicioDto
    {
        public long SoseId { get; set; }    

        public int ProdId { get; set; }      

        public long? FactId { get; set; }

        public int SoseEmpleado { get; set; }

        public string SoseNumero { get; set; }

        public decimal? Precio { get; set; }

        public string SoseDescripcion { get; set; }

        public DateTime? SoseFechaEjecucion { get; set; }
         
        public decimal? SosePrecio { get; set; }

        public int? SerId { get; set; }

        public EstadoSolicitudDto Esso { get; set; }

        public ProductoDto Prod { get; set; }

        public ServicioDto Ser { get; set; }

       
         
        //Producto

        public string ProdCodigo { get; set; }

        public string ProdNombre { get; set; }

        public decimal ProdPrecioVenta { get; set; }

        //servicio

        public string SerNombre { get; set; }

        public string SerDescripcion { get; set; }

        public long SerCodigo { get; set; }

        public long ContId { get; set; }

        //persona

        public string PersonaID { get; set; }

        public List<ProductosolicitudDto> productosolicitud { get; set; }

        public TipoSolicitudDto Tiso { get; set; }
        public List<ContratoDto> Contrato { get; set; }        
        public List<SolicitudConServicioDto> Solicitudconservicio { get; set; }

    }
}
