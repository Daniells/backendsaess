﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class EstadoContratoDto
    {
        public short EscoId { get; set; }

        public string EscoNombre { get; set; }

        public string EscoDescripcion { get; set; }

        public string EscoRegistradopor { get; set; }

        public DateTime EscoFechaCreacion { get; set; }

        public List<ContratoDto> Contrato { get; set; }
    }
}
