﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ContratoDto
    {
        public long ContId { get; set; }

        public short ClclId { get; set; }

        public int SectId { get; set; }

        public short EscoId { get; set; }

        public string ContNumero { get; set; }

        public decimal ContCuota { get; set; }

        public string ContDireccion { get; set; }

        public string ContTelefono { get; set; }

        public DateTime? ContFechaInstalacion { get; set; }

        public DateTime ContFechaCreacion { get; set; }

        public string ContGeolocalizacion { get; set; }

        public string PersId { get; set; }

        public int CabModenId { get; set; }

        public string Responsable { get; set; }

        public string Registradopor { get; set; }

        public bool? Activo { get; set; }

        public int? SerId { get; set; }

        //cable moden

        public string CabMarca { get; set; }

        public string CabIdMac { get; set; }

        public string CabSerial { get; set; }

        //ClasificacionCliente
               
        public string ClclNombre { get; set; }

        public string ClclDescripcion { get; set; }

        public bool ClclExento { get; set; }
              

        //Estado Contrato

        public string EscoNombre { get; set; }

        public string EscoDescripcion { get; set; }

        //servicio
        public List<SolicitudServicioDto> SolicitudServicio { get; set; }

        public List<ServicioDto> Servicio { get; set; }

        public List<PersonaDto> Persona { get; set; }

        public List<ZonaDto> Zona { get; set; }

        public List<FacturaDto> Factura { get; set; }
    }
}
