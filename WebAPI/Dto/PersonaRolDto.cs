﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class PersonaRolDto
    {
        public int PeroId { get; set; }

        public int PersId { get; set; }

        public int RolId { get; set; }

        public bool PeroEstado { get; set; }

        public string PeroRegistradopor { get; set; }

        public DateTime PeroFechaCreacion { get; set; }

        public PersonaDto Pers { get; set; }

        public RolDto Rol { get; set; }
    }
}
