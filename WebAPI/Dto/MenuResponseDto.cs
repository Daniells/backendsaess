﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class MenuResponseDto
    {
        public int Idmenu { get; set; }

        public string Menu { get; set; }

        public List<ModuloDto> Modulo { get; set; }
    }
}
