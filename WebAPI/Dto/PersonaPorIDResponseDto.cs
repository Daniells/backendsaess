﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class PersonaPorIDResponseDto
    {
        public string PersId { get; set; }

        public string PersNombre { get; set; }

        public string PersApellido { get; set; }

        public DateTime? PersFechanacimiento { get; set; }

        public string PersDireccion { get; set; }

        public string PersTelefono { get; set; }

        public string PersCelular { get; set; }

        public string PersEmail { get; set; }

        public string PersNumDocumento { get; set; }

        public string Contribuyente { get; set; }

        public EstadoCivilDto Esci { get; set; }

        public SexoDto Sex { get; set; }

        public TipoDocumentoDto Tido { get; set; }

        public ClasificacionClienteDto Clasificacion { get; set; }

        public TipoPersonaDto Tipo { get; set; }

        public List<ZonaPersonaPorIDDto> zona { get; set; }

        public List<SectorDto> sector { get; set; }
    }
}
