﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class TipoSolicitudDto
    {
        public int TisoId { get; set; }
        public string TisoNombre { get; set; }
        public string TisoDescripcion { get; set; }     
    }
}
