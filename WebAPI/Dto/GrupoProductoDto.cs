﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class GrupoProductoDto
    {
        public int Idgrupo { get; set; }
        public string Nombre { get; set; }
    }
}
