﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class DetalleFacturaDto
    {
        public long DefaId { get; set; }

        public long FactId { get; set; }

        public int ProdId { get; set; }

        public float? DefaIva { get; set; }

        public decimal DefaCantidad { get; set; }

        public string DefaDescripcion { get; set; }

        public decimal DefaVunitario { get; set; }

        public decimal DefaBase { get; set; }

        public decimal DefaMontoIva { get; set; }

        public decimal DefaTotal { get; set; }

        public string DefaRegistradopor { get; set; }

        public DateTime DefaFechaCreacion { get; set; }

        public int PaqId { get; set; }

        public FacturaDto Fact { get; set; }

        public PaqueteDto Paq { get; set; }

        public ProductoDto Prod { get; set; }
    }
}
