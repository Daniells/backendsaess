﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class FacturaDto
    {
        public long FactId { get; set; }

        public string FactNcontrol { get; set; }

        public string FactNfactura { get; set; }

        public short FactCondicionPago { get; set; }

        public int? FactTercero { get; set; }

        public int FactEmpleado { get; set; }

        public float? FactIva { get; set; }

        public decimal FactBase { get; set; }

        public decimal FactMontoIva { get; set; }

        public decimal FactTotal { get; set; }

        public DateTime FactFechaEmision { get; set; }

        public string TipoDeMovimiento { get; set; }

        public string PersId { get; set; }

        public string PersNombre { get; set; }

        public string FactCodigoPago { get; set; } 

        public string FactConcepto { get; set; }

        public int? SerId { get; set; }

        //datos serie
        public int SeriId { get; set; }

        public string SeriLetra { get; set; }

        public string SeriDescripcion { get; set; }

        //Datos de Mes

        public int MesId { get; set; }

        public string MesNombre { get; set; }

        //Datos Forma de Pago

        public short FopaId { get; set; }

        public string FopaNombre { get; set; }

        //Datos estado Factura
        
        public int EsfaId { get; set; }

        public string EsfaNombre { get; set; }

        //Datos de la Persona

        public List<PersonaDto> Persona { get; set; }

    }
}
