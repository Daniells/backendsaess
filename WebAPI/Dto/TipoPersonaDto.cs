﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class TipoPersonaDto
    {
        public int TipeId { get; set; }

        public string TipeNombre { get; set; }

        public string TipeDescrpcion { get; set; }
    }
}
