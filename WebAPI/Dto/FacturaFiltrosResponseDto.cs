﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Dto
{
    public class FacturaFiltrosResponseDto
    {
        public long FactId { get; set; }

        public string FactNcontrol { get; set; }

        public string FactNfactura { get; set; }

        public float? FactIva { get; set; }

        public decimal FactBase { get; set; }

        public decimal FactMontoIva { get; set; }

        public decimal FactTotal { get; set; }

        public DateTime FactFechaCreacion { get; set; }

        public string TipoDeMovimiento { get; set; }

        public decimal? FactDescuento { get; set; }

        public string FactCodigoPago { get; set; }

        public string FactConcepto { get; set; }

        public EstadoFacturaDto estado { get; set; }

        public FormaPagoDto formaPago { get; set; }

        public MesesDto mes { get; set; }

        public SerieDto serie { get; set; }

        public ContratoSolicitudDto contrato { get; set; }

        public List<ServicioFacturaDto> servicio { get; set; }        

        public string PersId { get; set; }
    }
}
