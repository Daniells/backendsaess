﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ClasificacionClienteDto
    {
        public short ClclId { get; set; }

        public string ClclNombre { get; set; }

        public string ClclDescripcion { get; set; }

        public bool ClclExento { get; set; }       
    }
}
