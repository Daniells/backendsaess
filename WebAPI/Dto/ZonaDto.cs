﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ZonaDto
    {
        public int ZonaId { get; set; }

        public string ZonaNombre { get; set; }

        public string ZonaDescripcion { get; set; }

        public long CimuId { get; set; }

        public string CimuDescripcion { get; set; }

        public long DepDepartamento { get; set; }

        public string DepDescripcion { get; set; }

        public List<SectorDto> Sector { get; set; }       
    }
}
