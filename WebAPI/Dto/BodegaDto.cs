﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class BodegaDto
    {
        public int BodeId { get; set; }
        public string BodeNombre { get; set; }        
        public string BodeResponsable { get; set; }
        public string BodeDireccion { get; set; }
        public string BodeTelefono { get; set; }
    }
}
