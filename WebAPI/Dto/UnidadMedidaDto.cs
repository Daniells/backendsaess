﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class UnidadMedidaDto
    {
        public short UnmeId { get; set; }

        public string UnmeAbreviatura { get; set; }

        public string UnmeNombre { get; set; }

        public string UnmeDescripcion { get; set; } 
       
    }
}
