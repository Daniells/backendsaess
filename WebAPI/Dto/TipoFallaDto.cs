﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class TipoFallaDto
    {
        public short TifaId { get; set; }

        public string TifaNombre { get; set; }

        public string TifaDescripcion { get; set; }

        public string TifaRegistradopor { get; set; }

        public DateTime TifaFechaCreacion { get; set; }

        public List<ReclamoDto> Reclamo { get; set; }
    }
}
