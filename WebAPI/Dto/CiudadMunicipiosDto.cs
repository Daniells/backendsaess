﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class CiudadMunicipiosDto
    {
        public long CimuId { get; set; }

        public string CimuDescripcion { get; set; }

        public long DepDepartamento { get; set; }       
    }
}
