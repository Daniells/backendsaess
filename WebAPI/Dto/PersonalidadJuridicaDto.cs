﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class PersonalidadJuridicaDto
    {
        public short PejuId { get; set; }

        public string PejuLetra { get; set; }

        public string PejuNombre { get; set; }

        public string PejuDescripcion { get; set; }

        public string PejuRegistradopor { get; set; }

        public DateTime PejuFechaCreacion { get; set; }

        public List<PersonaDto> Persona { get; set; }
    }
}
