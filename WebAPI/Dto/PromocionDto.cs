﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class PromocionDto
    {
        public int PromId { get; set; }

        public int PromNumDocumento { get; set; }

        public string PromNombre { get; set; }

        public string PromApellido { get; set; }

        public int PromTelefono { get; set; }

        public decimal PromDescuento { get; set; }

        public string PromCodigo { get; set; }     
       
    }
}
