﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ProductosolicitudDto
    {
        public int SoprId { get; set; }       
        public long? SoseId { get; set; }      
        public ProductoDto Prod { get; set; }
    }
}
