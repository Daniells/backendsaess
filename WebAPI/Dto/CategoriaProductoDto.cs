﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class CategoriaProductoDto
    {
        public short CaprId { get; set; }

        public short CaprTipo { get; set; }

        public string CaprNombre { get; set; }

        public string CaprDescripcion { get; set; }           
    }
}
