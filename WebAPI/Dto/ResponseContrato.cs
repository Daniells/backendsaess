﻿using System;
using System.Collections.Generic;
using WebAPI.Models;

namespace WebAPI.Dto
{
    public class ResponseContrato
    {
        public long ContId { get; set; }

        public short ClclId { get; set; }

        public int SectId { get; set; }

        public string PersId { get; set; }

        public short EscoId { get; set; }       

        public string ContNumero { get; set; }

        public decimal ContCuota { get; set; }

        public string ContDireccion { get; set; }

        public string ContTelefono { get; set; }

        public DateTime? ContFechaInstalacion { get; set; }

        public DateTime ContFechaCreacion { get; set; }

        public string ContResponsable { get; set; }

        public string ContGeolocalizacion { get; set; }

        //informacion adicional

        public Servicio Servicio { get; set; }

        public Persona Persona { get; set; }

        public Zona Zona { get; set; }

        public Sector Sector { get; set; }

        public EstadoContrato EstadoContrato { get; set; }

        public string Estado { get; set; }
    }
}
