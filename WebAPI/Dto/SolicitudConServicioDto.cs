﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class SolicitudConServicioDto
    {
        public decimal? SocseValor { get; set; }

        public ServicioDto Ser { get; set; }
        public SolicitudServicioDto Sose { get; set; }
    }
}
