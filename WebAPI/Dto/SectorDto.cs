﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class SectorDto
    {
        public int SectId { get; set; }

        public int ZonaId { get; set; }

        public string SectAbreviatura { get; set; }

        public string SectNombre { get; set; }

        public string SectDescripcion { get; set; }

        public float SectRecargo { get; set; }       
    }
}
