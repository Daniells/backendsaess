﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Dto
{
    public class ProductoDto
    {
        public int ProdId { get; set; }      

        public string ProdCodigo { get; set; }

        public string ProdNombre { get; set; }
         
        public decimal ProdPrecioVenta { get; set; }
              
        public decimal ProdCminimo { get; set; }
               
        public CategoriaProductoDto Categoria { get; set; }

        public UnidadMedidaDto UnMedida { get; set; }

        public IvaDto Iva { get; set; }

        public Marca Marca { get; set; }

        public Grupoproducto Grupo { get; set; }
    }
}
