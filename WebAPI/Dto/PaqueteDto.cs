﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class PaqueteDto
    {
        public int PaqId { get; set; }

        public string PaqNombre { get; set; }

        public string PaqDescripcion { get; set; }

        public DateTime PaqFechaCreacion { get; set; }

        public string PaqRegistradopor { get; set; }

        public int SerId { get; set; }

        public float PaqPrecio { get; set; }

        public ServicioDto Ser { get; set; }

        public List<DetalleFacturaDto> DetalleFactura { get; set; }
    }
}
