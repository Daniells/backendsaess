﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ReclamoDto
    {
        public int ReclId { get; set; }

        public long ContId { get; set; }

        public short TifaId { get; set; }

        public short EssoId { get; set; }

        public int ReclEmpleado { get; set; }

        public DateTime? ReclFechaEjecucion { get; set; }

        public string ReclDescripcion { get; set; }

        public string ReclRegistradopor { get; set; }

        public DateTime ReclFechaCreacion { get; set; }

        public ContratoDto Cont { get; set; }

        public EstadoSolicitudDto Esso { get; set; }

        public TipoFallaDto Tifa { get; set; }
    }
}
