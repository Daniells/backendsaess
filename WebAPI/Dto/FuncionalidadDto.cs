﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class FuncionalidadDto
    {
        public int FuncId { get; set; }

        public int ModuId { get; set; }

        public short? FuncPosicion { get; set; }

        public string FuncNombre { get; set; }

        public string FuncDescripcion { get; set; }

        public string FuncUrl { get; set; }

        public bool FuncEstado { get; set; }

        public string FuncRegistradopor { get; set; }

        public DateTime FuncFechaCreacion { get; set; }

        public ModuloDto Modu { get; set; }

        public ICollection<FuncionalidadRolDto> FuncionalidadRol { get; set; }

        //Modulo
        public string ModuNombre { get; set; }

    }
}
