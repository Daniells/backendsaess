﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class PersonaDto
    {
        public string PersId { get; set; }       

        public short EsciId { get; set; }

        public string PersNumDocumento { get; set; }

        public bool PersIscontribuyente { get; set; }    

        public string PersNombre { get; set; }

        public string PersApellido { get; set; }

        public DateTime? PersFechanacimiento { get; set; }

        public string PersSexo { get; set; }

        public int SexId { get; set; }

        public string PersDireccion { get; set; }

        public string PersTelefono { get; set; }

        public string PersCelular { get; set; }

        public string PersEmail { get; set; }

        public string PersPassword { get; set; }

        public short? ClclId { get; set; }

        //Estado Civil

        public string EsciNombre { get; set; }

        //tipo persona   

        public int TipeId { get; set; }

        public string TipoPersona { get; set; }

        public int tidoId { get; set; }        

        public string tipodocumento { get; set; }

        //empresa
        public List<EmpresaDto> Empresa { get; set; }

        // Zona
        public List<ZonaDto> Zona { get; set; }

        // Rol
        public List<RolDto> Rol { get; set; }

        public string RolNombre { get; set; }
    }
}
