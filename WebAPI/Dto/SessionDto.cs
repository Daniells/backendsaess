﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class SessionDto
    {
        public int Id { get; set; }

        public string User { get; set; }

        public string Password { get; set; }

        public short EmpresaId { get; set; }        
    }
}
