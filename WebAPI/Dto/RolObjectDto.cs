﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class RolObjectDto
    {
        public int RolId { get; set; }

        public string RolNombre { get; set; }

        public string RolDescripcion { get; set; }       

        public string RolRegistradopor { get; set; }
    }
}
