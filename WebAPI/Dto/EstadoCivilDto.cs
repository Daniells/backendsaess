﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class EstadoCivilDto
    {
        public short EsciId { get; set; }

        public string EsciNombre { get; set; }

        public string EsciDescripcion { get; set; }       
    }
}

