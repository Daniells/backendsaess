﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ModuloDto
    {
        public int ModuId { get; set; }    

        public string ModuNombre { get; set; }       
    }
}
