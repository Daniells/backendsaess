﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class EstadoFacturaDto
    {
        public int EsfaId { get; set; }

        public string EsfaNombre { get; set; }

        public string EsfaDescrpcion { get; set; }       
    }
}
