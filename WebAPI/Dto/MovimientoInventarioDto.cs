﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class MovimientoInventarioDto
    {
        //campos de inventario
        public long InveId { get; set; }

        public int InveTipo { get; set; }

        public string InveDescripcion { get; set; }

        public string InveRegistradopor { get; set; }        

        public int? BodeId { get; set; }       

        //campos de movimiento inventario
        public long MoinId { get; set; }
       
        public int ProdId { get; set; }

        public decimal MoinCantidad { get; set; }       
        
        public long? ContId { get; set; }

        public string MoinDescripcion { get; set; }       

        public decimal? MoinPrecioCompra { get; set; }
    }
}
