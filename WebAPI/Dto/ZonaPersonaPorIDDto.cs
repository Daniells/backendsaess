﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class ZonaPersonaPorIDDto
    {
        public int ZonaId { get; set; }

        public string ZonaNombre { get; set; }

        public string ZonaDescripcion { get; set; }
    }
}
