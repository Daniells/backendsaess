﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class FuncionalidadRolDto
    {
        public int Idmenu { get; set; }
        public string Menu { get; set; }
        public int Idmodulo { get; set; }
        public string Modulo { get; set; }
        public int IdFuncionalidad { get; set; }
        public string Funcionalidad { get; set; }
        public int IdRol { get; set; }
        public string Rol { get; set; }        
    }
}
