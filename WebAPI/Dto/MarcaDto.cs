﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class MarcaDto
    {
        public int Idmarca { get; set; }
        public string Nombre { get; set; }
    }
}
