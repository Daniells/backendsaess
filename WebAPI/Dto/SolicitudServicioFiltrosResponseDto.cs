﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class SolicitudServicioFiltrosResponseDto
    {
        public long SoseId { get; set; }

        public string SoseNumero { get; set; }

        public string SoseDescripcion { get; set; }

        public decimal? SosePrecio { get; set; }

        public DateTime SoseFechaEjecucion { get; set; }

        public DateTime SoseFechaCreacion { get; set; }

        public TipoSolicitudDto Tiso { get; set; }

        public EstadoSolicitudDto Esso { get; set; }

        public ContratoSolicitudDto Contrato { get; set; }        

        public string persId { get; set; }

        public string UltimoNumeroSolicitudBD { get; set; }
    }
}
