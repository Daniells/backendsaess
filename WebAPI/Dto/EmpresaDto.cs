﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class EmpresaDto
    {
        public short EmprId { get; set; }

        public string EmprNit { get; set; }

        public string EmprNombre { get; set; }

        public string EmprDireccion { get; set; }

        public string EmprEmail { get; set; }

        public string EmprTelefono { get; set; }

        public string EmprTelefax { get; set; }

        public DateTime EmprFechaInicio { get; set; }

        public string EmprCiudad { get; set; }

        public string Departamento { get; set; }

        public long IdEmprCiudad { get; set; }

        public long IdDepartamento { get; set; }

        //persona
        public List<PersonaDto> Persona { get; set; }
    }
}
