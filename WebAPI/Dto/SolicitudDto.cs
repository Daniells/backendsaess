﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class SolicitudDto
    {
        public long SoseId { get; set; }
        public short EssoId { get; set; }
        public long? FactId { get; set; }
        public int SoseEmpleado { get; set; }
        public string SoseNumero { get; set; }
        public string SoseDescripcion { get; set; }
        public DateTime SoseFechaEjecucion { get; set; }
        public string SoseRegistradopor { get; set; }
        public bool? SoseActivo { get; set; }
        public decimal? SosePrecio { get; set; }
        public int? SoseTipo { get; set; }
        public string SoseResponsable { get; set; }
        public int? TisoId { get; set; }
        public FacturaDto Factura { get; set; }
        public string ContNumero { get; set; }
    }
}
