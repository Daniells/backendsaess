﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class objectoPersona
    {
        public string PersId { get; set; }

        public short EsciId { get; set; }

        public bool PersIscontribuyente { get; set; }

        public string PersNombre { get; set; }

        public string PersApellido { get; set; }

        public DateTime? PersFechanacimiento { get; set; }

        public string PersDireccion { get; set; }

        public string PersTelefono { get; set; }

        public string PersCelular { get; set; }

        public string PersEmail { get; set; }

        public string PersLogin { get; set; }

        public string PersPassword { get; set; }

        public string PersRegistradopor { get; set; }
         
        public short? ClclId { get; set; }

        public int? TidoId { get; set; }

        public int? TipeId { get; set; }

        public int? SexId { get; set; }

        public string PersNumDocumento { get; set; }

        public List<objectoPersonaRolDto> PersonaRol { get; set; }

    }
}
