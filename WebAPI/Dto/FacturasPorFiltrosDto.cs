﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Dto
{
    public class FacturasPorFiltrosDto
    {
        public long FactId { get; set; }
         
        public string FactNcontrol { get; set; }

        public string FactNfactura { get; set; }
         
        public float? FactIva { get; set; }

        public decimal FactBase { get; set; }

        public decimal FactMontoIva { get; set; }

        public decimal FactTotal { get; set; }
         
        public DateTime FactFechaCreacion { get; set; }
         
        public string TipoDeMovimiento { get; set; }
         
        public decimal? FactDescuento { get; set; }

        public string FactCodigoPago { get; set; }

        public string FactConcepto { get; set; }

        public Estadofactura estado { get; set; }

        public FormaPago formaPago { get; set; }

        public Mes mes { get; set; }        

        public Serie serie { get; set; }

        public Contrato contrato { get; set; }

        public List<Servicio> servicio { get; set; }        

        public string PersId { get; set; }

        public long? ContId { get; set; }
    }
}
