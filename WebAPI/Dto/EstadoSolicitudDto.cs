﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dto
{
    public class EstadoSolicitudDto
    {
        public short EssoId { get; set; }

        public string EssoNombre { get; set; }

        public string EssoDescripcion { get; set; }        
    }
}
