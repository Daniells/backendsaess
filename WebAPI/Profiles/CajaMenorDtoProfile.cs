﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class CajaMenorDtoProfile : Profile
    {
        public CajaMenorDtoProfile()
        {
            this.CreateMap<CajaMenorDto, CajaMenorResponseDto>()
                .ForMember(u => u.proveedor, p => p.MapFrom(m => m.persona));           
        }
    }
}
