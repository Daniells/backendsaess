﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;

namespace WebAPI.Profiles
{
    public class SolicitudServicioDtoProfile : Profile
    {
        public SolicitudServicioDtoProfile()
        {
            this.CreateMap<SolicitudServicioFiltrosResponseDto,SolicitudServicioFiltrosDto>()
                .ForMember(u => u.Esso, p => p.MapFrom(m => m.Esso))
                .ForMember(u => u.Tiso, p => p.MapFrom(m => m.Tiso))               
                .ReverseMap();
        }
    }
}
