﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class ZonaPersonaPorIDProfile : Profile
    {
        public ZonaPersonaPorIDProfile()
        {
            this.CreateMap<Zona, ZonaPersonaPorIDDto>()
                .ForMember(u => u.ZonaId, p => p.MapFrom(m => m.ZonaId))
                .ForMember(u => u.ZonaNombre, p => p.MapFrom(m => m.ZonaNombre))
                .ForMember(u => u.ZonaDescripcion, p => p.MapFrom(m => m.ZonaDescripcion));                
        }
    }
}
