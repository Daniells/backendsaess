﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class TipoDocumentoProfile : Profile
    {
        public TipoDocumentoProfile()
        {
            this.CreateMap<Tipodocumento, TipoDocumentoDto>()
                .ForMember(u => u.TidoId, p => p.MapFrom(m => m.TidoId))
                .ForMember(u => u.TidoNombre, p => p.MapFrom(m => m.TidoNombre))
                .ForMember(u => u.TidoDescripcion, p => p.MapFrom(m => m.TidoDescripcion));                
        }
    }
}
