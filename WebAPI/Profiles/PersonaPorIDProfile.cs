﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;

namespace WebAPI.Profiles
{
    public class PersonaPorIDProfile : Profile
    {
        public PersonaPorIDProfile()
        {
            this.CreateMap<PersonaPorIdDto, PersonaPorIDResponseDto>()
                .ForMember(u => u.Esci, p => p.MapFrom(m => m.Esci))
                .ForMember(u => u.Sex, p => p.MapFrom(m => m.Sex))
                .ForMember(u => u.Tido, p => p.MapFrom(m => m.Tido))
                .ForMember(u => u.zona, p => p.MapFrom(m => m.zona));
        }
    }
}
