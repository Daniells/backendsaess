﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class ProductosolicitudProfile : Profile
    {
        public ProductosolicitudProfile()
        {
            this.CreateMap<ProductosolicitudDto, Productosolicitud>()
                .ForMember(u => u.Prod, p => p.MapFrom(m => m.Prod))
                .ForMember(u => u.Sose, p => p.Ignore())
                .ReverseMap();
        }
    }
}
