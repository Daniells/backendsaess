﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class EstadoSolicitudProfile : Profile
    {
        public EstadoSolicitudProfile()
        {
            this.CreateMap<EstadoSolicitudDto, EstadoSolicitud>()
                .ForMember(u => u.SolicitudServicio, p => p.Ignore())
                .ForMember(u => u.Reclamo, p => p.Ignore())
                .ReverseMap();
        }
    }
}
