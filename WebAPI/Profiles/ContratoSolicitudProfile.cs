﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class ContratoSolicitudProfile : Profile
    {
        public ContratoSolicitudProfile()
        {
            this.CreateMap<Contrato, ContratoSolicitudDto>()
                .ForMember(u => u.ContId, p => p.MapFrom(m => m.ContId))
                .ForMember(u => u.ContNumero, p => p.MapFrom(m => m.ContNumero));    
        }
    }
}
