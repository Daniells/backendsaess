﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class SerieProfile : Profile
    {
        public SerieProfile()
        {
            this.CreateMap<Serie, SerieDto>()
                .ReverseMap();
        }
    }
}
