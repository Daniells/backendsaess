﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class MenuProfile : Profile
    {
        public MenuProfile()
        {
            this.CreateMap<MenuDto, MenuResponseDto>()
                .ForMember(u => u.Modulo, p => p.MapFrom(m => m.Modulo))
                .ReverseMap();
        }
    }
}
