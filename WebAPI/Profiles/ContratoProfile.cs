﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class ContratoProfile : Profile
    {
        public ContratoProfile()
        {
            this.CreateMap<ContratoDto, Contrato>()
                .ForMember(u => u.ContResponsable, p => p.MapFrom(m => m.Responsable))
                .ForMember(u => u.ContRegistradopor, p => p.MapFrom(m => m.Registradopor))
                .ForMember(u => u.ContActivo, p => p.MapFrom(m => m.Activo));
        }
    }
}
