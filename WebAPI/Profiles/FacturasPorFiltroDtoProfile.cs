﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class FacturasPorFiltroDtoProfile : Profile
    {
        public FacturasPorFiltroDtoProfile()
        {
            this.CreateMap<FacturasPorFiltrosDto, FacturaFiltrosResponseDto>()
                .ForMember(f => f.estado, p => p.MapFrom(m => m.estado))
                .ForMember(f => f.formaPago, p => p.MapFrom(m => m.formaPago))
                .ForMember(f => f.mes, p => p.MapFrom(m => m.mes))
                .ForMember(f => f.serie, p => p.MapFrom(m => m.serie));
        }
    }
}
