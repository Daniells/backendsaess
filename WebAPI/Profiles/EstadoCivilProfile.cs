﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class EstadoCivilProfile : Profile
    {
        public EstadoCivilProfile()
        {
            this.CreateMap<EstadoCivil, EstadoCivilDto>()
                .ForMember(u => u.EsciId, p => p.MapFrom(m => m.EsciId))
                .ForMember(u => u.EsciNombre, p => p.MapFrom(m => m.EsciNombre))
                .ForMember(u => u.EsciDescripcion, p => p.MapFrom(m => m.EsciDescripcion));                
        }
    }
}
