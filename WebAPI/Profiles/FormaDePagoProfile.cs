﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class FormaDePagoProfile : Profile
    {
        public FormaDePagoProfile()
        {
            this.CreateMap<FormaPago, FormaPagoDto>()
                .ReverseMap();
        }
    }
}
