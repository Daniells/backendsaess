﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class ProductoProfile : Profile
    {
        public ProductoProfile()
        {
            this.CreateMap<ProductoDto, Producto>()                
                .ForMember(u => u.Iva, p => p.MapFrom(m => m.Iva))
                .ForMember(u => u.Capr, p => p.MapFrom(m => m.Categoria))
                .ForMember(u => u.Unme, p => p.MapFrom(m => m.UnMedida))
                .ForMember(u => u.IdgrupoNavigation, p => p.MapFrom(m => m.Grupo))
                .ForMember(u => u.IdmarcaNavigation, p => p.MapFrom(m => m.Marca))
                .ReverseMap();
        }
    }
}
