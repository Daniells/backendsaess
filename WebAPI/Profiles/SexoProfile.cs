﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class SexoProfile : Profile
    {
        public SexoProfile()
        {
            this.CreateMap<Sexo, SexoDto>()
                .ForMember(u => u.SexId, p => p.MapFrom(m => m.SexId))
                .ForMember(u => u.SexNombre, p => p.MapFrom(m => m.SexNombre))
                .ForMember(u => u.SexDescripcion, p => p.MapFrom(m => m.SexDescripcion));                
        }
    }
}
