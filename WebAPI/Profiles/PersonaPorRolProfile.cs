﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dto;
using WebAPI.Models;

namespace WebAPI.Profiles
{
    public class PersonaPorRolProfile : Profile
    {
        public PersonaPorRolProfile()
        {
            this.CreateMap<Persona, PersonaPorRolDto>();                 
        }
    }
}
